# Dependencies

* XAMPP
* SWT Toolkit
* Eclipse (optional)

1. Install XAMPP (tested version 1.7.3).
3. Copy the contents of `data/mysql` into `xampp/mysql/data`.
4. Extract `swt.tar.gz` and import it into Eclipse.
5. Extract `licenta.tar.gz` and import it into Eclipse.
6. Add external `jar` files from the `lib` directory.
7. Run.
