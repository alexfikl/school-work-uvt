package aparate;

import main.Main;

import org.eclipse.swt.SWT;
import timer.*;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import InterfataGrafica.Interfata;

public class Aragaz extends HouseComponentBean implements java.io.Serializable,
		TimerListener {

	/* Properties */
	/*
	 * private String name = null; private int power = 0;
	 */
	private boolean stare = false;
	private Image image;
	int x, y;

	/* Constructor */
	public Aragaz(final int x, final int y) {

		this.x = x;
		this.y = y;
		if (stare == false) {
			image = new Image(Interfata.display,
					"pozeAparate/aragazInchis1.png");
			stare = true;
		} else {
			image = new Image(Interfata.display,
					"pozeAparate/aragazDeschis.png");
			stare = false;
		}
	}

	public int getX() {
		return x;
	}


	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}


	public void setY(int y) {
		this.y = y;
	}


	public boolean getStare() {
		return stare;
	}

	public void setStare(boolean i) {
		
		stare = i;
		
		if (stare == false) {
			image = new Image(Interfata.display,
					"pozeAparate/aragazInchis1.png");
			stare = true;
		} else {
			image = new Image(Interfata.display,
					"pozeAparate/aragazDeschis.png");
			stare = false;
		}
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image img) {
		image = img;
	}

	public void timeElapsed(TimerEvent evt) {
		System.out.println(this.getName() + " functioneaza la data: "
				+ evt.toString() + " si consuma " + this.getPower());

	}
}
