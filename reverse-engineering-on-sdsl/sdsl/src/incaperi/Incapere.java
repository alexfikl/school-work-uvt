package incaperi;

import java.util.ArrayList;

public class Incapere {

	
	protected ArrayList<Perete> pereti;
	protected ArrayList<Usa> usi;
	protected ArrayList<Geam> geamuri;
	
	public Incapere()
	{
		pereti=new ArrayList<Perete>(4);
		usi=new ArrayList<Usa>();
		geamuri=new ArrayList<Geam>();
	}

	public ArrayList<Perete> getPereti() {
		return pereti;
	}

	
	public void adaugaPerete(Perete p)
	{
		this.pereti.add(p);
	}
	
	public void stergePerete(Perete p)
	{
		this.pereti.remove(p);
	}
	
	public void adaugaUsa(Usa p)
	{
		this.usi.add(p);
	}
	
	public void stergeUsa(Usa p)
	{
		this.usi.remove(p);
	}
	
	public void adaugaGeam(Geam p)
	{
		this.geamuri.add(p);
	}
	
	public void stergeGeam(Geam p)
	{
		this.geamuri.remove(p);
	}
	
	public void setPereti(ArrayList<Perete> pereti) {
		this.pereti = pereti;
	}

	public ArrayList<Usa> getUsi() {
		return usi;
	}

	public void setUsi(ArrayList<Usa> usi) {
		this.usi = usi;
	}

	public ArrayList<Geam> getGeamuri() {
		return geamuri;
	}

	public void setGeamuri(ArrayList<Geam> geamuri) {
		this.geamuri = geamuri;
	}
	
	
	
}
