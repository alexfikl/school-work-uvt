package lexicalAnalysis;

import java.io.IOException;

@SuppressWarnings("serial")
public class LexicalException extends Exception {
	public LexicalException(String msg) throws IOException {
		super(msg);		
	}
}
