package lexicalAnalysis;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import elements.Vocabulary;
/**
 * Verifica daca cuvintele sunt scrise bine si daca exista cuvinte ce nu sunt cuprinse
 * in vocabularul limbajului.
 */
public class LexicalAnalysis {
	protected Vocabulary voc;
	protected int index;

	public LexicalAnalysis() throws IOException {
		voc = new Vocabulary();
	}

	public boolean verifyBelonging(String[] words, int index)
			throws LexicalException, IOException {
		int j = 0, i;
		for (i = 0; i < words.length; i++) {
			//System.out.println("	"+words[i]);
			if (voc.getActiuni().contains(words[i])){
				j++;
			//	System.out.println("#Actiune");
			}
			else if (voc.getAparate().contains(words[i])){
				j++;
			//	System.out.println("#Aparate");
			}
			else if (voc.getContext().contains(words[i])){
				j++;
			//	System.out.println("#Context");
			}
			else if (voc.getCorp().contains(words[i])){
				j++;
			//	System.out.println("#Corp");
			}
			else if (voc.getIncaperi().contains(words[i])){
				j++;
			//	System.out.println("#Incapere");
			}
			else if (voc.getInincaperi().contains(words[i])){
				j++;
			//	System.out.println("#In Incapere");
			}
			else if (voc.getIntensitate().contains(words[i])){
				j++;
			//	System.out.println("#Intensitate");
			}
			else if (voc.getSetari().contains(words[i])){
				j++;
			//	System.out.println("#Setari");
			}
			else if (voc.getSpalare().contains(words[i])){
				j++;
			//	System.out.println("#Spalare");
			}
			else if (voc.getStatice().contains(words[i])){
				j++;
			//	System.out.println("#Statice");
			}
			else if (voc.getVreme().contains(words[i])){
				j++;
			//	System.out.println("#Vreme");
			}
			else if (voc.getTimp().contains(words[i])){
				j++;
			//	System.out.println("#Timp");
			}
			else if (voc.getName().contains(words[i])){
				j++;
			//	System.out.println("#Nume");
			}
			else if (verifyHour(words[i]) == true){
				j++;
			//	System.out.println("#Ora");
			}
		}
		if (j != i)
			throw new LexicalException("Word not found at line " + index + " : " + words[j]+"\n");
		return false;
	}

	public boolean verifyHour(String word) throws LexicalException {
		Pattern pattern;
		Matcher matcher;
		String TIME12HOURS_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";
		pattern = Pattern.compile(TIME12HOURS_PATTERN);
		matcher = pattern.matcher(word);
		return matcher.matches();
	}
}
