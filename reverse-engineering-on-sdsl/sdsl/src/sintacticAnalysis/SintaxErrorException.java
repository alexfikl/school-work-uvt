package sintacticAnalysis;

import java.io.IOException;

import main.WriteFile;

@SuppressWarnings("serial")
public class SintaxErrorException extends Exception {

	public SintaxErrorException(String msg) throws IOException {
		super(msg);
	}
}
