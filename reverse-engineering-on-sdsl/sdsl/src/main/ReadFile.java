package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFile {
	protected static ArrayList<String> composed;

	public ReadFile() {
		composed = new ArrayList<String>();
	}

	public static ArrayList<String> readFromFile(String file)
			throws IOException {
		FileReader f = new FileReader(file);
		BufferedReader br = new BufferedReader(f);

		String strLine;
		ArrayList<String> list = new ArrayList<String>();
		while ((strLine = br.readLine()) != null) {
			list.add(strLine);
			if (strLine.contains(" ") && !file.equals("scenariu.txt"))
				composed.add(strLine);
		}
		br.close();
		f.close();
		return list;
	}

	public static ArrayList<String> getComposed() {
		return composed;
	}
}
