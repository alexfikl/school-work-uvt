package main;

public class Om {

	public String nume;
	public String pozitie;
	public boolean isPlecat=false;
	public boolean doarme=true;
	
	public boolean isDoarme() {
		return doarme;
	}
	public void setDoarme(boolean doarme) {
		this.doarme = doarme;
	}
	public boolean isPlecat() {
		return isPlecat;
	}
	public void setPlecat(boolean isPlecat) {
		this.isPlecat = isPlecat;
	}
	public Om(String nume, String pozitie)
	{
		this.nume=nume;
		this.pozitie=pozitie;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPozitie() {
		return pozitie;
	}
	public void setPozitie(String pozitie) {
		this.pozitie = pozitie;
	}
	
}
