package main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {
	protected static FileWriter fw;
	protected static String file;
	public WriteFile(String file) throws IOException
	{
		this.file=file;
	}
	
	public static void write(String msg) throws IOException
	{
		
		fw=new FileWriter(file,true);		
		BufferedWriter bw=new BufferedWriter(fw);
		bw.append(msg);
		bw.close();
		fw.close();
	}
}
