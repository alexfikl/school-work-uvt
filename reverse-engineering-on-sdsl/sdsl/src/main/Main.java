package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import InterfataGrafica.Interfata;

import sintacticAnalysis.SintaxErrorException;
import lexicalAnalysis.LexicalException;

public class Main {
	public static Casa casa=new Casa();
	
	public static void main(String args[]) throws IOException, LexicalException, SintaxErrorException {
		
		Analysis a = new Analysis();
		Simulator s;
		
		a.verify();
		BufferedReader br=new BufferedReader(new FileReader("errorlog.txt"));
		if(br.readLine()!=null)
		{
			System.err.println("Scenariu gresit!");
			System.exit(1);
		}
		br.close();
		
		
		Interfata gui=new Interfata();
		s=new Simulator();
		try {
			s.startSimulator();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		gui.run();
		
		//s.stopSimulator();
		gui.stopInterfata();

	}
}
