package main;

import obStatice.cadaDeBaie;
import obStatice.*;
import aparate.Aragaz;
import aparate.Bec;
import aparate.Calculator;
import aparate.Frigider;
import aparate.Robinet;
import aparate.TV;
import aparate.WC;
import aparate.aerConditionat;
import aparate.masinaDeSpalat;
import incaperi.*;

public class Casa {

	Living living;
	Baie baie1;
	Baie baie2;
	Bucatarie bucatarie;
	Dormitor dormitor1;
	Dormitor dormitor2;
	Dormitor dormitor3;
	Hol hol;
	
	public Bec becBuc;
	public Bec becB2;
	public Bec becL;
	public Bec becB1;
	public Bec BecD1;
	public Bec BecD2;
	public Bec BecD3;
	public Bec BecH;
	public Frigider frigi;
	public Aragaz aragaz;
	public Robinet robinetBuc;
	public masinaDeSpalat masina;
	public Robinet robinetB2;
	public Robinet robinetBaie1;
	public cadaDeBaie cadaBaie1;
	public TV tv;
	public Calculator calcL;
	public aerConditionat aer;
	public WC wc1;
	public WC wc2;
	public Robinet robinetVana;
	public canapea canap;
	public masa masaScaune;
	public patDublu patD;
	public patSimplu patS1,patS2;
	public planta plant;
	
	public Casa()
	{
		createCasa();
	}
	
	public void createCasa()
	{
		living=new Living();
		baie1=new Baie();
		baie2=new Baie();
		bucatarie=new Bucatarie();
		dormitor1=new Dormitor();
		dormitor2=new Dormitor();
		dormitor3=new Dormitor();
		hol= new Hol();
		
		becBuc = new Bec(20,150);
		becB2 = new Bec(20, 400);
		becL = new Bec(200, 730);
		becB1 = new Bec(530, 730);
		BecD1 = new Bec(800, 730);
		BecD2 = new Bec(940, 400);
		BecD3 = new Bec(940, 150);
		BecH = new Bec(420, 400);
		
		//buc
		frigi= new Frigider(50, 20);
		aragaz = new Aragaz(150, 200);
		robinetBuc = new Robinet(220, 200);
		masaScaune=new masa(200,50);
		
		//bai2
		masina = new masinaDeSpalat(180, 310);
		robinetB2 = new Robinet(220, 430);
		wc2 = new WC(80, 310);
		
		//baie1
		robinetBaie1 = new Robinet(530,520);
		cadaBaie1 = new cadaDeBaie(420, 720);
		wc1 = new WC(410, 520);
		robinetVana=new Robinet(420,670);
		
		//living
		tv = new TV(180, 520);
		calcL = new Calculator(20, 520);
		canap=new canapea(250,700);
		
		//d2
		aer = new aerConditionat(940, 325);
		patS1=new patSimplu(650,450);
		
		//d3
		patS2=new patSimplu(650,250);
		//d1
		patD=new patDublu(650,730);
		//hol
		plant=new planta(600,50);
		
		living.adaugaPerete(new Perete(new Point(0,800), new Point (400,800)));
		living.adaugaPerete(new Perete(new Point(400,800), new Point (400,500)));
		living.adaugaPerete(new Perete(new Point(400,500), new Point (0,500)));
		living.adaugaPerete(new Perete(new Point(0,500), new Point (0,800)));
		living.adaugaUsa(new Usa(new Point(340,500), new Point (380,500)));
		living.adaugaUsa(new Usa(new Point(400,600), new Point (400,640)));
		living.adaugaGeam(new Geam(new Point(0,600), new Point (0,640)));
		living.adaugaGeam(new Geam(new Point(200,800), new Point (240,800)));
		
		baie1.adaugaPerete(new Perete(new Point(400,800), new Point (600,800)));
		baie1.adaugaPerete(new Perete(new Point(600,800), new Point (600,500)));
		baie1.adaugaPerete(new Perete(new Point(600,500), new Point (400,500)));
		baie1.adaugaPerete(new Perete(new Point(400,500), new Point (400,800)));
		baie1.adaugaUsa(new Usa(new Point(400,600), new Point (400,640)));
		baie1.adaugaUsa(new Usa(new Point(440,800), new Point (480,500)));
		baie1.adaugaUsa(new Usa(new Point(600,600), new Point (600,640)));
		baie1.adaugaGeam(new Geam(new Point(460,800), new Point (500,800)));
	
		dormitor1.adaugaPerete(new Perete(new Point(600,800), new Point (1000,800)));
		dormitor1.adaugaPerete(new Perete(new Point(1000,800), new Point (1000,500)));
		dormitor1.adaugaPerete(new Perete(new Point(1000,500), new Point (600,500)));
		dormitor1.adaugaPerete(new Perete(new Point(600,500), new Point (600,800)));
		dormitor1.adaugaUsa(new Usa(new Point(600,600), new Point (600,640)));
		dormitor1.adaugaUsa(new Usa(new Point(700,500), new Point (740,500)));
		dormitor1.adaugaGeam(new Geam(new Point(800,800), new Point (840,800)));
		
		dormitor2.adaugaPerete(new Perete(new Point(600,500), new Point (1000,500)));
		dormitor2.adaugaPerete(new Perete(new Point(1000,500), new Point (1000,300)));
		dormitor2.adaugaPerete(new Perete(new Point(1000,300), new Point (600,300)));
		dormitor2.adaugaPerete(new Perete(new Point(600,300), new Point (600,480)));
		dormitor2.adaugaUsa(new Usa(new Point(700,500), new Point (740,500)));
		dormitor2.adaugaUsa(new Usa(new Point(600,360), new Point (600,400)));
		dormitor2.adaugaGeam(new Geam(new Point(1000,400), new Point (1000,440)));
		
		dormitor3.adaugaPerete(new Perete(new Point(600,300), new Point (1000,300)));
		dormitor3.adaugaPerete(new Perete(new Point(1000,300), new Point (1000,0)));
		dormitor3.adaugaPerete(new Perete(new Point(1000,0), new Point (600,0)));
		dormitor3.adaugaPerete(new Perete(new Point(600,0), new Point (600,300)));
		dormitor3.adaugaUsa(new Usa(new Point(600,140), new Point (600,180)));
		dormitor3.adaugaGeam(new Geam(new Point(700,0), new Point (740,0)));
		
		baie2.adaugaPerete(new Perete(new Point(0,500), new Point (300,500)));
		baie2.adaugaPerete(new Perete(new Point(300,500), new Point (300,300)));
		baie2.adaugaPerete(new Perete(new Point(300,300), new Point (0,300)));
		baie2.adaugaPerete(new Perete(new Point(0,300), new Point (0,500)));
		baie2.adaugaUsa(new Usa(new Point(300,360), new Point (300,400)));
		baie2.adaugaGeam(new Geam(new Point(0,340), new Point (0,380)));
		
		bucatarie.adaugaPerete(new Perete(new Point(0,300), new Point (300,300)));
		bucatarie.adaugaPerete(new Perete(new Point(300,300), new Point (300,0)));
		bucatarie.adaugaPerete(new Perete(new Point(300,0), new Point (0,0)));
		bucatarie.adaugaPerete(new Perete(new Point(0,0), new Point (0,300)));
		bucatarie.adaugaUsa(new Usa(new Point(300,140), new Point (300,180)));
		bucatarie.adaugaGeam(new Geam(new Point(0,160), new Point (0,200)));
		
		hol.adaugaPerete(new Perete(new Point(300,500), new Point (600,500)));
		hol.adaugaPerete(new Perete(new Point(600,500), new Point (600,0)));
		hol.adaugaPerete(new Perete(new Point(600,0), new Point (300,0)));
		hol.adaugaPerete(new Perete(new Point(300,0), new Point (300,500)));
		hol.adaugaUsa(new Usa(new Point(300,140), new Point (300,180)));
		hol.adaugaUsa(new Usa(new Point(300,360), new Point (300,400)));
		hol.adaugaUsa(new Usa(new Point(400,0), new Point (440,0)));
		hol.adaugaUsa(new Usa(new Point(440,500), new Point (480,500)));
		hol.adaugaUsa(new Usa(new Point(340,500), new Point (380,500)));
		hol.adaugaUsa(new Usa(new Point(600,360), new Point (600,400)));
		hol.adaugaUsa(new Usa(new Point(600,140), new Point (600,180)));
		
		
		
	}
}
