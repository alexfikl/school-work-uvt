package main;

import java.sql.Connection;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import aparate.Bec;
import aparate.Calculator;
import aparate.Frigider;
import aparate.TV;
import aparate.masinaDeSpalat;
import bazeDeDate.PrelucrareDate;

import senzori.*;
import timer.HouseComponentBean;
import timer.MainTimer;
import timer.TimerBean;
import timer.TimerEvent;

public class Simulator {

	public static Connection connection = null;
	
	public static SenzorApa senzor_apa_buc=new SenzorApa();
	public static SenzorApa senzor_apa_baie1=new SenzorApa();
	public static SenzorApa senzor_apa_baie2=new SenzorApa();
	
	public static SenzorCurent senzor_curent=new SenzorCurent();
	
	public static SenzorGaz senzor_gaz=new SenzorGaz();
	
	public static SenzorTemperatura senzor_temperatura=new SenzorTemperatura();
	
	public static SenzorMiscare senzor_miscare_buc=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_baie1=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_baie2=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_living=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_dormitor1=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_dormitor2=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_dormitor3=new SenzorMiscare();
	public static SenzorMiscare senzor_miscare_hol=new SenzorMiscare();
	
	
	public Simulator()
	{
		connection=PrelucrareDate.connectToDB(PrelucrareDate.userName, PrelucrareDate.password, PrelucrareDate.url);
		PrelucrareDate.clearTable(connection, "scenariu");
		
	}
	public void startSimulator() throws InterruptedException
	{
		CreateOmuletiScenariu omuleti=new CreateOmuletiScenariu();
		TimerBean t=new TimerBean();
		HouseComponentBean hcb=new HouseComponentBean();//ceasul
		hcb.setName("House hours");
		MainTimer mainTimer=new MainTimer();
		
		
		senzor_apa_buc.setName("senzor apa bucatarie");
		senzor_apa_baie1.setName("senzor apa baie 1");
		senzor_apa_baie2.setName("senzor apa baie 2");
		
		senzor_curent.setName("senzor curent");
		
		senzor_gaz.setName("senzor gaz");
		
		senzor_temperatura.setName("senzor temperatura");
		
		senzor_miscare_buc.setName("senzor miscare bucatarie");
		senzor_miscare_baie1.setName("senzor miscare baie 1");
		senzor_miscare_baie2.setName("senzor miscare baie 2");
		senzor_miscare_living.setName("senzor miscare living");
		senzor_miscare_dormitor1.setName("senzor miscare dormitor 1");
		senzor_miscare_dormitor2.setName("senzor miscare dormitor 2");
		senzor_miscare_dormitor3.setName("senzor miscare dormitor 3");
		senzor_miscare_hol.setName("senzor miscare hol");
		
		senzor_curent.setPower(PrelucrareDate.getConsum(connection, "electricitate", "frigider", "pornit si raceste").get(0));
		
		/*Calculator calculator=new Calculator();
		calculator.setName("Calculator");
		Bec bec=new Bec();
		bec.setName("Bec");
		Frigider frigider=new Frigider();
		frigider.setName("Frigider");
		masinaDeSpalat masina=new masinaDeSpalat();
		masina.setName("Masina de spalat");
		TV tv=new TV();
		tv.setName("TV");*/
		
		
		//frigider.setPower();
		
		
		//0:0:0
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_apa_baie1.getName(), Simulator.senzor_apa_baie1.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_apa_baie2.getName(), Simulator.senzor_apa_baie2.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_apa_buc.getName(), Simulator.senzor_apa_buc.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_curent.getName(), Simulator.senzor_curent.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_gaz.getName(), Simulator.senzor_gaz.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_baie1.getName(), Simulator.senzor_miscare_baie1.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_baie2.getName(), Simulator.senzor_miscare_baie2.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_buc.getName(), Simulator.senzor_miscare_buc.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_dormitor1.getName(), Simulator.senzor_miscare_dormitor1.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_dormitor2.getName(), Simulator.senzor_miscare_dormitor2.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_dormitor3.getName(), Simulator.senzor_miscare_dormitor3.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_hol.getName(), Simulator.senzor_miscare_hol.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_miscare_living.getName(), Simulator.senzor_miscare_living.getPower());
		PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", "0:0:0", Simulator.senzor_temperatura.getName(), Simulator.senzor_temperatura.getPower());
		
		
		t.setRunning(true);
		t.addTimerListener(hcb);
		
		t.addTimerListener(mainTimer);
		
		t.addTimerListener(senzor_apa_buc);
		t.addTimerListener(senzor_apa_baie1);
		t.addTimerListener(senzor_apa_baie2);
		
		t.addTimerListener(senzor_curent);
		
		t.addTimerListener(senzor_temperatura);
		
		t.addTimerListener(senzor_gaz);
		
		t.addTimerListener(senzor_miscare_buc);
		t.addTimerListener(senzor_miscare_baie1);
		t.addTimerListener(senzor_miscare_baie2);
		t.addTimerListener(senzor_miscare_living);
		t.addTimerListener(senzor_miscare_dormitor1);
		t.addTimerListener(senzor_miscare_dormitor2);
		t.addTimerListener(senzor_miscare_dormitor3);
		t.addTimerListener(senzor_miscare_hol);
		
		/*t.addTimerListener(calculator);
		t.addTimerListener(bec);
		t.addTimerListener(frigider);
		t.addTimerListener(masina);
		t.addTimerListener(tv);
		*/
		
		////////////////////////////
		/*Thread.sleep(3000);
		t.addTimerListener(becul_1);
		Thread.sleep(5000);
		t.removeTimerListener(becul_1);*/
	}
	public void stopSimulator()
	{
		if (connection!=null)
			PrelucrareDate.closeDB(connection);
	}
	
}
