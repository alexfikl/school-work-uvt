package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CitireAux {
	public BufferedReader br;
	public ArrayList<String> lines;
	public String [] splitLines=null;
	public CitireAux(){
		try {
			br=new BufferedReader(new FileReader("auxiliar.txt"));
			lines=new ArrayList<String>();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void citire() throws IOException
	{
		String line;
		
		while((line=br.readLine())!=null)
		{
			lines.add(line);
		}
		br.close();
	}
	public int getHour(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		for (int j=0; j<splitLines.length; j++)
			if (splitLines[j].contains(":"))
				{
					time=splitLines[j].split(":");
					return Integer.parseInt(time[0]);
				}
		return -1;
				
	}
	public int getMinute(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		for (int j=0; j<splitLines.length; j++)
			if (splitLines[j].contains(":"))
				{
					time=splitLines[j].split(":");
					return Integer.parseInt(time[1]);
				}
		return -1;
	}
	public int getSeconde(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		for (int j=0; j<splitLines.length; j++)
			if (splitLines[j].contains(":"))
				{
					time=splitLines[j].split(":");
					return Integer.parseInt(time[2]);
				}
		return -1;
	}
	public int getHourSfarsit(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		time=splitLines[splitLines.length-1].split(":");
		return Integer.parseInt(time[0]);
				
	}
	public int getMinuteSfarsit(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		time=splitLines[splitLines.length-1].split(":");
		return Integer.parseInt(time[1]);
	}
	public int getSecondeSfarsit(int i)
	{
		String[] time;
		splitLines=lines.get(i).split(" ");
		time=splitLines[splitLines.length-1].split(":");
		return Integer.parseInt(time[2]);
	}
	public String getActiune(int i)
	{
		String[] actiune;
		actiune=lines.get(i).split(" ");
		/*if (actiune[2].equalsIgnoreCase("WC"))
			return actiune[0]+" "+actiune[1]+" "+actiune[2];*/
		return actiune[0];
	}
	public String getAtribut(int i)
	{
		String[] atribut;
		atribut=lines.get(i).split(" ");
		if (atribut[0].equalsIgnoreCase("merge_la_WC"))
			return atribut[2];
		if (atribut[0].equalsIgnoreCase("spala"))
			return atribut[2];
		if (atribut[0].equalsIgnoreCase("porneste")||atribut[0].equalsIgnoreCase("opreste")||atribut[0].equalsIgnoreCase("foloseste")||atribut[0].equalsIgnoreCase("seteaza"))
			if (atribut[2].equalsIgnoreCase("masina"))
				return atribut[2]+" "+atribut[3]+" "+atribut[4];
			else
				if (atribut[2].equalsIgnoreCase("aer"))
					return atribut[2]+" "+atribut[3];
				else
					return atribut[2];
		if (atribut[0].equalsIgnoreCase("merge"))
			return atribut[2];
		return null;
	}
	public String getSetare(int i)
	{
		String[] setare;
		setare=lines.get(i).split(" ");
		for (int k=0; k<setare.length; k++)
			if (setare[k].equalsIgnoreCase("intensitate") || setare[k].equalsIgnoreCase("putere"))
				return setare[k+1];
		return null;
	}
	public String getOmulet(int i)
	{
		String[] omulet;
		omulet=lines.get(i).split(" ");
		return omulet[1];
	}
}
