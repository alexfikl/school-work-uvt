package timer;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventObject;

public class TimerEvent extends EventObject {

	public static Date now=new Date(2010, 05, 15, 0,0,0);

	public TimerEvent(Object source) {
		super(source);
		
		
	}
	public Date getDate()
	{
		
		return now;
	}
	public String toString() {
		String ora;
		ora=now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
		return ora;
	}
	

}