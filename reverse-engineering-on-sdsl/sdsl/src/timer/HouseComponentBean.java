package timer;

import main.Simulator;
import bazeDeDate.PrelucrareDate;

public class HouseComponentBean implements java.io.Serializable,TimerListener {

	/* Properties */
	protected String name = null;
	private float power = 0;

	/* Empty Constructor */
	public HouseComponentBean() {}

	/* Getter and Setter Methods */
	public String getName() {
		return name;
	}

	public void setName(String s) {
		name = s;
	}

	public float getPower() {
		return power;
	}

	public void setPower(float i) {
		power = i;
	}

	@Override
	public void timeElapsed(TimerEvent evt) {
		// TODO Auto-generated method stub
		int zi=TimerEvent.now.getDay();
		//PrelucrareDate.insetIntoTable(Simulator.connection, "scenariu", evt.toString(), this.getName(), this.getPower());
		TimerEvent.now.setSeconds(TimerEvent.now.getSeconds()+30);
		if (zi+1==TimerEvent.now.getDay())
		{
			System.exit(1);
		}
		//System.out.println(this.name+"-"+evt.toString());
		
	}
}