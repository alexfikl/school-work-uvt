package timer;

import java.io.IOException;
import java.util.ArrayList;

import main.CitireAux;
import main.CreateOmuletiScenariu;
import main.Main;
import main.Simulator;
import InterfataGrafica.Interfata;
import bazeDeDate.PrelucrareDate;

public class MainTimer implements java.io.Serializable,TimerListener {

	int i,n;
	CitireAux aux;

	ArrayList<Float> consum_curent=new ArrayList<Float>();
	ArrayList<Integer> ora_sf=new ArrayList<Integer>();
	ArrayList<Integer> min_sf=new ArrayList<Integer>();
	ArrayList<Integer> sec_sf=new ArrayList<Integer>();
	ArrayList<String> ceFoloseste=new ArrayList<String>();
	String cameraBec=null;

	ArrayList<Float> consum_apa=new ArrayList<Float>();
	ArrayList<String> cameraConsum=new ArrayList<String>();
	ArrayList<Integer> ora_sfarsit=new ArrayList<Integer>();
	ArrayList<Integer> min_sfarsit=new ArrayList<Integer>();
	ArrayList<Integer> sec_sfarsit=new ArrayList<Integer>();
	ArrayList<String> ceSpala=new ArrayList<String>();

	ArrayList<Float> ultima_gaz=new ArrayList<Float>();
	ArrayList<Integer> ora_sf_gaz=new ArrayList<Integer>();
	ArrayList<Integer> min_sf_gaz=new ArrayList<Integer>();
	ArrayList<Integer> sec_sf_gaz=new ArrayList<Integer>();

	ArrayList<Float> ultima_curent=new ArrayList<Float>();

	ArrayList<Float> ultima_apa=new ArrayList<Float>();
	ArrayList<String> ultima_camera=new ArrayList<String>();

	float ultim_gaz=0;

	int ora_sf_mancare=-1;
	int min_sf_mancare=-1;
	int sec_sf_mancare=-1;
	
	int ora_aer=-1;
	int min_aer=-1;
	int sec_aer=-1;
	
	int ora_masina=-1;
	int min_masina=-1;
	int sec_masina=-1;
	String cameraOmulet=null;

	boolean aer_conditionat=false;

	/* Empty Constructor */
	public MainTimer() {
		aux=new CitireAux();
		i=0;
		try {
			aux.citire();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		n=aux.lines.size();
	}
	public String getCameraOmulet(String omulet)
	{
		String camera=null;
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
			camera=CreateOmuletiScenariu.omulet1.getPozitie();
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
				camera=CreateOmuletiScenariu.omulet2.getPozitie();
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
					camera=CreateOmuletiScenariu.omulet3.getPozitie();
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
						camera=CreateOmuletiScenariu.omulet4.getPozitie();
		return camera;
	}
	public void trezeste(String omulet)
	{
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
			CreateOmuletiScenariu.omulet1.setDoarme(false);
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
				CreateOmuletiScenariu.omulet2.setDoarme(false);
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
					CreateOmuletiScenariu.omulet3.setDoarme(false);
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
						CreateOmuletiScenariu.omulet4.setDoarme(false);
		String camera=getCameraOmulet(omulet);
		if (camera.equalsIgnoreCase("dormitor1"))
		{

			Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
		}
		else
			if (camera.equalsIgnoreCase("dormitor2"))
				Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
			else
				if (camera.equalsIgnoreCase("dormitor3"))
					Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
	}
	public void culca(String omulet)
	{
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
			CreateOmuletiScenariu.omulet1.setDoarme(true);
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
				CreateOmuletiScenariu.omulet2.setDoarme(true);
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
					CreateOmuletiScenariu.omulet3.setDoarme(true);
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
						CreateOmuletiScenariu.omulet4.setDoarme(true);
		String camera=getCameraOmulet(omulet);
		if (camera.equalsIgnoreCase("dormitor1"))
		{

			Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		}
		else
			if (camera.equalsIgnoreCase("dormitor2"))
				Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
			else
				if (camera.equalsIgnoreCase("dormitor3"))
					Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
	}
	public void mergeCamera(String omulet,ArrayList<String> camere)
	{
		for (int index=0; index<camere.size(); index++)
		{
			if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
			{
				CreateOmuletiScenariu.omulet1.setPozitie(camere.get(index));
			}
			else
				if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
				{
					CreateOmuletiScenariu.omulet2.setPozitie(camere.get(index));CreateOmuletiScenariu.omulet2.setPozitie("hol");		
				}
				else
					if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
					{
						CreateOmuletiScenariu.omulet3.setPozitie(camere.get(index));				
					}
					else
						if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
						{
							CreateOmuletiScenariu.omulet4.setPozitie(camere.get(index));
						}
			modificaSenzorMiscare(camere.get(index));

		}
	}
	public void modificaSenzorMiscare(String camera)
	{
		if (camera.equalsIgnoreCase("bucatarie"))
			Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
		else
			if (camera.equalsIgnoreCase("baie1"))
				Simulator.senzor_miscare_baie1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
			else
				if (camera.equalsIgnoreCase("baie2"))
					Simulator.senzor_miscare_baie2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
				else
					if (camera.equalsIgnoreCase("living"))
						Simulator.senzor_miscare_living.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
					else
						if (camera.equalsIgnoreCase("dormitor1"))
							Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
						else
							if (camera.equalsIgnoreCase("dormitor2"))
								Simulator.senzor_miscare_dormitor2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
							else
								if (camera.equalsIgnoreCase("dormitor3"))
									Simulator.senzor_miscare_dormitor3.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
								else
									if (camera.equalsIgnoreCase("hol"))
										Simulator.senzor_miscare_hol.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
	}
	public void setBec(String camera,boolean stare)
	{
		if (camera.equalsIgnoreCase("bucatarie"))
			Main.casa.becBuc.setStare(stare);
		else
			if (camera.equalsIgnoreCase("baie1"))
				Main.casa.becB1.setStare(stare);
			else
				if (camera.equalsIgnoreCase("baie2"))
					Main.casa.becB2.setStare(stare);
				else
					if (camera.equalsIgnoreCase("living"))
						Main.casa.becL.setStare(stare);
					else
						if (camera.equalsIgnoreCase("hol"))
							Main.casa.BecH.setStare(stare);
						else
							if (camera.equalsIgnoreCase("dormitor1"))
								Main.casa.BecD1.setStare(stare);
							else
								if (camera.equalsIgnoreCase("dormitor2"))
									Main.casa.BecD2.setStare(stare);
								else
									if (camera.equalsIgnoreCase("dormitor3"))
										Main.casa.BecD3.setStare(stare);
	}
	public void mergeInterfata (ArrayList<Integer> xx, ArrayList<Integer> yy,String omulet)
	{
		for (int i=0; i<xx.size();i++)
		{
			if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
			{
				Interfata.xDestOmulet1.add(xx.get(i));
				Interfata.yDestOmulet1.add(yy.get(i));
			}
			else
				if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
				{
					Interfata.xDestOmulet2.add(xx.get(i));
					Interfata.yDestOmulet2.add(yy.get(i));	
				}
				else
					if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
					{
						Interfata.xDestOmulet3.add(xx.get(i));
						Interfata.yDestOmulet3.add(yy.get(i));				
					}
					else
						if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
						{
							Interfata.xDestOmulet4.add(xx.get(i));
							Interfata.yDestOmulet4.add(yy.get(i));
						}
		}
	}
	public void pozitioneazaOmuletCamera(String camera, String omulet)
	{
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
		{
			if (camera.equalsIgnoreCase("bucatarie"))
			{
				Interfata.xDestOmulet1.add(100);
				Interfata.yDestOmulet1.add(200);
				
			}
			else
				if (camera.equalsIgnoreCase("baie2"))
				{
					Interfata.xDestOmulet1.add(50);
					Interfata.yDestOmulet1.add(400);
				}
				else
					if (camera.equalsIgnoreCase("living"))
					{
						Interfata.xDestOmulet1.add(50);
						Interfata.yDestOmulet1.add(600);
					}
					else
						if (camera.equalsIgnoreCase("baie1"))
						{
							Interfata.xDestOmulet1.add(450);
							Interfata.yDestOmulet1.add(650);
						}
						else
							if (camera.equalsIgnoreCase("dormitor1"))
							{
								Interfata.xDestOmulet1.add(850);
								Interfata.yDestOmulet1.add(650);
							}
							else
								if (camera.equalsIgnoreCase("dormitor2"))
								{
									Interfata.xDestOmulet1.add(800);
									Interfata.yDestOmulet1.add(350);
								}
								else
									if (camera.equalsIgnoreCase("dormitor3"))
									{
										Interfata.xDestOmulet1.add(750);
										Interfata.yDestOmulet1.add(100);
									}
									else
										if (camera.equalsIgnoreCase("hol"))
										{
											Interfata.xDestOmulet1.add(350);
											Interfata.yDestOmulet1.add(200);
										}
		}
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
			{
				if (camera.equalsIgnoreCase("bucatarie"))
				{
					Interfata.xDestOmulet2.add(50);
					Interfata.yDestOmulet2.add(200);
					
				}
				else
					if (camera.equalsIgnoreCase("baie2"))
					{
						Interfata.xDestOmulet2.add(50);
						Interfata.yDestOmulet2.add(450);
					}
					else
						if (camera.equalsIgnoreCase("living"))
						{
							Interfata.xDestOmulet2.add(100);
							Interfata.yDestOmulet2.add(600);
						}
						else
							if (camera.equalsIgnoreCase("baie1"))
							{
								Interfata.xDestOmulet2.add(500);
								Interfata.yDestOmulet2.add(650);
							}
							else
								if (camera.equalsIgnoreCase("dormitor1"))
								{
									Interfata.xDestOmulet2.add(900);
									Interfata.yDestOmulet2.add(650);
								}
								else
									if (camera.equalsIgnoreCase("dormitor2"))
									{
										Interfata.xDestOmulet2.add(850);
										Interfata.yDestOmulet2.add(350);
									}
									else
										if (camera.equalsIgnoreCase("dormitor3"))
										{
											Interfata.xDestOmulet2.add(800);
											Interfata.yDestOmulet2.add(100);
										}
										else
											if (camera.equalsIgnoreCase("hol"))
											{
												Interfata.xDestOmulet2.add(400);
												Interfata.yDestOmulet2.add(200);
											}
			}
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
				{
					if (camera.equalsIgnoreCase("bucatarie"))
					{
						Interfata.xDestOmulet3.add(150);
						Interfata.yDestOmulet3.add(50);
						
					}
					else
						if (camera.equalsIgnoreCase("baie2"))
						{
							Interfata.xDestOmulet3.add(100);
							Interfata.yDestOmulet3.add(450);
						}
						else
							if (camera.equalsIgnoreCase("living"))
							{
								Interfata.xDestOmulet3.add(150);
								Interfata.yDestOmulet3.add(600);
							}
							else
								if (camera.equalsIgnoreCase("baie1"))
								{
									Interfata.xDestOmulet3.add(550);
									Interfata.yDestOmulet3.add(650);
								}
								else
									if (camera.equalsIgnoreCase("dormitor1"))
									{
										Interfata.xDestOmulet3.add(800);
										Interfata.yDestOmulet3.add(700);
									}
									else
										if (camera.equalsIgnoreCase("dormitor2"))
										{
											Interfata.xDestOmulet3.add(850);
											Interfata.yDestOmulet3.add(400);
										}
										else
											if (camera.equalsIgnoreCase("dormitor3"))
											{
												Interfata.xDestOmulet3.add(850);
												Interfata.yDestOmulet3.add(100);
											}
											else
												if (camera.equalsIgnoreCase("hol"))
												{
													Interfata.xDestOmulet3.add(450);
													Interfata.yDestOmulet3.add(200);
												}			
				}
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
					{
						if (camera.equalsIgnoreCase("bucatarie"))
						{
							Interfata.xDestOmulet4.add(50);
							Interfata.yDestOmulet4.add(150);
							
						}
						else
							if (camera.equalsIgnoreCase("baie2"))
							{
								Interfata.xDestOmulet4.add(150);
								Interfata.yDestOmulet4.add(450);
							}
							else
								if (camera.equalsIgnoreCase("living"))
								{
									Interfata.xDestOmulet4.add(200);
									Interfata.yDestOmulet4.add(600);
								}
								else
									if (camera.equalsIgnoreCase("baie1"))
									{
										Interfata.xDestOmulet4.add(500);
										Interfata.yDestOmulet4.add(600);
									}
									else
										if (camera.equalsIgnoreCase("dormitor1"))
										{
											Interfata.xDestOmulet4.add(750);
											Interfata.yDestOmulet4.add(700);
										}
										else
											if (camera.equalsIgnoreCase("dormitor2"))
											{
												Interfata.xDestOmulet4.add(750);
												Interfata.yDestOmulet4.add(350);
											}
											else
												if (camera.equalsIgnoreCase("dormitor3"))
												{
													Interfata.xDestOmulet4.add(850);
													Interfata.yDestOmulet4.add(150);
												}
												else
													if (camera.equalsIgnoreCase("hol"))
													{
														Interfata.xDestOmulet4.add(500);
														Interfata.yDestOmulet4.add(200);
													}
					}
	}
	public void merge(int i,String omulet)
	{
		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		ArrayList<Integer> xx=new ArrayList<Integer>();
		ArrayList<Integer> yy=new ArrayList<Integer>();
		if (camera.equalsIgnoreCase("bucatarie"))
		{
			if (atribut.equalsIgnoreCase("baie2"))
			{
				ArrayList<String> camere=new ArrayList<String>();
				camere.add("bucatarie");
				camere.add("hol");
				camere.add("baie2");
				mergeCamera(omulet,camere);
				xx.add(150);
				yy.add(150);
				xx.add(350);
				yy.add(150);
				xx.add(350);
				yy.add(400);
				xx.add(150);
				yy.add(400);
				
				mergeInterfata(xx, yy, omulet);
				pozitioneazaOmuletCamera("baie2", omulet);

			}
			else
				if (atribut.equalsIgnoreCase("living"))
				{
					ArrayList<String> camere=new ArrayList<String>();
					camere.add("bucatarie");
					camere.add("hol");
					camere.add("living");
					mergeCamera(omulet,camere);
					xx.add(150);
					yy.add(150);
					xx.add(350);
					yy.add(150);
					xx.add(350);
					yy.add(650);
					
					mergeInterfata(xx, yy, omulet);
					pozitioneazaOmuletCamera("living", omulet);
				}
				else
					if (atribut.equalsIgnoreCase("baie1"))
					{
						ArrayList<String> camere=new ArrayList<String>();
						camere.add("bucatarie");
						camere.add("hol");
						camere.add("baie1");
						mergeCamera(omulet,camere);
						xx.add(150);
						yy.add(150);
						xx.add(500);
						yy.add(150);
						xx.add(500);
						yy.add(650);
						
						mergeInterfata(xx, yy, omulet);
						pozitioneazaOmuletCamera("baie1", omulet);
					}
					else
						if (atribut.equalsIgnoreCase("dormitor1"))
						{
							ArrayList<String> camere=new ArrayList<String>();
							camere.add("bucatarie");
							camere.add("hol");
							camere.add("dormitor2");
							camere.add("dormitor1");
							mergeCamera(omulet,camere);
							xx.add(150);
							yy.add(150);
							xx.add(500);
							yy.add(150);
							xx.add(500);
							yy.add(400);
							xx.add(800);
							yy.add(400);
							xx.add(800);
							yy.add(650);
							
							mergeInterfata(xx, yy, omulet);
							pozitioneazaOmuletCamera("dormitor1", omulet);
						}
						else
							if (atribut.equalsIgnoreCase("dormitor2"))
							{
								ArrayList<String> camere=new ArrayList<String>();
								camere.add("bucatarie");
								camere.add("hol");
								camere.add("dormitor2");
								mergeCamera(omulet,camere);	
								xx.add(150);
								yy.add(150);
								xx.add(500);
								yy.add(150);
								xx.add(500);
								yy.add(400);
								xx.add(800);
								yy.add(400);
								
								mergeInterfata(xx, yy, omulet);
								pozitioneazaOmuletCamera("dormitor2", omulet);
							}
							else
								if (atribut.equalsIgnoreCase("dormitor3"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("bucatarie");
									camere.add("hol");
									camere.add("dormitor3");
									mergeCamera(omulet,camere);
									xx.add(150);
									yy.add(150);
									xx.add(500);
									yy.add(150);
									xx.add(800);
									yy.add(150);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("dormitor3", omulet);
								}

								else
									if (atribut.equalsIgnoreCase("hol"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("bucatarie");
										camere.add("hol");
										mergeCamera(omulet,camere);
										xx.add(150);
										yy.add(150);
										xx.add(500);
										yy.add(150);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("hol", omulet);
									}
		}
		else
			if(camera.equalsIgnoreCase("baie2"))
			{
				if (atribut.equalsIgnoreCase("bucatarie"))
				{
					ArrayList<String> camere=new ArrayList<String>();
					camere.add("baie2");
					camere.add("hol");
					camere.add("bucatarie");
					mergeCamera(omulet,camere);
					xx.add(150);
					yy.add(400);
					xx.add(350);
					yy.add(400);
					xx.add(350);
					yy.add(150);
					xx.add(150);
					yy.add(150);
					
					mergeInterfata(xx, yy, omulet);
					pozitioneazaOmuletCamera("bucatarie", omulet);
				}
				else
					if (atribut.equalsIgnoreCase("living"))
					{
						ArrayList<String> camere=new ArrayList<String>();
						camere.add("baie2");
						camere.add("hol");
						camere.add("living");
						mergeCamera(omulet,camere);
						xx.add(150);
						yy.add(400);
						xx.add(350);
						yy.add(400);
						xx.add(350);
						yy.add(650);
						
						mergeInterfata(xx, yy, omulet);
						pozitioneazaOmuletCamera("living", omulet);
					}
					else
						if (atribut.equalsIgnoreCase("baie1"))
						{

							ArrayList<String> camere=new ArrayList<String>();
							camere.add("baie2");
							camere.add("hol");
							camere.add("baie1");
							mergeCamera(omulet,camere);
							xx.add(150);
							yy.add(400);
							xx.add(500);
							yy.add(400);
							xx.add(500);
							yy.add(650);
							
							mergeInterfata(xx, yy, omulet);
							pozitioneazaOmuletCamera("baie1", omulet);
						}
						else
							if (atribut.equalsIgnoreCase("dormitor1"))
							{
								ArrayList<String> camere=new ArrayList<String>();
								camere.add("baie2");
								camere.add("hol");
								camere.add("dormitor2");
								camere.add("dormitor1");
								mergeCamera(omulet,camere);
								xx.add(150);
								yy.add(400);
								xx.add(500);
								yy.add(400);
								xx.add(800);
								yy.add(400);
								xx.add(800);
								yy.add(650);
								
								mergeInterfata(xx, yy, omulet);
								pozitioneazaOmuletCamera("dormitor1", omulet);
							}
							else
								if (atribut.equalsIgnoreCase("dormitor2"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("baie2");
									camere.add("hol");
									camere.add("dormitor2");
									mergeCamera(omulet,camere);
									xx.add(150);
									yy.add(400);
									xx.add(500);
									yy.add(400);
									xx.add(800);
									yy.add(400);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("dormitor2", omulet);
								}
								else
									if (atribut.equalsIgnoreCase("dormitor3"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("baie2");
										camere.add("hol");
										camere.add("dormitor3");
										mergeCamera(omulet,camere);
										xx.add(150);
										yy.add(400);
										xx.add(350);
										yy.add(400);
										xx.add(350);
										yy.add(150);
										xx.add(800);
										yy.add(150);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("dormitor3", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("hol"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("baie2");
											camere.add("hol");
											mergeCamera(omulet,camere);
											xx.add(150);
											yy.add(400);
											xx.add(350);
											yy.add(400);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("hol", omulet);
										}
			}
			else
				if (camera.equalsIgnoreCase("living"))
				{
					if(atribut.equalsIgnoreCase("baie2"))
					{
						ArrayList<String> camere=new ArrayList<String>();
						camere.add("living");
						camere.add("hol");
						camere.add("baie2");
						mergeCamera(omulet,camere);
						xx.add(350);
						yy.add(650);
						xx.add(350);
						yy.add(400);
						xx.add(150);
						yy.add(400);
						
						mergeInterfata(xx, yy, omulet);
						pozitioneazaOmuletCamera("baie2", omulet);
					}
					else
						if (atribut.equalsIgnoreCase("bucatarie"))
						{
							ArrayList<String> camere=new ArrayList<String>();
							camere.add("living");
							camere.add("hol");
							camere.add("bucatarie");
							mergeCamera(omulet,camere);
							xx.add(350);
							yy.add(650);
							xx.add(350);
							yy.add(150);
							xx.add(150);
							yy.add(150);
							
							mergeInterfata(xx, yy, omulet);
							pozitioneazaOmuletCamera("bucatarie", omulet);
						}
						else
							if (atribut.equalsIgnoreCase("baie1"))
							{
								ArrayList<String> camere=new ArrayList<String>();
								camere.add("living");
								camere.add("baie1");
								mergeCamera(omulet,camere);
								xx.add(350);
								yy.add(650);
								xx.add(500);
								yy.add(650);
								
								mergeInterfata(xx, yy, omulet);
								pozitioneazaOmuletCamera("baie1", omulet);
							}
							else
								if (atribut.equalsIgnoreCase("dormitor1"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("living");
									camere.add("baie1");
									camere.add("dormitor1");
									mergeCamera(omulet,camere);
									xx.add(350);
									yy.add(650);
									xx.add(500);
									yy.add(650);
									xx.add(800);
									yy.add(650);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("dormitor1", omulet);
								}
								else
									if (atribut.equalsIgnoreCase("dormitor2"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("living");
										camere.add("hol");
										camere.add("domitor2");
										mergeCamera(omulet,camere);
										xx.add(350);
										yy.add(650);
										xx.add(350);
										yy.add(400);
										xx.add(800);
										yy.add(400);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("dormitor2", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("dormitor3"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("living");
											camere.add("hol");
											camere.add("dormitor3");
											mergeCamera(omulet,camere);
											xx.add(350);
											yy.add(650);
											xx.add(350);
											yy.add(150);
											xx.add(800);
											yy.add(150);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("dormitor3", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("hol"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("living");
												camere.add("hol");
												mergeCamera(omulet,camere);
												xx.add(350);
												yy.add(650);
												xx.add(350);
												yy.add(150);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("hol", omulet);

											}
				}
				else
					if (camera.equalsIgnoreCase("baie1"))
					{
						if (atribut.equalsIgnoreCase("baie2"))
						{
							ArrayList<String> camere=new ArrayList<String>();
							camere.add("baie1");
							camere.add("hol");
							camere.add("baie2");
							mergeCamera(omulet,camere);
							xx.add(500);
							yy.add(650);
							xx.add(500);
							yy.add(400);
							xx.add(150);
							yy.add(400);
							
							mergeInterfata(xx, yy, omulet);
							pozitioneazaOmuletCamera("baie2", omulet);
						}
						else
							if (atribut.equalsIgnoreCase("living"))
							{
								ArrayList<String> camere=new ArrayList<String>();
								camere.add("baie1");
								camere.add("living");
								mergeCamera(omulet,camere);
								xx.add(500);
								yy.add(650);
								xx.add(350);
								yy.add(650);
								
								mergeInterfata(xx, yy, omulet);
								pozitioneazaOmuletCamera("living", omulet);
							}
							else
								if (atribut.equalsIgnoreCase("bucatarie"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("baie1");
									camere.add("hol");
									camere.add("bucatarie");
									mergeCamera(omulet,camere);
									xx.add(500);
									yy.add(650);
									xx.add(500);
									yy.add(150);
									xx.add(150);
									yy.add(150);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("bucatarie", omulet);
								}
								else
									if (atribut.equalsIgnoreCase("dormitor1"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("baie1");
										camere.add("dormitor1");
										mergeCamera(omulet,camere);
										xx.add(500);
										yy.add(650);
										xx.add(800);
										yy.add(650);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("dormitor1", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("dormitor2"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("baie1");
											camere.add("hol");
											camere.add("dormitor2");
											mergeCamera(omulet,camere);
											xx.add(500);
											yy.add(650);
											xx.add(500);
											yy.add(400);
											xx.add(800);
											yy.add(400);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("dormitor2", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("dormitor3"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("baie1");
												camere.add("hol");
												camere.add("dormitor3");
												mergeCamera(omulet,camere);
												xx.add(500);
												yy.add(650);
												xx.add(500);
												yy.add(150);
												xx.add(800);
												yy.add(150);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("dormitor3", omulet);
											}
											else
												if (atribut.equalsIgnoreCase("hol"))
												{
													ArrayList<String> camere=new ArrayList<String>();
													camere.add("baie1");
													camere.add("hol");
													mergeCamera(omulet,camere);
													xx.add(500);
													yy.add(650);
													xx.add(500);
													yy.add(150);
													
													mergeInterfata(xx, yy, omulet);
													pozitioneazaOmuletCamera("hol", omulet);
												}
					}
					else
						if (camera.equalsIgnoreCase("dormitor1"))
						{
							if (atribut.equalsIgnoreCase("baie2"))
							{
								ArrayList<String> camere=new ArrayList<String>();
								camere.add("dormitor1");
								camere.add("dormitor2");
								camere.add("hol");
								camere.add("baie2");
								mergeCamera(omulet,camere);
								xx.add(800);
								yy.add(650);
								xx.add(800);
								yy.add(400);
								xx.add(500);
								yy.add(400);
								xx.add(150);
								yy.add(400);
								
								mergeInterfata(xx, yy, omulet);
								pozitioneazaOmuletCamera("baie2", omulet);
							}
							else
								if (atribut.equalsIgnoreCase("living"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("dormitor1");
									camere.add("baie1");
									camere.add("living");
									mergeCamera(omulet,camere);
									xx.add(800);
									yy.add(650);
									xx.add(500);
									yy.add(650);
									xx.add(350);
									yy.add(650);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("living", omulet);
								}
								else
									if (atribut.equalsIgnoreCase("baie1"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("dormitor1");
										camere.add("baie1");
										mergeCamera(omulet,camere);
										xx.add(800);
										yy.add(650);
										xx.add(500);
										yy.add(650);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("baie1", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("bucatarie"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("dormitor1");
											camere.add("dormitor2");
											camere.add("hol");
											camere.add("bucatarie");
											mergeCamera(omulet,camere);
											xx.add(800);
											yy.add(650);
											xx.add(800);
											yy.add(400);
											xx.add(350);
											yy.add(400);
											xx.add(350);
											yy.add(150);
											xx.add(150);
											yy.add(150);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("bucatarie", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("dormitor2"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("dormitor1");
												camere.add("dormitor2");
												mergeCamera(omulet,camere);
												xx.add(800);
												yy.add(650);
												xx.add(800);
												yy.add(400);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("dormitor2", omulet);
											}
											else
												if (atribut.equalsIgnoreCase("dormitor3"))
												{
													ArrayList<String> camere=new ArrayList<String>();
													camere.add("dormitor1");
													camere.add("dormitor2");
													camere.add("hol");
													camere.add("dormitor3");
													mergeCamera(omulet,camere);
													xx.add(800);
													yy.add(650);
													xx.add(800);
													yy.add(400);
													xx.add(500);
													yy.add(400);
													xx.add(500);
													yy.add(150);
													xx.add(800);
													yy.add(150);
													
													mergeInterfata(xx, yy, omulet);
													pozitioneazaOmuletCamera("dormitor3", omulet);
												}
												else
													if (atribut.equalsIgnoreCase("hol"))
													{
														ArrayList<String> camere=new ArrayList<String>();
														camere.add("dormitor1");
														camere.add("dormitor2");
														camere.add("hol");
														mergeCamera(omulet,camere);
														xx.add(800);
														yy.add(650);
														xx.add(800);
														yy.add(400);
														xx.add(350);
														yy.add(400);
														
														mergeInterfata(xx, yy, omulet);
														pozitioneazaOmuletCamera("hol", omulet);
													}
						}
						else
							if (camera.equalsIgnoreCase("dormitor2"))
							{
								if (atribut.equalsIgnoreCase("baie2"))
								{
									ArrayList<String> camere=new ArrayList<String>();
									camere.add("dormitor2");
									camere.add("hol");
									camere.add("baie2");
									mergeCamera(omulet,camere);
									xx.add(800);
									yy.add(400);
									xx.add(350);
									yy.add(400);
									xx.add(150);
									yy.add(400);
									
									mergeInterfata(xx, yy, omulet);
									pozitioneazaOmuletCamera("baie2", omulet);
								}
								else
									if (atribut.equalsIgnoreCase("living"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("dormitor2");
										camere.add("hol");
										camere.add("living");
										mergeCamera(omulet,camere);
										xx.add(800);
										yy.add(400);
										xx.add(350);
										yy.add(400);
										xx.add(350);
										yy.add(650);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("living", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("baie1"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("dormitor2");
											camere.add("hol");
											camere.add("baie1");
											mergeCamera(omulet,camere);
											xx.add(800);
											yy.add(400);
											xx.add(500);
											yy.add(400);
											xx.add(500);
											yy.add(650);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("baie1", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("dormitor1"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("dormitor2");
												camere.add("dormitor1");
												mergeCamera(omulet,camere);
												xx.add(800);
												yy.add(400);
												xx.add(800);
												yy.add(650);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("dormitor1", omulet);
											}
											else
												if (atribut.equalsIgnoreCase("bucatarie"))
												{
													ArrayList<String> camere=new ArrayList<String>();
													camere.add("dormitor2");
													camere.add("hol");
													camere.add("bucatarie");
													mergeCamera(omulet,camere);
													xx.add(800);
													yy.add(400);
													xx.add(350);
													yy.add(400);
													xx.add(350);
													yy.add(150);
													xx.add(150);
													yy.add(150);
													
													mergeInterfata(xx, yy, omulet);
													pozitioneazaOmuletCamera("bucatarie", omulet);
												}
												else
													if (atribut.equalsIgnoreCase("dormitor3"))
													{
														ArrayList<String> camere=new ArrayList<String>();
														camere.add("dormitor2");
														camere.add("hol");
														camere.add("dormitor3");
														mergeCamera(omulet,camere);
														xx.add(800);
														yy.add(400);
														xx.add(500);
														yy.add(400);
														xx.add(500);
														yy.add(150);
														xx.add(800);
														yy.add(150);
														
														mergeInterfata(xx, yy, omulet);
														pozitioneazaOmuletCamera("dormitor3", omulet);
													}
													else
														if (atribut.equalsIgnoreCase("hol"))
														{
															ArrayList<String> camere=new ArrayList<String>();
															camere.add("dormitor2");
															camere.add("hol");
															mergeCamera(omulet,camere);
															xx.add(800);
															yy.add(400);
															xx.add(350);
															yy.add(400);
															
															mergeInterfata(xx, yy, omulet);
															pozitioneazaOmuletCamera("hol", omulet);
														}
							}
							else
								if (camera.equalsIgnoreCase("dormitor3"))
								{
									if (atribut.equalsIgnoreCase("baie2"))
									{
										ArrayList<String> camere=new ArrayList<String>();
										camere.add("dormitor3");
										camere.add("hol");
										camere.add("baie2");
										mergeCamera(omulet,camere);
										xx.add(800);
										yy.add(150);
										xx.add(350);
										yy.add(150);
										xx.add(350);
										yy.add(400);
										xx.add(150);
										yy.add(400);
										
										mergeInterfata(xx, yy, omulet);
										pozitioneazaOmuletCamera("baie2", omulet);
									}
									else
										if (atribut.equalsIgnoreCase("living"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("dormitor3");
											camere.add("hol");
											camere.add("living");
											mergeCamera(omulet,camere);
											xx.add(800);
											yy.add(150);
											xx.add(350);
											yy.add(150);
											xx.add(350);
											yy.add(650);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("living", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("baie1"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("dormitor3");
												camere.add("hol");
												camere.add("baie1");
												mergeCamera(omulet,camere);
												xx.add(800);
												yy.add(150);
												xx.add(500);
												yy.add(150);
												xx.add(500);
												yy.add(650);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("baie1", omulet);
											}
											else
												if (atribut.equalsIgnoreCase("dormitor1"))
												{
													ArrayList<String> camere=new ArrayList<String>();
													camere.add("dormitor3");
													camere.add("hol");
													camere.add("dormitor2");
													camere.add("dormitor1");
													mergeCamera(omulet,camere);
													xx.add(800);
													yy.add(150);
													xx.add(500);
													yy.add(150);
													xx.add(500);
													yy.add(400);
													xx.add(800);
													yy.add(400);
													xx.add(800);
													yy.add(650);
													
													mergeInterfata(xx, yy, omulet);
													pozitioneazaOmuletCamera("dormitor1", omulet);
												}
												else
													if (atribut.equalsIgnoreCase("dormitor2"))
													{
														ArrayList<String> camere=new ArrayList<String>();
														camere.add("dormitor3");
														camere.add("hol");
														camere.add("dormitor2");
														mergeCamera(omulet,camere);
														xx.add(800);
														yy.add(150);
														xx.add(500);
														yy.add(150);
														xx.add(500);
														yy.add(400);
														xx.add(800);
														yy.add(400);
														
														mergeInterfata(xx, yy, omulet);
														pozitioneazaOmuletCamera("dormitor2", omulet);
													}
													else
														if (atribut.equalsIgnoreCase("bucatarie"))
														{
															ArrayList<String> camere=new ArrayList<String>();
															camere.add("dormitor3");
															camere.add("hol");
															camere.add("bucatarie");
															mergeCamera(omulet,camere);
															xx.add(800);
															yy.add(150);
															xx.add(350);
															yy.add(150);
															xx.add(150);
															yy.add(150);
															
															mergeInterfata(xx, yy, omulet);
															pozitioneazaOmuletCamera("bucatarie", omulet);
															System.out
																	.println(Interfata.xDestOmulet4);
															System.out
																	.println(Interfata.yDestOmulet4);
														}
														else
															if (atribut.equalsIgnoreCase("hol"))
															{
																ArrayList<String> camere=new ArrayList<String>();
																camere.add("dormitor3");
																camere.add("hol");
																mergeCamera(omulet,camere);
																xx.add(800);
																yy.add(150);
																xx.add(350);
																yy.add(150);
																
																mergeInterfata(xx, yy, omulet);
																pozitioneazaOmuletCamera("hol", omulet);
															}
								}
								else
									if (camera.equalsIgnoreCase("hol"))
									{
										if (atribut.equalsIgnoreCase("baie2"))
										{
											ArrayList<String> camere=new ArrayList<String>();
											camere.add("hol");
											camere.add("baie2");
											mergeCamera(omulet,camere);
											xx.add(350);
											yy.add(400);
											xx.add(150);
											yy.add(400);
											
											mergeInterfata(xx, yy, omulet);
											pozitioneazaOmuletCamera("baie2", omulet);
										}
										else
											if (atribut.equalsIgnoreCase("living"))
											{
												ArrayList<String> camere=new ArrayList<String>();
												camere.add("hol");
												camere.add("living");
												mergeCamera(omulet,camere);
												xx.add(350);
												yy.add(400);
												xx.add(350);
												yy.add(650);
												
												mergeInterfata(xx, yy, omulet);
												pozitioneazaOmuletCamera("living", omulet);
											}
											else
												if (atribut.equalsIgnoreCase("baie1"))
												{
													ArrayList<String> camere=new ArrayList<String>();
													camere.add("hol");
													camere.add("baie1");
													mergeCamera(omulet,camere);
													xx.add(500);
													yy.add(400);
													xx.add(500);
													yy.add(650);
													
													mergeInterfata(xx, yy, omulet);
													pozitioneazaOmuletCamera("baie1", omulet);
												}
												else
													if (atribut.equalsIgnoreCase("dormitor1"))
													{
														ArrayList<String> camere=new ArrayList<String>();
														camere.add("hol");
														camere.add("dormitor2");
														camere.add("dormitor1");
														mergeCamera(omulet,camere);
														xx.add(500);
														yy.add(400);
														xx.add(800);
														yy.add(400);
														xx.add(800);
														yy.add(650);
														
														mergeInterfata(xx, yy, omulet);
														pozitioneazaOmuletCamera("dormitor1", omulet);
													}
													else
														if (atribut.equalsIgnoreCase("dormitor2"))
														{
															ArrayList<String> camere=new ArrayList<String>();
															camere.add("hol");
															camere.add("dormitor2");
															mergeCamera(omulet,camere);
															xx.add(500);
															yy.add(400);
															xx.add(800);
															yy.add(400);
															
															mergeInterfata(xx, yy, omulet);
															pozitioneazaOmuletCamera("dormitor2", omulet);
														}
														else
															if (atribut.equalsIgnoreCase("dormitor3"))
															{
																ArrayList<String> camere=new ArrayList<String>();
																camere.add("hol");
																camere.add("dormitor3");
																mergeCamera(omulet,camere);
																xx.add(500);
																yy.add(150);
																xx.add(800);
																yy.add(150);
																
																mergeInterfata(xx, yy, omulet);
																pozitioneazaOmuletCamera("dormitor3", omulet);
															}
															else
																if (atribut.equalsIgnoreCase("bucatarie"))
																{
																	ArrayList<String> camere=new ArrayList<String>();
																	camere.add("hol");
																	camere.add("bucatarie");
																	mergeCamera(omulet,camere);
																	xx.add(350);
																	yy.add(150);
																	xx.add(150);
																	yy.add(150);
																	
																	mergeInterfata(xx, yy, omulet);
																	pozitioneazaOmuletCamera("bucatarie", omulet);
																}
									}
	}
	public void pleaca(String omulet)
	{
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
		{
			CreateOmuletiScenariu.omulet1.setPozitie("hol");
			Interfata.xDestOmulet1.add(450);
			Interfata.yDestOmulet1.add(150);
			Interfata.xDestOmulet1.add(450);
			Interfata.yDestOmulet1.add(-50);
			CreateOmuletiScenariu.omulet1.setPlecat(true);
		}
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
			{
				CreateOmuletiScenariu.omulet2.setPozitie("hol");
				Interfata.xDestOmulet2.add(450);
				Interfata.yDestOmulet2.add(150);
				Interfata.xDestOmulet2.add(450);
				Interfata.yDestOmulet2.add(-50);
				CreateOmuletiScenariu.omulet2.setPlecat(true);
			}
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
				{
					CreateOmuletiScenariu.omulet3.setPozitie("hol");
					Interfata.xDestOmulet3.add(450);
					Interfata.yDestOmulet3.add(150);
					Interfata.xDestOmulet3.add(450);
					Interfata.yDestOmulet3.add(-50);
					CreateOmuletiScenariu.omulet3.setPlecat(true);
				}
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
					{
						CreateOmuletiScenariu.omulet4.setPozitie("hol");
						Interfata.xDestOmulet4.add(450);
						Interfata.yDestOmulet4.add(150);
						Interfata.xDestOmulet4.add(450);
						Interfata.yDestOmulet4.add(-50);
						CreateOmuletiScenariu.omulet4.setPlecat(true);
					}
		if (CreateOmuletiScenariu.omulet1.isPlecat() && CreateOmuletiScenariu.omulet2.isPlecat() && CreateOmuletiScenariu.omulet3.isPlecat() && CreateOmuletiScenariu.omulet4.isPlecat())
		{
			Interfata.TIMER_INTERVAL=10;
			TimerBean.interval=10;
		}
	
		
	}
	public void vine(String omulet)
	{
		Interfata.TIMER_INTERVAL=1000;
		TimerBean.interval=1000;
		
//		Interfata.TIMER_INTERVAL=3000;
//		TimerBean.interval=3000;
		
		/*Interfata.TIMER_INTERVAL=100;	
		TimerBean.interval=100;*/
		
		
		if (CreateOmuletiScenariu.omulet1.getNume().equalsIgnoreCase(omulet))
		{
			CreateOmuletiScenariu.omulet1.setPozitie("hol");
			Interfata.xDestOmulet1.add(450);
			Interfata.yDestOmulet1.add(150);
			pozitioneazaOmuletCamera("hol", omulet);
			CreateOmuletiScenariu.omulet1.setPlecat(false);
		}
		else
			if (CreateOmuletiScenariu.omulet2.getNume().equalsIgnoreCase(omulet))
			{
				CreateOmuletiScenariu.omulet2.setPozitie("hol");
				Interfata.xDestOmulet2.add(450);
				Interfata.yDestOmulet2.add(150);
				pozitioneazaOmuletCamera("hol", omulet);
				CreateOmuletiScenariu.omulet2.setPlecat(false);
			}
			else
				if (CreateOmuletiScenariu.omulet3.getNume().equalsIgnoreCase(omulet))
				{
					CreateOmuletiScenariu.omulet3.setPozitie("hol");
					Interfata.xDestOmulet3.add(450);
					Interfata.yDestOmulet3.add(150);
					pozitioneazaOmuletCamera("hol", omulet);
					CreateOmuletiScenariu.omulet3.setPlecat(false);
				}
				else
					if (CreateOmuletiScenariu.omulet4.getNume().equalsIgnoreCase(omulet))
					{
						CreateOmuletiScenariu.omulet4.setPozitie("hol");
						Interfata.xDestOmulet4.add(450);
						Interfata.yDestOmulet4.add(150);
						pozitioneazaOmuletCamera("hol", omulet);
						CreateOmuletiScenariu.omulet4.setPlecat(false);
					}
		Simulator.senzor_miscare_hol.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
	}
	public void merge_la_WC(int i,String omulet)
	{
		String camera=getCameraOmulet(omulet);
		ora_sfarsit.add(aux.getHourSfarsit(i));
		min_sfarsit.add(aux.getMinuteSfarsit(i));
		sec_sfarsit.add(aux.getSecondeSfarsit(i));
		cameraConsum.add(camera);
		ceSpala.add("wc");
		if (camera.equalsIgnoreCase("baie1"))
		{
			float consum_total=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "WC");
			consum_apa.add(consum_total);
			Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()+consum_total);
			Simulator.senzor_miscare_baie1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
			Main.casa.wc1.setStare(true);
		}
		else
			if (camera.equalsIgnoreCase("baie2"))
			{
				float consum_total=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "WC");
				consum_apa.add(consum_total);
				Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()+consum_total);
				Simulator.senzor_miscare_baie2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
				Main.casa.wc2.setStare(true);
			}
	}
	public void spala(int i,String omulet)
	{
		ora_sfarsit.add(aux.getHourSfarsit(i));
		min_sfarsit.add(aux.getMinuteSfarsit(i));
		sec_sfarsit.add(aux.getSecondeSfarsit(i));
		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		cameraConsum.add(camera);
		ceSpala.add(atribut);
		if (camera.equalsIgnoreCase("baie1"))
		{
			float consum_total=PrelucrareDate.getConsumApa(Simulator.connection, "apa", atribut);
			consum_apa.add(consum_total);
			Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()+consum_total);
			Simulator.senzor_miscare_baie1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
			if (atribut.equalsIgnoreCase("dus") || atribut.equalsIgnoreCase("picioare"))
				Main.casa.robinetVana.setStare(true);
			else
				Main.casa.robinetBaie1.setStare(true);
		}
		else
			if (camera.equalsIgnoreCase("baie2"))
			{
				float consum_total=PrelucrareDate.getConsumApa(Simulator.connection, "apa", atribut);
				consum_apa.add(consum_total);
				Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()+consum_total);
				Simulator.senzor_miscare_baie2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
				Main.casa.robinetB2.setStare(true);
			}
		if (atribut.equalsIgnoreCase("haine"))
		{
			ora_sf.add(aux.getHourSfarsit(i));
			min_sf.add(aux.getMinuteSfarsit(i));
			sec_sf.add(aux.getSecondeSfarsit(i));
			consum_curent.add(PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0));
			Simulator.senzor_curent.setPower(PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0));
			Main.casa.masina.setStare(true);
			ceFoloseste.add("masina de spalaat");
		}
	}
	public void porneste(int i,String omulet)
	{
		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		if (atribut.equalsIgnoreCase("aragaz"))
		{
			Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()+(float)3.5);
			Main.casa.aragaz.setStare(true);

		}
		else
			if (atribut.equalsIgnoreCase("aer conditionat"))
			{
				float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "aer conditionat", "-").get(0);
				Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
				aer_conditionat=true;
				//raceste cu 5 grade
				Simulator.senzor_temperatura.setPower(Simulator.senzor_temperatura.getPower()-5);
				Main.casa.aer.setStare(true);
			}
			else
				if (atribut.equalsIgnoreCase("calculator"))
				{
					float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "computer", "pornit").get(0);
					Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
					Main.casa.calcL.setStare(true);
				}
				else
					if (atribut.equalsIgnoreCase("televizor"))
					{
						float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate normala)").get(0);
						Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
						Main.casa.tv.setStare(true);
					}
					else
						if (atribut.equalsIgnoreCase("bec"))
						{
							float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "veioza", "pornit").get(0);
							Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
							setBec(camera, true);
						}
						else
							if (atribut.equalsIgnoreCase("masina de spalat"))
							{
								float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0);
								Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
								consum=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "haine");
								if (camera.equalsIgnoreCase("baie1"))
									Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()+consum);
								else
									if (camera.equalsIgnoreCase("baie2"))
									{
										Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()+consum);
										Main.casa.masina.setStare(true);
									}
							}
		modificaSenzorMiscare(camera);
	}
	public void opreste(int i,String omulet)
	{
		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		if (atribut.equalsIgnoreCase("aragaz"))
		{
			Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()-(float)3.5);
			Main.casa.aragaz.setStare(false);
		}
		else
			if (atribut.equalsIgnoreCase("aer conditionat"))
			{
				float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "aer conditionat", "-").get(0);
				Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum);
				aer_conditionat=false;
				//reincalzeste cu 5 grade
				Simulator.senzor_temperatura.setPower(Simulator.senzor_temperatura.getPower()+5);
				Main.casa.aer.setStare(false);
			}
			else
				if (atribut.equalsIgnoreCase("calculator"))
				{
					float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "computer", "pornit").get(0);
					Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum);
					Main.casa.calcL.setStare(false);
				}
				else
					if (atribut.equalsIgnoreCase("televizor"))
					{
						float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate normala)").get(0);
						Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum);
						Main.casa.tv.setStare(false);
					}
					else
						if (atribut.equalsIgnoreCase("bec"))
						{
							float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "veioza", "pornit").get(0);
							Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum);
							setBec(camera, false);
						}
						else
							if (atribut.equalsIgnoreCase("masina de spalat"))
							{
								float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0);
								Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum);
								consum=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "haine");
								if (camera.equalsIgnoreCase("baie1"))
									Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()-consum);
								else
									if (camera.equalsIgnoreCase("baie2"))
									{
										Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()-consum);
										Main.casa.masina.setStare(false);
									}
							}
		modificaSenzorMiscare(camera);
	}
	public void foloseste(int i,String omulet)
	{

		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		ceFoloseste.add(atribut);
		if (atribut.equalsIgnoreCase("aragaz"))
		{
			ora_sf_gaz.add(aux.getHourSfarsit(i));
			min_sf_gaz.add(aux.getMinuteSfarsit(i));
			sec_sf_gaz.add(aux.getSecondeSfarsit(i));
			Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()+(float)3.5);
			Main.casa.aragaz.setStare(true);
		}
		else
			if (atribut.equalsIgnoreCase("aer conditionat"))
			{

				float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "aer conditionat", "-").get(0);
				ora_sf.add(aux.getHourSfarsit(i));
				min_sf.add(aux.getMinuteSfarsit(i));
				sec_sf.add(aux.getSecondeSfarsit(i));
				consum_curent.add(consum);
				Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
				ora_aer=aux.getHourSfarsit(i);
				min_aer=aux.getMinuteSfarsit(i);
				sec_aer=aux.getSecondeSfarsit(i);
				Simulator.senzor_temperatura.setPower(Simulator.senzor_temperatura.getPower()-5);
				Main.casa.aer.setStare(true);
			}
			else
				if (atribut.equalsIgnoreCase("calculator"))
				{
					float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "computer", "pornit").get(0);
					ora_sf.add(aux.getHourSfarsit(i));
					min_sf.add(aux.getMinuteSfarsit(i));
					sec_sf.add(aux.getSecondeSfarsit(i));
					consum_curent.add(consum);
					Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
					Main.casa.calcL.setStare(true);
				}
				else
					if (atribut.equalsIgnoreCase("televizor"))
					{
						float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate normala)").get(0);
						ora_sf.add(aux.getHourSfarsit(i));
						min_sf.add(aux.getMinuteSfarsit(i));
						sec_sf.add(aux.getSecondeSfarsit(i));
						consum_curent.add(consum);
						Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
						Main.casa.tv.setStare(true);
					}
					else
						if (atribut.equalsIgnoreCase("bec"))
						{
							float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "veioza", "pornit").get(0);
							ora_sf.add(aux.getHourSfarsit(i));
							min_sf.add(aux.getMinuteSfarsit(i));
							sec_sf.add(aux.getSecondeSfarsit(i));
							consum_curent.add(consum);
							Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
							setBec(camera, true);
							cameraBec=camera;
						}
						else
							if (atribut.equalsIgnoreCase("masina de spalat"))
							{
								float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0);
								ora_sf.add(aux.getHourSfarsit(i));
								min_sf.add(aux.getMinuteSfarsit(i));
								sec_sf.add(aux.getSecondeSfarsit(i));
								consum_curent.add(consum);
								Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
								consum=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "haine");
								ora_masina=aux.getHourSfarsit(i);
								min_masina=aux.getMinuteSfarsit(i);
								sec_masina=aux.getSecondeSfarsit(i);
								cameraOmulet=camera;
								if (camera.equalsIgnoreCase("baie1"))
									Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()+consum);
								else
									if (camera.equalsIgnoreCase("baie2"))
									{
										Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()+consum);
										Main.casa.masina.setStare(true);
									}
								
							}
							else
								if (atribut.equalsIgnoreCase("frigider"))
								{

									float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "frigider", "pornit, nu raceste cu usa deschisa").get(0);
									ora_sf.add(aux.getHourSfarsit(i));
									min_sf.add(aux.getMinuteSfarsit(i));
									sec_sf.add(aux.getSecondeSfarsit(i));
									consum_curent.add(consum);
									Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
									Main.casa.frigi.setStare(true);
								}
		modificaSenzorMiscare(camera);
	}
	public void seteaza(int i,String omulet)
	{
		String camera=getCameraOmulet(omulet);
		String atribut=aux.getAtribut(i);
		String setare=aux.getSetare(i);
		if (atribut.equalsIgnoreCase("aragaz"))
		{
			ultim_gaz=(float)3.5;
			Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()+(float)3.5);
			Main.casa.aragaz.setStare(true);
		}
		else
			if (atribut.equalsIgnoreCase("aer conditionat"))
			{
				float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "aer conditionat", "-").get(0);
				ultima_curent.add(consum);
				Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
				Main.casa.aer.setStare(true);
			}
			else
				if (atribut.equalsIgnoreCase("calculator"))
				{
					float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "computer", "pornit").get(0);
					ultima_curent.add(consum);
					Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
					Main.casa.calcL.setStare(true);
				}
				else
					if (atribut.equalsIgnoreCase("televizor"))
					{
						float consum;
						if (setare.equalsIgnoreCase("mica"))
							consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate mica)").get(0);
						else
							if (setare.equalsIgnoreCase("mare"))
								consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate mare)").get(0);
							else
								consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "televizor", "pornit(imagine luminozitate normala)").get(0);
						ultima_curent.add(consum);
						Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
						Main.casa.tv.setStare(true);
					}
					else
						if (atribut.equalsIgnoreCase("bec"))
						{
							float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "veioza", "pornit").get(0);
							ultima_curent.add(consum);
							Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
							setBec(camera, true);
						}
						else
							if (atribut.equalsIgnoreCase("masina de spalat"))
							{
								float consum=PrelucrareDate.getConsum(Simulator.connection, "electricitate", "masina de spalat vase", "-").get(0);
								ultima_curent.add(consum);
								Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()+consum);
								Main.casa.masina.setStare(true);
							}
		modificaSenzorMiscare(camera);
	}
	public void prelucreaza(int i)
	{
		String actiune=aux.getActiune(i);
		String omulet=aux.getOmulet(i);
		if (actiune.equalsIgnoreCase("trezeste"))
		{
			trezeste(omulet);
		}
		else
			if (actiune.equalsIgnoreCase("culca"))
			{
				culca(omulet);
			}
			else
				if (actiune.equalsIgnoreCase("merge"))
				{
					merge(i,omulet);
				}
				else
					if (actiune.equalsIgnoreCase("pleaca"))
					{
						pleaca(omulet);
					}
					else 
						if (actiune.equalsIgnoreCase("vine"))
						{
							vine(omulet);
						}
						else
							if (actiune.equalsIgnoreCase("mananca"))
							{
								ora_sf_mancare=aux.getHourSfarsit(i);
								min_sf_mancare=aux.getMinuteSfarsit(i);
								sec_sf_mancare=aux.getSecondeSfarsit(i);

								Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
							}
							else
								if (actiune.equalsIgnoreCase("bea"))
								{
									float consum=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "bea");
									ultima_apa.add(consum);
									ultima_camera.add("bucatarie");
									Simulator.senzor_apa_buc.setPower(Simulator.senzor_apa_buc.getPower()+consum);
									Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
									Main.casa.robinetBuc.setStare(true);
								}
								else
									if (actiune.equalsIgnoreCase("gateste"))
									{
										Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()+(float)3.5);
										ultim_gaz=(float)3.5;
										Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
										Main.casa.aragaz.setStare(true);
									}
									else
										if (actiune.equalsIgnoreCase("merge_la_WC"))
										{
											merge_la_WC(i,omulet);
										}
										else
											if (actiune.equalsIgnoreCase("spala"))
											{
												spala(i,omulet);

											}
											else
												if (actiune.equalsIgnoreCase("porneste"))
												{
													porneste(i,omulet);
												}
												else 
													if (actiune.equalsIgnoreCase("opreste"))
													{
														opreste(i,omulet);
													}
													else
														if (actiune.equalsIgnoreCase("foloseste"))
														{
															foloseste(i,omulet);
														}
														else
															if (actiune.equalsIgnoreCase("seteaza"))
															{
																seteaza(i,omulet);
															}
	}
	public void setViteza()
	{
		if ((CreateOmuletiScenariu.omulet1.isPlecat()||CreateOmuletiScenariu.omulet1.isDoarme())&& (CreateOmuletiScenariu.omulet2.isPlecat()||CreateOmuletiScenariu.omulet2.isDoarme()) && (CreateOmuletiScenariu.omulet3.isPlecat()||CreateOmuletiScenariu.omulet3.isDoarme())&&(CreateOmuletiScenariu.omulet4.isPlecat()||CreateOmuletiScenariu.omulet4.isDoarme()))
		{
			Interfata.TIMER_INTERVAL=1;
			TimerBean.interval=1;
		}
		else
		{
			Interfata.TIMER_INTERVAL=1000;
			TimerBean.interval=1000;
			/*Interfata.TIMER_INTERVAL=3000;
			TimerBean.interval=3000;*/
			/*Interfata.TIMER_INTERVAL=100;	
			TimerBean.interval=100;*/
		}
	}

	@Override
	public void timeElapsed(TimerEvent evt) {
		// TODO Auto-generated method stub
		int ora=TimerEvent.now.getHours();
		int min=TimerEvent.now.getMinutes();
		int sec=TimerEvent.now.getSeconds();

		Simulator.senzor_miscare_baie1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_baie2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_living.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_dormitor1.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_dormitor2.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_dormitor3.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
		Simulator.senzor_miscare_hol.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));

		//System.out.println("ora:"+ora+":"+min+":"+sec);

		//intre 0-7 temperatura=20 grade, in rest temperatura=25 grade
		if (!aer_conditionat)
			if (ora<7)
				Simulator.senzor_temperatura.setPower(20);
			else
				Simulator.senzor_temperatura.setPower(23);
		if (i<n)
		{
			setViteza();
			System.out.println(aux.lines.get(i));
			int oraScenariu=aux.getHour(i);
			int minScenariu=aux.getMinute(i);
			int secScenariu=aux.getSeconde(i);


			//System.out.println("ora scenariu:"+oraScenariu+":"+minScenariu+":"+secScenariu);

			if (ora_aer!=-1 && min_aer!=-1 && sec_aer!=-1)
			{
				if ((ora>ora_aer) || (ora==ora_aer && min>min_aer) || (ora==ora_aer && min==min_aer && sec>=sec_aer))
				{
					Simulator.senzor_temperatura.setPower(Simulator.senzor_temperatura.getPower()+5);
					ora_aer=-1;
					min_aer=-1;
					sec_aer=-1;
					Main.casa.aer.setStare(false);
				}
			}
			if (ora_masina!=-1 && min_masina!=-1 && sec_masina!=-1)
			{
				if ((ora>ora_masina) || (ora==ora_masina && min>min_masina) || (ora==ora_masina && min==min_masina && sec>=sec_masina))
				{
					float consum=PrelucrareDate.getConsumApa(Simulator.connection, "apa", "haine");
					if (cameraOmulet.equalsIgnoreCase("baie1"))
						Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()-consum);
					else
						if (cameraOmulet.equalsIgnoreCase("baie2"))
							Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()-consum);
					ora_masina=-1;
					min_masina=-1;
					sec_masina=-1;
					Main.casa.masina.setStare(false);
				}
			}
			
			if (ora_sf_mancare!=-1 && min_sf_mancare!=-1 && sec_sf_mancare!=-1)
			{
				if (ora>ora_sf_mancare)
				{
					Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
					ora_sf_mancare=-1;
					min_sf_mancare=-1;
					sec_sf_mancare=-1;
				}
				else
					if (ora==ora_sf_mancare && min>min_sf_mancare)
					{
						Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
						ora_sf_mancare=-1;
						min_sf_mancare=-1;
						sec_sf_mancare=-1;
					}
					else
						if (ora==ora_sf_mancare && min==min_sf_mancare && sec>=sec_sf_mancare)
						{
							Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "sta"));
							ora_sf_mancare=-1;
							min_sf_mancare=-1;
							sec_sf_mancare=-1;
						}
						else
						{
							Simulator.senzor_miscare_buc.setPower(PrelucrareDate.getMiscare(Simulator.connection, "senzor_miscare", "misca"));
						}
			}

			
			for (int index=0; index<ultima_gaz.size(); index++)
			{
				if ((ora>ora_sf_gaz.get(index)) || (ora==ora_sf_gaz.get(index) && min>min_sf_gaz.get(index)) || (ora==ora_sf_gaz.get(index) && min==min_sf_gaz.get(index) && sec>=sec_sf_gaz.get(index)))
				{
					Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()-ultima_gaz.get(index));
					ultima_gaz.remove(index);
					ora_sf_gaz.remove(index);
					min_sf_gaz.remove(index);
					sec_sf_gaz.remove(index);
					index--;
					Main.casa.aragaz.setStare(false);
				}
				
			}
			if (ultim_gaz!=0)
			{
				Simulator.senzor_gaz.setPower(Simulator.senzor_gaz.getPower()-ultim_gaz);
				ultim_gaz=0;
				Main.casa.aragaz.setStare(false);
			}
			for (int index=0; index<ultima_apa.size(); index++)
			{
				if (ultima_camera.get(index).equalsIgnoreCase("baie1"))
				{
					Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()-ultima_apa.get(index));
					ultima_apa.remove(index);
					ultima_camera.remove(index);
					index--;
					Main.casa.robinetBaie1.setStare(false);
				}
				else
					if (ultima_camera.get(index).equalsIgnoreCase("baie2"))
					{
						Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()-ultima_apa.get(index));
						ultima_apa.remove(index);
						ultima_camera.remove(index);
						index--;
						Main.casa.robinetB2.setStare(false);
					}
					else
						if (ultima_camera.get(index).equalsIgnoreCase("bucatarie"))
						{
							Simulator.senzor_apa_buc.setPower(Simulator.senzor_apa_buc.getPower()-ultima_apa.get(index));
							ultima_apa.remove(index);
							ultima_camera.remove(index);
							index--;
							Main.casa.robinetBuc.setStare(false);
						}
			}

			for (int index=0; index<consum_apa.size(); index++)
			{

				if ((ora>ora_sfarsit.get(index)) || (ora==ora_sfarsit.get(index) && min>min_sfarsit.get(index)) || (ora==ora_sfarsit.get(index) && min==min_sfarsit.get(index) && sec>=sec_sfarsit.get(index)))
				{
					if (cameraConsum.get(index).equalsIgnoreCase("baie1"))
					{
						Simulator.senzor_apa_baie1.setPower(Simulator.senzor_apa_baie1.getPower()-consum_apa.get(index));
						if (ceSpala.get(index).equalsIgnoreCase("wc"))
							Main.casa.wc1.setStare(false);
						else
							if (ceSpala.get(index).equalsIgnoreCase("dus") || ceSpala.get(index).equalsIgnoreCase("picioare"))
								Main.casa.robinetVana.setStare(false);
							else
								Main.casa.robinetBaie1.setStare(false);
						ceSpala.remove(index);
						consum_apa.remove(index);
						ora_sfarsit.remove(index);
						min_sfarsit.remove(index);
						sec_sfarsit.remove(index);
						cameraConsum.remove(index);
						index--;
					}
					else
						if (cameraConsum.get(index).equalsIgnoreCase("baie2"))
						{
							Simulator.senzor_apa_baie2.setPower(Simulator.senzor_apa_baie2.getPower()-consum_apa.get(index));
							if (ceSpala.get(index).equalsIgnoreCase("wc"))
								Main.casa.wc2.setStare(false);
							else
								if (ceSpala.get(index).equalsIgnoreCase("dus") || ceSpala.get(index).equalsIgnoreCase("picioare"))
									Main.casa.robinetVana.setStare(false);
								else
									Main.casa.robinetB2.setStare(false);
							ceSpala.remove(index);
							consum_apa.remove(index);
							ora_sfarsit.remove(index);
							min_sfarsit.remove(index);
							sec_sfarsit.remove(index);
							cameraConsum.remove(index);
							index--;
						}
						else
							if (cameraConsum.get(index).equalsIgnoreCase("bucatarie"))
							{
								Simulator.senzor_apa_buc.setPower(Simulator.senzor_apa_buc.getPower()-consum_apa.get(index));
								ceSpala.remove(index);
								consum_apa.remove(index);
								ora_sfarsit.remove(index);
								min_sfarsit.remove(index);
								sec_sfarsit.remove(index);
								cameraConsum.remove(index);
								index--;
							}
				}
			}
			for (int index=0; index<ultima_curent.size(); index++)
			{
				Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-ultima_curent.get(index));
				ultima_curent.remove(index);
				index--;
			}
			for (int index=0; index<consum_curent.size(); index++)
			{
				if ((ora>ora_sf.get(index))|| (ora==ora_sf.get(index) && min>min_sf.get(index)) || (ora==ora_sf.get(index) && min==min_sf.get(index) && sec>=sec_sf.get(index)))
				{
					Simulator.senzor_curent.setPower(Simulator.senzor_curent.getPower()-consum_curent.get(index));
					if (ceFoloseste.get(index).equalsIgnoreCase("aer conditionat"))
						Main.casa.aer.setStare(false);
					else
						if (ceFoloseste.get(index).equalsIgnoreCase("aragaz"))
							Main.casa.aragaz.setStare(false);
						else
							if (ceFoloseste.get(index).equalsIgnoreCase("calculator"))
								Main.casa.calcL.setStare(false);
							else
								if (ceFoloseste.get(index).equalsIgnoreCase("televizor"))
									Main.casa.tv.setStare(false);
								else
									if (ceFoloseste.get(index).equalsIgnoreCase("masina de spalat"))
										Main.casa.masina.setStare(false);
									else
										if (ceFoloseste.get(index).equalsIgnoreCase("frigider"))
											Main.casa.frigi.setStare(false);
										else
											if (ceFoloseste.get(index).equalsIgnoreCase("bec"))
												setBec(cameraBec, false);
					ceFoloseste.remove(index);
					consum_curent.remove(index);
					ora_sf.remove(index);
					min_sf.remove(index);
					sec_sf.remove(index);
					index--;
				}
			}


			while (ora==oraScenariu && min==minScenariu && (sec>=30) && (secScenariu>=30 && secScenariu<60))
			{
				Interfata.TIMER_INTERVAL=1000;
				TimerBean.interval=1000;
				/*Interfata.TIMER_INTERVAL=3000;
				TimerBean.interval=3000;*/
				/*Interfata.TIMER_INTERVAL=100;	
				TimerBean.interval=100;*/
				prelucreaza(i);
				//skip to next line
				i++;
				if (i<n)
				{
					oraScenariu=aux.getHour(i);
					minScenariu=aux.getMinute(i);
					secScenariu=aux.getSeconde(i);
				}
				else
					if (i==n)
					{
						oraScenariu=0;
						minScenariu=0;
						secScenariu=0;
					}
			}//while
			while (ora==oraScenariu && min==minScenariu && (sec<30) && (secScenariu>=0 && secScenariu<30))
			{
				Interfata.TIMER_INTERVAL=1000;
				TimerBean.interval=1000;
				/*Interfata.TIMER_INTERVAL=3000;
				TimerBean.interval=3000;*/
				/*Interfata.TIMER_INTERVAL=100;	
				TimerBean.interval=100;*/
				prelucreaza(i);
				//skip to next line
				i++;
				if (i<n)
				{
					oraScenariu=aux.getHour(i);
					minScenariu=aux.getMinute(i);
					secScenariu=aux.getSeconde(i);
				}
				else
					if (i==n)
					{
						oraScenariu=0;
						minScenariu=0;
						secScenariu=0;
					}

			}
		}//end if
		else
			if (i>=n)
			{
				Interfata.TIMER_INTERVAL=1;
				TimerBean.interval=1;
			}
	}
}