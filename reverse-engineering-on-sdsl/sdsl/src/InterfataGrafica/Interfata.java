package InterfataGrafica;

import java.util.ArrayList;

import main.Main;
import obStatice.cadaDeBaie;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;

import timer.TimerEvent;

import aparate.*;

public class Interfata {

	public static Canvas canvas;
	public final static Display display = new Display();
	public final static Shell shell = new Shell(display);

	public static final int IMAGE_WIDTH = 20;
	public static int TIMER_INTERVAL = 1;

	public static int xOmulet1 = 850;
	public static int yOmulet1 = 650;
	public static ArrayList<Integer> xDestOmulet1=new ArrayList<Integer>();
	public static ArrayList<Integer> yDestOmulet1=new ArrayList<Integer>();
	
	public static int xOmulet2=900;
	public static int yOmulet2=650;
	public static ArrayList<Integer> xDestOmulet2=new ArrayList<Integer>();
	public static ArrayList<Integer> yDestOmulet2=new ArrayList<Integer>();
	
	public static int xOmulet3=850;
	public static int yOmulet3=400;
	public static ArrayList<Integer> xDestOmulet3=new ArrayList<Integer>();
	public static ArrayList<Integer> yDestOmulet3=new ArrayList<Integer>();
	
	public static int xOmulet4=850;
	public static int yOmulet4=150;
	public static ArrayList<Integer> xDestOmulet4=new ArrayList<Integer>();
	public static ArrayList<Integer> yDestOmulet4=new ArrayList<Integer>();

	public static int directionX = 1;
	public static int directionY = 1;

	public static int iInterfata=0;
	public static int iInterfata2=0;
	public static int iInterfata3=0;
	public static int iInterfata4=0;


	static Font font = new Font(display,"Arial", 14, SWT.BOLD | SWT.ITALIC); 


	public Interfata()
	{
		shell.setLayout(new FillLayout());
		//canvas = new Canvas(shell,SWT.NO_REDRAW_RESIZE);
		shell.setSize(1200, 810);
	}

	public void run() {
		// final Display display = new Display();
		//Shell shell = new Shell(Interfata.display);
		createContents(Interfata.shell);
		Interfata.shell.open();

		// Set up the timer for the animation
		Runnable runnable = new Runnable() {
			public void run() {
				animate();
				Interfata.display.timerExec(TIMER_INTERVAL, this);
			}
		};

		// Launch the timer
		Interfata.display.timerExec(TIMER_INTERVAL, runnable);

		while (!Interfata.shell.isDisposed()) {
			if (!Interfata.display.readAndDispatch()) {
				Interfata.display.sleep();
			}
		}

		// Kill the timer
		Interfata.display.timerExec(-1, runnable);
		Interfata.display.dispose();
	}

	/**
	 * Creates the main window's contents
	 * 
	 * @param shell the main window
	 */
	private void createContents(final Shell shell) {

		// Create the canvas for drawing
		Interfata.canvas = new Canvas(shell, SWT.NO_REDRAW_RESIZE);
		Interfata.canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				// Draw the background
				e.gc.fillRectangle(Interfata.canvas.getBounds());

				// Set the color of the ball
				e.gc.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_RED));

				// Draw the ball
				e.gc.fillOval(xOmulet1, yOmulet1, IMAGE_WIDTH, IMAGE_WIDTH);
				
				e.gc.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_MAGENTA));
				e.gc.fillOval(xOmulet2, yOmulet2, IMAGE_WIDTH, IMAGE_WIDTH);
				
				e.gc.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));
				e.gc.fillOval(xOmulet3, yOmulet3, IMAGE_WIDTH, IMAGE_WIDTH);
				
				e.gc.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_CYAN));
				e.gc.fillOval(xOmulet4, yOmulet4, IMAGE_WIDTH, IMAGE_WIDTH);


				e.gc.drawLine(0, 800, 1000, 800);
				e.gc.drawLine(1000, 800, 1000, 0);
				e.gc.drawLine(0, 0, 0, 800);
				//usa h-iesire
				e.gc.drawLine(0, 0, 430, 0);
				e.gc.drawLine(470, 0, 1000, 0);

				//usa l-h
				e.gc.drawLine(0, 500, 338, 500);
				e.gc.drawLine(378, 500, 480, 500);
				//usa b1-h
				e.gc.drawLine(520, 500, 780, 500);
				//usa d1-d2
				e.gc.drawLine(820, 500, 1000, 500);

				//usa l-b1
				e.gc.drawLine(400, 800, 400, 670);
				e.gc.drawLine(400, 500, 400, 630);
				//usa b1-d1
				e.gc.drawLine(600, 800, 600, 670);
				e.gc.drawLine(600, 500, 600, 630);

				e.gc.drawLine(0, 300, 300, 300);
				//usa b2
				e.gc.drawLine(300, 500, 300, 420);
				e.gc.drawLine(300, 300, 300, 380);
				//usa buc
				e.gc.drawLine(300, 300, 300, 170);
				e.gc.drawLine(300, 0, 300, 130);

				e.gc.drawLine(1000, 300, 600, 300);
				//usa d2
				e.gc.drawLine(600, 500, 600, 420);
				e.gc.drawLine(600, 300, 600, 380);
				//usa d3
				e.gc.drawLine(600, 300, 600, 170);
				e.gc.drawLine(600, 0, 600, 130);

				e.gc.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
				e.gc.setFont(font); 
				e.gc.drawText("Living",50,700);
				e.gc.drawText("Baie 1",530,700); 
				e.gc.drawText("Dormitor 1",870,700);
				e.gc.drawText("Baie 2", 50, 470);
				e.gc.drawText("Bucatarie", 50, 270);
				e.gc.drawText("Hol", 450, 250);
				e.gc.drawText("Dormitor 2", 870, 470);
				e.gc.drawText("Dormitor 3", 870, 270);
				
				String oraA="";
				if (TimerEvent.now.getHours()<10)
					oraA+="0"+TimerEvent.now.getHours();
				else
					oraA+=TimerEvent.now.getHours();
				oraA+=":";
				if (TimerEvent.now.getMinutes()<10)
					oraA+="0"+TimerEvent.now.getMinutes();
				else
					oraA+=TimerEvent.now.getMinutes();
				oraA+=":";
				if (TimerEvent.now.getSeconds()<10)
					oraA+="0"+TimerEvent.now.getSeconds();
				else
					oraA+=TimerEvent.now.getSeconds();
				
				e.gc.drawText("ORA CURENTA:", 1020, 50);
				e.gc.drawText(oraA, 1020, 100);
				
				

				Image i = Main.casa.becBuc.getImage();
				e.gc.drawImage(i, Main.casa.becBuc.getX(), Main.casa.becBuc.getY());
				//i.dispose();

				i = Main.casa.becB2.getImage();
				e.gc.drawImage(i, Main.casa.becB2.getX(), Main.casa.becB2.getY());
				//i.dispose();

				i = Main.casa.becB1.getImage();
				e.gc.drawImage(i, Main.casa.becB1.getX(), Main.casa.becB1.getY());
				//i.dispose();

				i = Main.casa.becL.getImage();
				e.gc.drawImage(i, Main.casa.becL.getX(), Main.casa.becL.getY());
				//i.dispose();

				i = Main.casa.BecH.getImage();
				e.gc.drawImage(i, Main.casa.BecH.getX(), Main.casa.BecH.getY());
				//i.dispose();

				i = Main.casa.BecD1.getImage();
				e.gc.drawImage(i, Main.casa.BecD1.getX(), Main.casa.BecD1.getY());
				//i.dispose();

				i = Main.casa.BecD2.getImage();
				e.gc.drawImage(i, Main.casa.BecD2.getX(), Main.casa.BecD2.getY());
				//i.dispose();

				i = Main.casa.BecD3.getImage();
				e.gc.drawImage(i, Main.casa.BecD3.getX(), Main.casa.BecD3.getY());
				//i.dispose();

				i = Main.casa.frigi.getImage();
				e.gc.drawImage(i, Main.casa.frigi.getX(), Main.casa.frigi.getY());
				//i.dispose();

				i = Main.casa.aragaz.getImage();
				e.gc.drawImage(i, Main.casa.aragaz.getX(), Main.casa.aragaz.getY());
				//i.dispose();

				i = Main.casa.robinetBuc.getImage();
				e.gc.drawImage(i, Main.casa.robinetBuc.getX(), Main.casa.robinetBuc.getY());
				//i.dispose();

				i = Main.casa.masina.getImage();
				e.gc.drawImage(i, Main.casa.masina.getX(), Main.casa.masina.getY());
				//i.dispose();

				i = Main.casa.robinetB2.getImage();
				e.gc.drawImage(i, Main.casa.robinetB2.getX(), Main.casa.robinetB2.getY());
				//i.dispose();

				i = Main.casa.robinetBaie1.getImage();
				e.gc.drawImage(i, Main.casa.robinetBaie1.getX(), Main.casa.robinetBaie1.getY());
				//i.dispose();

				i = Main.casa.cadaBaie1.getImage();
				e.gc.drawImage(i, Main.casa.cadaBaie1.getX(), Main.casa.cadaBaie1.getY());
				//i.dispose();

				i = Main.casa.tv.getImage();
				e.gc.drawImage(i, Main.casa.tv.getX(), Main.casa.tv.getY());
				//i.dispose();

				i = Main.casa.calcL.getImage();
				e.gc.drawImage(i, Main.casa.calcL.getX(), Main.casa.calcL.getY());
				//i.dispose();

				i = Main.casa.aer.getImage();
				e.gc.drawImage(i, Main.casa.aer.getX(), Main.casa.aer.getY());
				//i.dispose();

				i = Main.casa.wc1.getImage();
				e.gc.drawImage(i, Main.casa.wc1.getX(), Main.casa.wc1.getY());

				i = Main.casa.wc2.getImage();
				e.gc.drawImage(i, Main.casa.wc2.getX(), Main.casa.wc2.getY());

				i = Main.casa.robinetVana.getImage();
				e.gc.drawImage(i, Main.casa.robinetVana.getX(), Main.casa.robinetVana.getY());
				
				i = Main.casa.canap.getImage();
				e.gc.drawImage(i, Main.casa.canap.getX(), Main.casa.canap.getY());
				
				i = Main.casa.masaScaune.getImage();
				e.gc.drawImage(i, Main.casa.masaScaune.getX(), Main.casa.masaScaune.getY());
				
				i = Main.casa.patD.getImage();
				e.gc.drawImage(i, Main.casa.patD.getX(), Main.casa.patD.getY());
				
				i = Main.casa.patS1.getImage();
				e.gc.drawImage(i, Main.casa.patS1.getX(), Main.casa.patS1.getY());
				
				i = Main.casa.patS2.getImage();
				e.gc.drawImage(i, Main.casa.patS2.getX(), Main.casa.patS2.getY());
				
				i = Main.casa.plant.getImage();
				e.gc.drawImage(i, Main.casa.plant.getX(), Main.casa.plant.getY());
				
			}
		});
	}

	/**
	 * Animates the next frame
	 */
	public void animate() {
		// Determine the ball's location
		if (xDestOmulet1.size()>0)
		{
			if (xOmulet1>xDestOmulet1.get(iInterfata))
			{
				directionX=-1;
				xOmulet1 += 50*directionX;
				Interfata.TIMER_INTERVAL=10;
			}
			else
				if (yOmulet1>yDestOmulet1.get(iInterfata) )
				{
					directionY=-1;
					yOmulet1 += 50*directionY;
					Interfata.TIMER_INTERVAL=10;
				}
				else
					if (xOmulet1<xDestOmulet1.get(iInterfata))
					{
						
						directionX=1;
						xOmulet1 += 50*directionX;
						Interfata.TIMER_INTERVAL=10;
					}
					else
						if (yOmulet1<yDestOmulet1.get(iInterfata) )
						{
							
							directionY=1;
							yOmulet1 += 50*directionY;
							Interfata.TIMER_INTERVAL=10;
						}
						else
						{
							Interfata.xDestOmulet1.remove(iInterfata);
							Interfata.yDestOmulet1.remove(iInterfata);
							Interfata.TIMER_INTERVAL=1000;
						}
		}
		if (xDestOmulet2.size()>0)
		{
			if (xOmulet2>xDestOmulet2.get(iInterfata2))
			{
				directionX=-1;
				xOmulet2 += 50*directionX;
				Interfata.TIMER_INTERVAL=10;
			}
			else
				if (yOmulet2>yDestOmulet2.get(iInterfata2) )
				{
					directionY=-1;
					yOmulet2 += 50*directionY;
					Interfata.TIMER_INTERVAL=10;
				}
				else
					if (xOmulet2<xDestOmulet2.get(iInterfata2))
					{
						
						directionX=1;
						xOmulet2 += 50*directionX;
						Interfata.TIMER_INTERVAL=10;
					}
					else
						if (yOmulet2<yDestOmulet2.get(iInterfata2) )
						{
							
							directionY=1;
							yOmulet2 += 50*directionY;
							Interfata.TIMER_INTERVAL=10;
						}
						else
						{
							Interfata.xDestOmulet2.remove(iInterfata2);
							Interfata.yDestOmulet2.remove(iInterfata2);
						}
		}
		if (xDestOmulet3.size()>0)
		{
			if (xOmulet3>xDestOmulet3.get(iInterfata3))
			{
				directionX=-1;
				xOmulet3 += 50*directionX;
				Interfata.TIMER_INTERVAL=10;
			}
			else
				if (yOmulet3>yDestOmulet3.get(iInterfata3) )
				{
					directionY=-1;
					yOmulet3 += 50*directionY;
					Interfata.TIMER_INTERVAL=10;
				}
				else
					if (xOmulet3<xDestOmulet3.get(iInterfata3))
					{
						
						directionX=1;
						xOmulet3 += 50*directionX;
						Interfata.TIMER_INTERVAL=10;
					}
					else
						if (yOmulet3<yDestOmulet3.get(iInterfata3) )
						{
							
							directionY=1;
							yOmulet3 += 50*directionY;
							Interfata.TIMER_INTERVAL=10;
						}
						else
						{
							Interfata.xDestOmulet3.remove(iInterfata3);
							Interfata.yDestOmulet3.remove(iInterfata3);
							Interfata.TIMER_INTERVAL=1000;
						}
		}
		if (xDestOmulet4.size()>0)
		{
			if (xOmulet4>xDestOmulet4.get(iInterfata4))
			{
				directionX=-1;
				xOmulet4 += 50*directionX;
				Interfata.TIMER_INTERVAL=10;
			}
			else
				if (yOmulet4>yDestOmulet4.get(iInterfata4) )
				{
					directionY=-1;
					yOmulet4 += 50*directionY;
					Interfata.TIMER_INTERVAL=10;
				}
				else
					if (xOmulet4<xDestOmulet4.get(iInterfata4))
					{
						
						directionX=1;
						xOmulet4 += 50*directionX;
						Interfata.TIMER_INTERVAL=10;
					}
					else
						if (yOmulet4<yDestOmulet4.get(iInterfata4) )
						{
							
							directionY=1;
							yOmulet4 += 50*directionY;
							Interfata.TIMER_INTERVAL=10;
						}
						else
						{
							Interfata.xDestOmulet4.remove(iInterfata4);
							Interfata.yDestOmulet4.remove(iInterfata4);
							Interfata.TIMER_INTERVAL=1000;
						}
		}
		// Force a redraw
		Interfata.canvas.redraw();
	}

	public void stopInterfata()
	{
		//shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
		System.exit(1);
	}
}
