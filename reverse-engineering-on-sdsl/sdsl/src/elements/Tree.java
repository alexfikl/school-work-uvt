package elements;

import java.util.ArrayList;

public class Tree<T> {
	private Node<T> rootElement;
     
    public Tree() {
       	super();
    }
    
    /**
     * Returneaza radacina arborelui
     */
    public Node<T> getRootElement() {
        return this.rootElement;
    }
 
    /**
     * Seteaza radacina arborelui
     */
    public void setRootElement(Node<T> rootElement) {
        this.rootElement = rootElement;
    }
     
    /**
     * Returneaza arborele ca o lista de norduri, obtinuta prin
     * parcurgerea lui in preordine.
     */
    public ArrayList<Node<T>> toList() {
        ArrayList<Node<T>> list = new ArrayList<Node<T>>();
        walk(rootElement, list);
        return list;
    }
     
    /**
     * Returneaza lista in preordine sub forma de sir de caractere.
     */
    public String toString() {
        return toList().toString();
    }
     
    /**
     *Functie recursiva care returneaza arborele in preordine.
     */
    private void walk(Node<T> element, ArrayList<Node<T>> list) {
        list.add(element);
        for (Node<T> data : element.getChildren()) {
            walk(data, list);
        }
    }
}
