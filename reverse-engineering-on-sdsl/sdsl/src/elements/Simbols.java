package elements;

/**
 * Retine informatia dintr-un nod al arborelui, specificand daca nodul este sau nu este terminal.
 */
public class Simbols {
	protected String simbol;
	//0=nu, 1=da, 2=si nu si da.
	protected int terminal;
	
	public Simbols(String simbol, int terminal)
	{
		this.simbol=simbol;
		this.terminal=terminal;
	}

	public String getSimbol() {
		return simbol;
	}

	public int getTerminal() {
		return terminal;
	}

	public void setSimbol(String simbol) {
		this.simbol = simbol;
	}

	public void setTerminal(int terminal) {
		this.terminal = terminal;
	}
	
	public String toString()
	{
		return "("+simbol+","+terminal+")";
	}
	
}
