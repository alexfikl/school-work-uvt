package elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import main.ReadFile;

/**
 * Citeste din fisiere cuvintele clasificate pe mai multe categorii.
 */
public class Vocabulary {
	protected ArrayList<String> actiuni;
	protected ArrayList<String> context;
	protected ArrayList<String> vreme;
	protected ArrayList<String> incaperi;
	protected ArrayList<String> inincaperi;
	protected ArrayList<String> aparate;
	protected ArrayList<String> statice;
	protected ArrayList<String> spalare;
	protected ArrayList<String> corp;
	protected ArrayList<String> setari;
	protected ArrayList<String> intensitate;
	protected ArrayList<String> timp;
	protected ArrayList<String> composed;
	protected ReadFile rf;
	public static ArrayList<String> name=new ArrayList<String>();

	public Vocabulary() throws IOException {
		rf = new ReadFile();
		setLists();
	}

	private void setLists() throws IOException {
		setActiuni();
		setAparate();
		setContext();
		setCorp();
		setIncaperi();
		setInincaperi();
		setIntensitate();
		setSetari();
		setSpalare();
		setStatice();
		setVreme();
		setTimp();
		setComposed();
		//setName();
	}

	public void getAll() {
		@SuppressWarnings("unused")
		ArrayList<String> aux = new ArrayList<String>();
		aux = getActiuni();
		aux = getAparate();
		aux = getContext();
		aux = getCorp();
		aux = getIncaperi();
		aux = getInincaperi();
		aux = getIntensitate();
		aux = getSetari();
		aux = getStatice();
		aux = getVreme();
		aux = getTimp();
		aux = getComposed();
		aux = getName();
	}

	public void writeArray(ArrayList<String> list) {
		Iterator<String> itr = list.iterator();
		while (itr.hasNext()) {
			String element = itr.next();
			System.out.print(element + " * ");
		}
		System.out.println();
	}

	public ArrayList<String> getActiuni() {
		/*
		 * System.out.print("Actiuni "); writeArray(actiuni);
		 */
		return actiuni;
	}

	public ArrayList<String> getContext() {
		/*
		 * System.out.print("context "); writeArray(context);
		 */
		return context;
	}

	public ArrayList<String> getVreme() {
		/*
		 * System.out.print("vreme "); writeArray(vreme);
		 */
		return vreme;
	}

	public ArrayList<String> getIncaperi() {
		/*
		 * System.out.print("incaperi "); writeArray(incaperi);
		 */
		return incaperi;
	}

	public ArrayList<String> getAparate() {
		/*
		 * System.out.print("aparate "); writeArray(aparate);
		 */
		return aparate;
	}

	public ArrayList<String> getStatice() {
		/*
		 * System.out.print("statice "); writeArray(statice);
		 */
		return statice;
	}

	public ArrayList<String> getSpalare() {
		/*
		 * System.out.print("spalare "); writeArray(spalare);
		 */
		return spalare;
	}

	public ArrayList<String> getCorp() {
		/*
		 * System.out.print("corp "); writeArray(corp);
		 */
		return corp;
	}

	public ArrayList<String> getSetari() {
		/*
		 * System.out.print("setari "); writeArray(setari);
		 */
		return setari;
	}

	public ArrayList<String> getIntensitate() {
		/*
		 * System.out.print("intensitate "); writeArray(intensitate);
		 */
		return intensitate;
	}

	public ArrayList<String> getComposed() {
		/*
		 * System.out.print("composed "); writeArray(composed);
		 */
		return composed;
	}

	public ArrayList<String> getTimp() {
	/*	System.out.print("timp "); writeArray(timp);*/
		return timp;
	}

	public ArrayList<String> getName() {
		return name;
	}
	
	public ArrayList<String> getInincaperi() {
		return inincaperi;
	}

	public void setInincaperi() throws IOException {
		this.inincaperi = ReadFile.readFromFile("vocabular/inincaperi.txt");
	}

	public void setName(String nume) throws IOException {
		//this.name=ReadFile.readFromFile("vocabular/nume.txt");
		this.name.add(nume);
	}

	public void setTimp() throws IOException {
		this.timp = ReadFile.readFromFile("vocabular/timp.txt");
	}

	public void setActiuni() throws IOException {
		this.actiuni = ReadFile.readFromFile("vocabular/actiuni.txt");
	}

	public void setContext() throws IOException {
		this.context = ReadFile.readFromFile("vocabular/context.txt");
	}

	public void setVreme() throws IOException {
		this.vreme = ReadFile.readFromFile("vocabular/vreme.txt");
	}

	public void setIncaperi() throws IOException {
		this.incaperi = ReadFile.readFromFile("vocabular/incaperi.txt");
	}

	public void setAparate() throws IOException {
		this.aparate = ReadFile.readFromFile("vocabular/aparate.txt");
	}

	public void setStatice() throws IOException {
		this.statice = ReadFile.readFromFile("vocabular/statice.txt");
	}

	public void setSpalare() throws IOException {
		this.spalare = ReadFile.readFromFile("vocabular/spalare.txt");
	}

	public void setCorp() throws IOException {
		this.corp = ReadFile.readFromFile("vocabular/corp.txt");
	}

	public void setSetari() throws IOException {
		this.setari = ReadFile.readFromFile("vocabular/setari.txt");
	}

	public void setIntensitate() throws IOException {
		this.intensitate = ReadFile.readFromFile("vocabular/intensitate.txt");
	}

	public void setComposed() {
		this.composed = ReadFile.getComposed();
	}
}
