package elements;

/**
 * Creaza gramatica.
 */
public class Grammar {
	Simbols s;
	Simbols p1, p2, sv, ct1, ct2, ct3, ct4, ct5, ct6, ct7, ct8, ct9;
	Simbols cm1, cm2, cl1, cl2, cd1, cd2;
	Tree<Simbols> tree;

	public Grammar() {
		s = new Simbols("Subiect", 0);
		p1 = new Simbols("Predicat", 0);
		p2 = new Simbols("Predicat", 0);
		sv = new Simbols("Schimbare vreme", 0);
		ct1 = new Simbols("Complement de timp", 2);
		ct2 = new Simbols("Complement de timp", 1);
		ct3 = new Simbols("Complement de timp", 1);
		ct4 = new Simbols("Complement de timp", 2);
		ct5 = new Simbols("Complement de timp", 1);
		ct6 = new Simbols("Complement de timp", 1);
		ct7 = new Simbols("Complement de timp", 0);
		ct8 = new Simbols("Complement de timp", 1);
		ct9 = new Simbols("Complement de timp", 1);
		cm1 = new Simbols("Complement de mod", 0);
		cm2 = new Simbols("Complement de mod", 0);
		cl1 = new Simbols("Complement de loc", 0);
		cl2 = new Simbols("Complement de loc", 1);
		cd1 = new Simbols("Complement direct", 0);
		cd2 = new Simbols("Complement direct", 2);
		tree = new Tree<Simbols>();
		
		tree.setRootElement(new Node<Simbols>(new Simbols("Propozitie", 0)));

		/**
		 * s,p,cl,ct
		 */
		tree.getRootElement().addChild(new Node<Simbols>(s));
		tree.getRootElement().getChild(s).addChild(new Node<Simbols>(p2));
		tree.getRootElement().getChild(s).getChild(p2).addChild(
				new Node<Simbols>(cl1));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cl1).addChild(
				new Node<Simbols>(ct3));
		/**
		 * s,p,ct,ct
		 */
		tree.getRootElement().getChild(s).getChild(p2).addChild(
				new Node<Simbols>(ct1));
		tree.getRootElement().getChild(s).getChild(p2).getChild(ct1).addChild(
				new Node<Simbols>(ct2));

		/**
		 * s,p,cm,ct,ct
		 */
		tree.getRootElement().getChild(s).getChild(p2).addChild(
				new Node<Simbols>(cm2));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cm2).addChild(
				new Node<Simbols>(ct7));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cm2).getChild(
				ct7).addChild(new Node<Simbols>(ct8));

		/**
		 * s,p,cd,ct,ct
		 */
		tree.getRootElement().getChild(s).getChild(p2).addChild(
				new Node<Simbols>(cd1));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cd1).addChild(
				new Node<Simbols>(ct4));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cd1).getChild(
				ct4).addChild(new Node<Simbols>(ct5));
		/**
		 * s,p,cd,ct,cm,ct
		 */
		tree.getRootElement().getChild(s).getChild(p2).getChild(cd1).getChild(
				ct4).addChild(new Node<Simbols>(cm1));
		tree.getRootElement().getChild(s).getChild(p2).getChild(cd1).getChild(
				ct4).getChild(cm1).addChild(new Node<Simbols>(ct6));
		/**
		 * p,cd,cl
		 */
		tree.getRootElement().addChild(new Node<Simbols>(p1));
		tree.getRootElement().getChild(p1).addChild(new Node<Simbols>(cd2));
		tree.getRootElement().getChild(p1).getChild(cd2).addChild(
				new Node<Simbols>(cl2));
		/**
		 * sv,ct
		 */
		tree.getRootElement().addChild(new Node<Simbols>(sv));
		tree.getRootElement().getChild(sv).addChild(new Node<Simbols>(ct9));
	//	System.out.print(tree);

	}
	public Tree<Simbols> getTree()
	{
		return tree; 
	}

}
