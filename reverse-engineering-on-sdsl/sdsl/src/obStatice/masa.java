package obStatice;

import main.Main;

import org.eclipse.swt.SWT;
import timer.*;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import InterfataGrafica.Interfata;

public class masa extends HouseComponentBean implements
		java.io.Serializable, TimerListener {

	/* Properties */
	/*
	 * private String name = null; private int power = 0;
	 */
	private Image image;
	int x, y;

	/* Constructor */
	public masa(final int x, final int y) {

		this.x = x;
		this.y = y;
		image = new Image(Interfata.display, "pozeAparate/table.png");
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image img) {

		image = img;

		image = new Image(Interfata.display, "pozeAparate/table.png");
	}

	public void timeElapsed(TimerEvent evt) {
		System.out.println(this.getName() + " functioneaza la data: "
				+ evt.toString() + " si consuma " + this.getPower());

	}
}
