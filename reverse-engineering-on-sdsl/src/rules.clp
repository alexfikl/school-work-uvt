; idea - make decision tree as follows:
; - we already know when and who: when is initialized at the begining and iterates
;								  who is initialized at the begining from the name the user asked for.
; - using the motion sensor find out the location
; - using specific rules find out what the person is doing
; - we got an answer!

; TODO:
; - keep track of location so we only mention it when it changes
; - ?

; inits
(batch "templates.clp")
(assert (answer (when 0) (who "Vlad") (where nil) (what nil)))
(assert (last-location (location "bedroom") (changed 0)))
(bind ?time-handler (new Time))

; get location. if it changed
; conditions:
; - the answer was just initialized
; - the motion sensor is reporting movement for a time slot including our time which wasn't already read.
(defrule where-with-movement
    ?c<-(last-location (location ?old-location))
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where nil)) (what ?what&:(= ?what nil)))
    ?s<-(sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-type ?type) (sensor-location ?location) (value 128) (read FALSE))
    =>
    (modify ?a (where ?location))
    (modify ?s (read TRUE))
    (modify ?c (location ?location))
    (if (not (= ?old-location ?location)) then
            (printout t (call ?time-handler toString ?when) " " ?who " went into the " ?location "." crlf))
)

; get location. if it didn't change
; condition:
; - the answer was just initialized
; - there is no motion
(defrule where-without-movement
    ?c<-(last-location (location ?l))
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where nil)) (what ?what&:(= ?what nil)))
    =>
    (modify ?a (where ?l))
)

; once we found an answer. print it
; conditions:
; - all the answer slots are filled
(defrule got-answer
    (declare (salience 1))
    ?a<-(answer (when ?when&:(<= ?when 240000)) (who ?who) (where ?where&:(not (= ?where nil))) (what ?what&:(not (= ?what nil))))
    =>
    (printout t (call ?time-handler toString ?when) " " ?who " " ?what "." crlf)
    (modify ?a (when (call ?time-handler incrementTime ?when 30)) (where nil) (what nil))
)

(defrule unknown-action
    (declare (salience -1))
    ?a<-(answer (when ?when&:(<= ?when 1000)) (who ?who))
    =>
    (printout t (call ?time-handler toString ?when) " " ?who " did something new." crlf)
    (modify ?a (when (call ?time-handler increment ?when)) (where nil) (what nil))
)

; wake up rule
; conditions:
; - we're in the bedroom
; - we have no action yet
; - our time is in the interval in which the chosen person wakes up.
(defrule wake-up
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where "bedroom")) (what ?what&:(= ?what nil)))
    (habit (start-time ?st&:(<= ?st ?when)) (end-time ?et&:(<= ?when ?et)) (person ?who) (location ?where) (activity "woke up"))
    =>
    (modify ?a (what "woke up"))
)

; wake up rule
; conditions:
; - we're in the bedroom
; - we have no action yet
; - our time is in the interval in which the chosen person goes to sleep.
(defrule sleep
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where "bedroom")) (what ?what&:(= ?what nil)))
    (habit (start-time ?st&:(<= ?st ?when)) (end-time ?et&:(<= ?when ?et)) (person ?who) (location ?where) (activity "goes to sleep"))
    =>
    (modify ?a (what "goes to sleep"))
)

; leave rule
; conditions:
; - we're in the hallway
; - there is no more movement in the house for a while after our current time
; - current time fits in the leaving habit's time slot
(defrule leave-home
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where 'hallway')) (what ?what))
    (exists (sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-location ?l) (value 128) (read FALSE)))
    (habit (start-time ?sth&:(<= ?sth ?when)) (end-time ?eth&:(<= ?when ?eth)) (person ?who) (location ?where) (activity "leaves home"))
    =>
    (modify ?a (what "leaves home"))
)

; come home rule
; conditions:
; - we're in the hallway
; - there is no more movement in the house for a while before our current time
; - current time fits in the coming home habit's time slot
(defrule come-home
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where 'hallway')) (what ?what))
    (exists (sensor-reading (start-time ?st) (end-time ?et&:(>= ?when ?et)) (sensor-location ?l) (value 0) (read FALSE)))
    (habit (start-time ?sth&:(<= ?sth ?when)) (end-time ?eth&:(<= ?when ?eth)) (person ?who) (location ?where) (activity "comes home"))
    =>
    (modify ?a (what "comes home"))
)

; eat rule
; conditions:
; - we're in the kitchen
; - there's no gas used (to not be confused with cooking)
; - current time in the eating habit's time slot
; - time spent in kitchen is about 15-30 minutes
(defrule eat
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where 'kitchen')) (what ?what))
    ?s1<-(sensor-reading (start-time ?st1&:(>= ?when ?st1)) (end-time ?et1&:(<= ?when ?et1)) (sensor-type "motion") (sensor-location ?where) (value 128) (read TRUE))
    ?s2<-(sensor-reading (start-time ?st2&:(>= ?when ?st2)) (end-time ?et2&:(<= ?when ?et2)) (sensor-type "gas") (value 0) (read FALSE))
    (habit (start-time ?sth&:(<= ?sth ?when)) (end-time ?eth&:(<= ?when ?eth)) (person ?who) (location ?where) (activity "eating"))
    (test (or (<= 1500 (- ?et1 ?st1)) (>= 3000 (- ?et1 ?st1))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?s2 (read TRUE))
    (modify ?a (what "is eating"))
)

; drink water
; conditions:
; - we're in the kitchen (maybe should also check bathrooms?)
; - the water sensor has a value that's about the standard drinking value
(defrule drink
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where 'kitchen')) (what ?what))
    ?s2<-(sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-type "water") (sensor-location ?where) (value ?value) (read FALSE))
    (water-used-for (object "drinking") (consumption ?c&:(or (>= ?c (- ?value 0.1)) (<= ?c (+ ?value 0.1)))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?s2 (read TRUE))
    (modify ?a (what "is drinking water"))
)

; cook rule
; conditions:
; - we're in the kitchen
; - gas sensor is reporting a value different than 0
; - time spent in kitchen is about 15-120 minutes
(defrule cook
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(= ?where 'kitchen')) (what ?what))
    ?s1<-(sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-type "motion") (sensor-location ?where) (value 128) (read TRUE))
    ?s2<-(sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-type "gas") (value ?value&:(not (= ?value 0))) (read FALSE))
    (test (or (<= 1500 (- ?et ?st)) (>= 20000 (- ?et ?st))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?s2 (read TRUE))
    (modify ?a (what "is cooking"))
)

; toilet rule
; conditions:
; - we're in one of the bathrooms
; - the water sensor is reporting a value that's around the standard for using the toilet
; - time spent in bathroom is about 2-3 minutes
(defrule toilet
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(or (= ?where "bathroom1") (= ?where "bathroom2"))) (what ?what))
    ?s1<-(sensor-reading (start-time ?st1&:(>= ?when ?st1)) (end-time ?et1&:(<= ?when ?et1)) (sensor-type "motion") (sensor-location ?where) (value 128) (read TRUE))
    ?s2<-(sensor-reading (start-time ?st2&:(>= ?when ?st2)) (end-time ?et2&:(<= ?when ?et2)) (sensor-type "water") (sensor-location ?where)(value ?value&:(not (= ?value 0))) (read FALSE))
    (water-used-for (object "toilet") (consumption ?c&:(= ?c ?value)))
    (test (or (<= 200 (- ?et1 ?st1)) (>= 300 (- ?et1 ?st1))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?s2 (read TRUE))
    (modify ?a (what "went to the toilet"))
)

; washing rule
; conditions
; - we're in one of the bathrooms
; - water used is standard for one of the objects in the database
; - time spent is between 1-90 minutes
; TODO: should differentiante more between washing actions
(defrule wash-something
    ?a<-(answer (when ?when) (who ?who) (where ?where&:(or (= ?where "bathroom1") (= ?where "bathroom2"))) (what ?what))
    ?s1<-(sensor-reading (start-time ?st1&:(>= ?when ?st1)) (end-time ?et1&:(<= ?when ?et1)) (sensor-type "motion") (sensor-location ?where) (value 128) (read TRUE))
    ?s2<-(sensor-reading (start-time ?st2&:(>= ?when ?st2)) (end-time ?et2&:(<= ?when ?et2)) (sensor-type "water") (sensor-location ?where)(value ?value&:(not (= ?value 0))) (read FALSE))
    (water-used-for (object ?object) (consumption ?c&:(= ?c ?value)))
    (test (or (<= 100 (- ?et1 ?st1)) (>= 13000 (- ?et1 ?st1))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?s2 (read TRUE))
    (modify ?a (what (str-cat "washed " ?object)))
)

; turn on/off or set some appliances
; conditions:
; - the value from the electricity sensor is standard for some appliance.
; TODO:
; - should find some way to know if an appliance was turned on/off and keep track of them so we don't
; say we turned the same one on multiple times.
; - should also count location of appliances to better chose which one was used
(defrule used-app
    ?a<-(answer (when ?when) (who ?who) (where ?where) (what ?what))
    ?s1<-(sensor-reading (start-time ?st&:(>= ?when ?st)) (end-time ?et&:(<= ?when ?et)) (sensor-type "electricity") (sensor-location nil) (value ?value) (read FALSE))
    (electricity-used-by-appliance (appliance ?app) (state ?state) (min-consumption ?cmin&:(<= ?cmin ?value)) (max-consumption ?cmax&:(or (= ?cmax -1) (<= ?cmax ?value))))
    =>
    (modify ?s1 (read TRUE))
    (modify ?a (what (str-cat "used " ?app)))
)