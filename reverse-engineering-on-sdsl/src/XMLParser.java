/* *******************************************************************
* Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/


import java.io.File;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * The XMLParser class provides a simple way to access a xml file with a specific structure
 * @author Aleandru Fikl
 *
 */
public class XMLParser {
	/**
	 * XML document
	 */
	Document doc = null;
	/**
	 * A map to easier access a specific person in the document
	 */
	private static Map<String, Integer> personMap = new HashMap<String, Integer>() {
		private static final long serialVersionUID = 7302929529602589685L;

		{
			put("Ion", 0);
			put("Ana", 1);
			put("Alina", 2);
			put("Vlad", 3);
		}
	};

	/**
	 * Class constructor
	 * @param filename used file
	 */
	public XMLParser(String filename) {
		try {
			File file = new File(filename);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(file);
			doc.getDocumentElement().normalize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads all the elements of the XML document and returns and array with the required structure
	 * @param person name of the person we want the habits for
	 * @return list of habits clasified by time and location
	 */
	public ArrayList<Map<String, String>> getHabits(String person)
	{
		ArrayList<Map<String, String>> readings = new ArrayList<Map<String, String>>();
		Time timeHandler = new Time();
		
		Element personElement = (Element)doc.getElementsByTagName("persoana").item(personMap.get(person));
		NodeList nodeList = personElement.getElementsByTagName("*");
		int s = 1;
		System.out.println(person);
		
		while(s < nodeList.getLength())
		{
			String location = nodeList.item(s).getTextContent();
			++s;
			while(s < nodeList.getLength() && nodeList.item(s).getNodeName().equals("ora"))
			{
				String time = nodeList.item(s).getTextContent();
				String startTime = timeHandler.getStartTime(time);
				String endTime = timeHandler.getEndTime(time);
				++s;
				String[] activities = nodeList.item(s).getTextContent().split(", ");
				++s;
				
				for(String activity: activities)
				{
					Map<String, String> reading = new HashMap<String, String>();
					reading.put("location", location);
					reading.put("start-time", startTime);
					reading.put("end-time", endTime);
					reading.put("activity", activity);
					readings.add(reading);
				}
			}
		}
		
		return readings;
	}
}
