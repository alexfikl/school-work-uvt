/* *******************************************************************
* Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/


import java.sql.*;
import java.util.*;

/**
 * The SDSLDatabase class provides all access to outside information to the
 * ReverseEngineeringEngine.
 * 
 * @author Alexandru Fikl
 *
 */
public class SDSLDatabase {
	/**
	 * URL of the MySQL database
	 */
	private static String URL = "jdbc:mysql://localhost/consum";
	/**
	 * Username for the database
	 */
	private static String USERNAME = "root";
	/**
	 * Password for the database
	 */
	private static String PASSWD = "root";
	/**
	 * Database connection
	 */
	private Connection conn = null;
	/**
	 * Map used to make locations more readable
	 */
	private static Map<String, String> locationMap = new HashMap<String, String>() {
		private static final long serialVersionUID = 7302929529602589685L;

		{
            put("miscare baie 1", "bathroom1");
            put("miscare baie 2", "bathroom2");
            put("miscare bucatarie", "kitchen");
            put("miscare dormitor 1", "bedroom1");
            put("miscare dormitor 2", "bedroom2");
            put("miscare dormitor 3", "bedroom3");
            put("miscare living", "living");
            put("miscare hol", "hallway");
            put("apa baie 1", "bathroom1");
            put("apa baie 2", "bathroom2");
            put("apa bucatarie", "kitchen");
            put("curent", null);
            put("temperatura", null);
            put("gaz", "kitchen");
        }
    };

	/**
	 * Class constructor
	 */
	public SDSLDatabase() {
        // create database connection
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(URL, USERNAME, PASSWD);
			System.out.println("Database connection established");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.err.println("Cannot connect to database server");
		}
	}

	/**
	 * Closes the database
	 */
	public void close()
	{
        //close database when we're done with it.
		try {
			if(conn != null)
			{
				conn.close();
				System.out.println("Database connection terminated");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Queries the database for water consumption habits
	 * @return a ResultSet containing the data
	 * @throws SQLException
	 */
	public ResultSet getWaterConsumptionHabits() throws SQLException
	{
        // nothing fancy. just execute a query and return the results.
		Statement s = conn.createStatement();
		String query = "select * from apa";
		s.executeQuery(query);
		return s.getResultSet();
	}

	/**
	 * Queries the database for electricity used by appliances
	 * @return a ResultSet containing all data
	 * @throws SQLException
	 */
	public ResultSet getElectricityConsumptionHabits() throws SQLException
	{
        // same
		Statement s = conn.createStatement();
		String query = "select * from electricitate";
		s.executeQuery(query);
		return s.getResultSet();
	}

	/**
	 * Queries the database for motion sensor states
	 * @return a ResultSet containing the data
	 * @throws SQLException
	 */
	public ResultSet getMotionSensorStates() throws SQLException
	{
        // same
		Statement s = conn.createStatement();
		String query = "select * from senzor_miscare";
		s.executeQuery(query);
		return s.getResultSet();
	}

	/**
	 * Queries the database for readings from a specific class of sensors.
	 * @param type sensor type (motion, water, temperature, electricity, gas)
	 * @return an ArrayList<Map<String, String>> containing the data
	 * @throws SQLException
	 */
	public ArrayList<Map<String, String>> getSensorReadings(String type) throws SQLException
	{
        // a little more interesting.
        // for each sensor type we have a special function so that we cand compress them more easily
        // with specific select statements.
		type = type.toLowerCase();
		if(type.equals("water"))
			return getWaterReadings();

		if(type.equals("motion"))
			return getMotionSensorReadings();

		if(type.equals("temperature"))
			return getReadingsFrom("temperature", "temperatura");

		if(type.equals("gas"))
			return getReadingsFrom("gas", "gaz");

		if(type.equals("electricity"))
			return getReadingsFrom("electricity", "curent");

		return null;
	}

	/**
	 * Queries the database for all water sensor readings
	 * @return an ArrayList<Map<String, String>> containing all data
	 */
	private ArrayList<Map<String, String>> getWaterReadings()
	{
        // get readings for water consumption from all rooms
		ArrayList<Map<String, String>> readings = new ArrayList<Map<String, String>>();

		readings.addAll(getReadingsFrom("water", "apa baie 1"));
		readings.addAll(getReadingsFrom("water", "apa baie 2"));
		readings.addAll(getReadingsFrom("water", "apa bucatarie"));

		return readings;
	}

	/**
	 * Queries the database for all motion sensor readings
	 * @return an ArrayList<Map<String, String>> containing all data
	 * @throws SQLException
	 */
	private ArrayList<Map<String, String>> getMotionSensorReadings() throws SQLException
	{
        // get motion sensor readings from all rooms
		ArrayList<Map<String, String>> readings = new ArrayList<Map<String, String>>();

		readings.addAll(getReadingsFrom("motion", "miscare baie 1"));
		readings.addAll(getReadingsFrom("motion", "miscare baie 2"));
		readings.addAll(getReadingsFrom("motion", "miscare bucatarie"));
		readings.addAll(getReadingsFrom("motion", "miscare dormitor 1"));
		readings.addAll(getReadingsFrom("motion", "miscare dormitor 2"));
		readings.addAll(getReadingsFrom("motion", "miscare living"));
		readings.addAll(getReadingsFrom("motion", "miscare hol"));
		readings.addAll(getReadingsFrom("motion", "miscare dormitor 3"));

		return readings;
	}

	/**
	 * Queries the database for readings from a specific sensor type and location
	 * @param type sensor type
	 * @param id sensor id
	 * @return an ArrayList<Map<String, String>> containing all data
	 */
	private ArrayList<Map<String, String>> getReadingsFrom(String type, String id)
	{
        // this function get all the values from a specific motion sensor and
        // compresses them into a map with the following values: type, location, start, end and value
		ArrayList<Map<String, String>> readings = new ArrayList<Map<String, String>>();
		
		Statement s;
		try {
            // query the database and get only specific values
			s = conn.createStatement();
			String query = "SELECT * FROM scenariu WHERE senzor LIKE \"%" + id + "%\"";
			ResultSet rs = s.executeQuery(query);

            // initiate the map
			rs.next();
			Map<String, String> reading = new HashMap<String, String>();
			reading.put("sensor-type", type);
			reading.put("sensor-location", getLocation(id));
			reading.put("start-time", formatTime(rs.getString("ora")));
			reading.put("stop-time", formatTime(rs.getString("ora")));
			reading.put("value", Double.toString(rs.getDouble("valoare")));

			// compress all entries that have the same values.
			// works like this:
			// - iterates over the ResultSet while the value read from the sensor is the same
			// - once it changes it adds the Map to the Array and makes a new map with the new value
			// - repeat.
			while(rs.next())
			{
				if(reading.get("value").equals(Double.toString(rs.getDouble("valoare"))))
					reading.put("stop-time", formatTime(rs.getString("ora")));
				else
				{
					readings.add(reading);
					reading = new HashMap<String, String>();
					reading.put("sensor-type", type);
					reading.put("sensor-location", getLocation(id));
					reading.put("start-time", formatTime(rs.getString("ora")));
					reading.put("stop-time", formatTime(rs.getString("ora")));
					reading.put("value", Double.toString(rs.getDouble("valoare")));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return readings;
	}

	/**
	 * Uses the time class to convert to the new time format
	 * @param time hh:mm:ss time string
	 * @return new formatted time
	 */
	private String formatTime(String time)
	{
        Time t = new Time();
        return t.fromString(time);
	}

	/**
	 * Gets location string given the database sensor id
	 * @param id sensor id
	 * @return sensor location
	 */
	private String getLocation(String id)
	{
        // gets the location of the sensor from its id with the use of a dictionary.
        return locationMap.get(id);
	}
}

