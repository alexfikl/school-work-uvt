/* *******************************************************************
* Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/


/**
 * The Time class provides some easy conversions for our time format.
 * @author Fikl ALexandru
 *
 */
public class Time {
	/**
	 * Class constructor
	 */
	public Time() {}

	/**
	 * Computes the number of seconds given the time.
	 * @param time an integer in (0, 235930)
	 * @return number of seconds
	 */
	private int getSeconds(int time)
	{
		return time % 100;
	}

	/**
	 * Computes the number of seconds given the time.
	 * @param time an integer in (0, 235930)
	 * @return number of minutes
	 */
	private int getMinutes(int time)
	{
		return (int)((time % 10000) / 100);
	}

	/**
	 * Computes the number of hours given the time.
	 * @param time an integer in (0, 235930)
	 * @return number of hours
	 */
	private int getHours(int time)
	{
		return (int)(time / 10000);
	}

    /**
     * Converts time from 'hh:mm:ss' to hhmmss
     * @param time old time format
     * @return new time format
     */
    public String fromString(String time)
    {
        String[] parts = time.split(":");
        StringBuffer newTime = new StringBuffer();
        for(String part: parts)
            newTime.append(String.format("%02s", part));

        return newTime.toString();
    }

	/**
	 * Converts time from an integer back to the original format
	 * @param time an integer in (0, 235930)
	 * @return returns newly formatted string.
	 */
	public String toString(int time)
	{
		String second, minute, hour;
		second = String.format("%02d", getSeconds(time));
		minute = String.format("%02d", getMinutes(time));
		hour = String.format("%02d", getHours(time));
		return hour + ":" + minute + ":" + second;
	}

	/**
	 * Increments a given time with 30 seconds
	 * @param time an integer in (0, 235930)
	 * @return new time
	 */
	public int increment(int time)
	{
		//we're always incrementing with 30 seconds;
		int seconds = getSeconds(time);
		int minutes = getMinutes(time);
		int hours = getHours(time);
		seconds = (seconds + 30);
		minutes = (minutes + (int)(seconds / 60));
		hours = (hours + (int)(minutes / 60)) % 24;
		time = hours * 10000 + (minutes % 60) * 100 + seconds % 60;
		return time;
	}
	
	/**
	 * Gets start time from a '17-18' type string and converts it to out internal format 
	 * @param time time interval
	 * @return start time in new format
	 */
	public String getStartTime(String time)
	{
		return fromString(time.split("-")[0] + ":00:00");
	}
	
	/**
	 * Gets end time from a '17-18' type string and converts it to our internal formal
	 * @param time time interval
	 * @return end time in new format
	 */
	public String getEndTime(String time)
	{
		return fromString(time.split("-")[1] + ":00:00");
	}
}
