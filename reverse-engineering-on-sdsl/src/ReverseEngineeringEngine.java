/* *******************************************************************
* Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/


import jess.*;
import jess.swing.JTextAreaWriter;
import javax.swing.JTextArea;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * The ReverseEngineeringEngine class takes care of all JESS engine
 * and fact manipulations.
 * @author Fikl Alexandru
 *
 */
public class ReverseEngineeringEngine {
	/**
	 * The JESS engine
	 */
	private static Rete engine;
	/**
	 * A JTextAreaWriter to pipe the output to
	 */
	private static JTextAreaWriter taw = null;

	/**
	 * Constructor for ReverseEngineeringEngine
	 */
	public ReverseEngineeringEngine()
	{
        //create engine
		engine = new Rete();
	}

	/**
	 * Sets the JTextArea to which the output will be piped.
	 * @param area used JTextArea
	 */
	public void setConnectedTextArea(JTextArea area)
	{
        // pipe output too a textarea
		taw = new JTextAreaWriter(area);
		engine.addOutputRouter("t", taw);
	    engine.addOutputRouter("WSTDOUT", taw);
	}

	/**
	 * Starts the engine. Adds the facts.
	 * @param person one of the four available people
	 * @throws JessException
	 */
	public void reverseEngineerScenario(String person) throws JessException
	{
        // reset and prepare templates
        engine.reset();
		engine.batch("templates.clp");

		// get readings from database and convert them to facts
		createSensorReadingsFacts(person);

        // get matrix
		createDefaultActionsFacts(person);

        // instantiate rules and run!
		engine.batch("rules.clp");
		engine.run();
	}

	/**
	 * Creates actions from the 3D Matrix and adds them to the engine.
	 * @throws JessException 
	 */
	private void createDefaultActionsFacts(String person) throws JessException
	{
		ArrayList<Map<String, String>> habits = new ArrayList<Map<String, String>>();
		XMLParser x = new XMLParser("src/habits.xml");
		habits = x.getHabits(person);
		
		for(Map<String, String> habit: habits)
		{
			Fact f = new Fact("habit", engine);
			f.setSlotValue("person", new Value(person));
			f.setSlotValue("location", new Value(habit.get("location"), RU.STRING));
			f.setSlotValue("activity", new Value(habit.get("activity"), RU.STRING));
			f.setSlotValue("start-time", new Value(Integer.parseInt(habit.get("start-time")), RU.INTEGER));
			f.setSlotValue("end-time", new Value(Integer.parseInt(habit.get("end-time")), RU.INTEGER));
		}
	}

	/**
	 * Creates facts from sensor readings for a specific person
	 * @param person one of the four available people
	 * @throws JessException
	 */
	private void createSensorReadingsFacts(String person) throws JessException
	{
		SDSLDatabase db = new SDSLDatabase();
		Fact f = null;
		ResultSet rs = null;
		ArrayList<Map<String, String>> readings = null;

		try {
            // get water consumption for a specific activity
            // pretty straight forward.
            // - get a ResultSet from the database
            // - iterate through it and for facts which we then add to the engine
			rs = db.getWaterConsumptionHabits();
			while(rs.next())
			{
				f = new Fact("water-used-for", engine);
				f.setSlotValue("object", new Value(rs.getString("tip_spalare"), RU.STRING));
				f.setSlotValue("consumption", new Value(rs.getDouble("consum"), RU.FLOAT));
				engine.assertFact(f);
			}

            // get electricity consumption for different electronics
			rs = db.getElectricityConsumptionHabits();
			while(rs.next())
			{
				f = new Fact("electricity-used-by-appliance", engine);
				f.setSlotValue("appliance", new Value(rs.getString("aparat"), RU.STRING));
				f.setSlotValue("state", new Value(rs.getString("stare"), RU.STRING));
				f.setSlotValue("max-consumption", new Value(rs.getDouble("consum_max"), RU.FLOAT));
				f.setSlotValue("min-consumption", new Value(rs.getDouble("consum_min"), RU.FLOAT));
				engine.assertFact(f);
			}

            // get motion sensor states. probably redundant. but maybe sometime they'll have more than two
			rs = db.getMotionSensorStates();
			while(rs.next())
			{
				f = new Fact("motion-sensor-state", engine);
				f.setSlotValue("state", new Value(rs.getString("stare"), RU.STRING));
				f.setSlotValue("value", new Value(rs.getInt("valoare"), RU.INTEGER));
				engine.assertFact(f);
			}

            // get readings from all sensors.
            // due to the fact that there are a lot of sensor readings, they were compressed
            // into periods of time with a specific value.
			String[] availableSensors = {"water", "temperature", "motion", "electricity", "gas"};
			for(String type: availableSensors)
			{
				readings = db.getSensorReadings(type);
				//System.out.println(type + " " + readings.size());
				for(Map<String, String> row: readings)
				{
					f = new Fact("sensor-reading", engine);
					//System.out.println("added:" + row.get("sensor-type") + " " + row.get("sensor-location") + " " + row.get("value"));
					f.setSlotValue("sensor-type", new Value(row.get("sensor-type"), RU.STRING));
					f.setSlotValue("sensor-location", new Value(row.get("sensor-location"), RU.STRING));
					f.setSlotValue("start-time", new Value(Integer.parseInt(row.get("start-time")), RU.INTEGER));
					f.setSlotValue("end-time", new Value(Integer.parseInt(row.get("stop-time")), RU.INTEGER));
					f.setSlotValue("value", new Value(Double.parseDouble(row.get("value")), RU.FLOAT));
					engine.assertFact(f);
				}
			}

			// TODO: read habit matrix thing.
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// close database
		db.close();
	}

}
