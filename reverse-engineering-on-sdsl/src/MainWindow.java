/* *******************************************************************
* Copyright (C) 2011 Ioana Jucu <ioana.jucu@gmail.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.io.*;

import jess.JessException;

/**
 * The MainWindow 
 * @author Ioana Jucu
 *
 */
public class MainWindow extends JFrame {
	/**
	 * Class serial version
	 */
	private static final long serialVersionUID = -5712595063914891853L;
    /**
     * Text area
     */
    private JTextArea    _editArea;
    /**
     * File Chooser
     */
    private JFileChooser _fileChooser = new JFileChooser();

    /**
     * View graphs action
     */
    private Action _viewGraphAction = new ViewGraphAction();
    /**
     * Save to file action
     */
    private Action _saveAction = new SaveAction();
    /**
     * Exit program action
     */
    private Action _exitAction = new ExitAction();

    /**
     * Main method
     * @param args system arguments
     */
    public static void main(String[] args) {
        new MainWindow();
    }

    /**
     * Class constructor
     */
    public MainWindow() {
        //... Create scrollable text area.
        _editArea = new JTextArea(15, 80);
        _editArea.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        _editArea.setFont(new Font("monospaced", Font.PLAIN, 14));
        JScrollPane scrollingText = new JScrollPane(_editArea);

        //-- Create a content pane, set layout, add component.
        JPanel content = new JPanel();
        content.setLayout(new BorderLayout());
        content.add(scrollingText, BorderLayout.CENTER);

        //... Create menubar
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = menuBar.add(new JMenu("File"));
        JMenu personMenu=menuBar.add(new JMenu("People"));
        JMenu graph=menuBar.add(new JMenu("View Graph"));
        //JMenu choosePersonMenu = new JMenu("Choose Person...");
        personMenu.add(new ChooseAction("Ion"));
        personMenu.add(new ChooseAction("Ana"));
        personMenu.add(new ChooseAction("Alina"));
        personMenu.add(new ChooseAction("Vlad"));
        fileMenu.setMnemonic('F');
        //personMenu.add(choosePersonMenu);
        graph.add(_viewGraphAction);
        fileMenu.add(_saveAction);
        fileMenu.addSeparator();
        fileMenu.add(_exitAction);

        //... Set window content and menu.
        setContentPane(content);
        setJMenuBar(menuBar);

        //... Set other window characteristics.
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Reverse Engineering on SDSL");
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * ViewGraphAction class provides a menu item for viewing the 
     * associated graphs
     * 
     * Inner class of MainWindow
     * @author Ioana Jucu
     *
     */
    class ViewGraphAction extends AbstractAction {
		private static final long serialVersionUID = -5523338384920647671L;

        /**
         * Class constructor
         */
        public ViewGraphAction() {
            super("View Graph");
            putValue(MNEMONIC_KEY, new Integer('G'));
        }

        /* (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
        	System.out.println("This is gonna show a graph of something.");
        	JFrame f = new JFrame();
            f.add(new GraphViewer());
            f.setSize(400,400);
            f.setLocation(200,200);
            f.setVisible(true);
        }
    }

    /**
     * SaveAction class provides a menu item used to view the save file dialog 
     * 
     * Inner class of MainWindow
     * @author Ioana Jucu
     *
     */
    class SaveAction extends AbstractAction {
		private static final long serialVersionUID = -9164297761309009473L;

        /**
         * Class constructor
         */
        public SaveAction() {
            super("Save...");
            putValue(MNEMONIC_KEY, new Integer('S'));
        }

        /* (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            int retval = _fileChooser.showSaveDialog(MainWindow.this);
            if (retval == JFileChooser.APPROVE_OPTION) {
                File f = _fileChooser.getSelectedFile();
                try {
                    FileWriter writer = new FileWriter(f);
                    _editArea.write(writer);  // Use TextComponent write
                } catch (IOException ioex) {
                    JOptionPane.showMessageDialog(MainWindow.this, ioex);
                    System.exit(1);
                }
            }
        }
    }

    /**
     * ChooseAction class provides a menu item used to start the 
     * ReverseEngineeringEngine
     * 
     *  Inner class of MainWindow 
     * @author Ioana Jucu
     *
     */
    class ChooseAction extends AbstractAction {
		/**
		 * Class serial version
		 */
		private static final long serialVersionUID = -9164297761309009473L;
		/**
		 * Name of the chosen person
		 */
		private String person;

        /**
         * Class Constructor
         * @param name the person's name
         */
        ChooseAction(String name) {
            super(name);
            person = name;
            putValue(MNEMONIC_KEY, new Integer('N'));
        }

        /* (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
        	System.out.println("This will choose a person for which to reconstruct the itinerary or whatnot.");
        	ReverseEngineeringEngine engine = new ReverseEngineeringEngine();
        	engine.setConnectedTextArea(_editArea);
        	try {
				engine.reverseEngineerScenario(person);
			} catch (JessException e1) {
				e1.printStackTrace();
				_editArea.setText("Unable to generate scenario for " + person + ".");
			}
			System.out.println("Scenario recreated.");
        }
    }

    /**
     * ExitAction provides a menu item to close the application
     * @author Ioana Jucu
     *
     */
    class ExitAction extends AbstractAction {
		private static final long serialVersionUID = 1517342878641232444L;

        /**
         * Class constructor
         */
        public ExitAction() {
            super("Exit");
            putValue(MNEMONIC_KEY, new Integer('X'));
        }

        /* (non-Javadoc)
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}