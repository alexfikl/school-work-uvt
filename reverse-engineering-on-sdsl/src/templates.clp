/* *******************************************************************
 * Copyright (C) 2011 Fikl Alexandru <alexfikl@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
***********************************************************************/

;consumption values for different actions
(deftemplate water-used-for
    (slot object
        (type STRING))
    (slot consumption
        (type FLOAT)))

;consumption ranges for different utilities
(deftemplate electricity-used-by-appliance
    (slot appliance
        (type STRING))
    (slot state
        (TYPE STRING))
    (slot max-consumption
        (type FLOAT))
    (slot min-consumption
        (type FLOAT)))

;motion sensor values
(deftemplate motion-sensor-state
    (slot state
        (type STRING))
    (slot value
        (type INTEGER))
)

;reading from the sensors
(deftemplate sensor-reading
    (slot start-time
        (type INTEGER))
    (slot end-time
        (type INTEGER))
    (slot sensor-type
        (type STRING))
    (slot sensor-location
        (type STRING))
 	(slot value
        (type NUMBER))
    (slot read
        (type INTEGER)
        (default 0)))

;timer
(deftemplate timer
    (slot time
        (type INTEGER))
)

;habits for different people so we can distinguish between ambiguous sensor readings
(deftemplate habit
    (slot start-time
        (type INTEGER))
    (slot end-time
        (type INTEGER))
    (slot person
        (type STRING)
        (allowed-values "Vlad" "Ion" "Ana" "Alina"))
    (slot location
        (type STRING))
    (slot activity
        (type STRING))
    (slot probability
        (type INTEGER))
)

(deftemplate answer
	(slot when
        (type INTEGER))
    (slot who
        (type STRING)
        (allowed-values "Vlad" "Ion" "Ana" "Alina"))
    (slot where
        (type STRING))
    (slot what
        (type STRING))
)

(deftemplate last-location
    (slot location
        (type STRING))
    (slot changed
        (type INTEGER))
)