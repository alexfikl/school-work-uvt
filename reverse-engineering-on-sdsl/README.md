# Dependancies

* [Eclipse](https://www.eclipse.org/swt/) version **3.6.2**.
* [SWT](https://www.eclipse.org/swt/).
* [JESS](http://www.jessrules.com/download.shtml) version **7.1p2**.
* [JDK](http://openjdk.java.net/install/) version **6 update 22**.
* [MySQL Connector](http://www.mysql.com/downloads/connector/j/) version **5.0.8**.
* [XAMPP](http://www.apachefriends.org/en/xampp-windows.html).

Dependancies can be downloaded and installed from their respective websites
or through your distribution if you are on Linux.

# Installation

1. Open Eclipse
2. Start a new project
3. Add current directory to it
5. Go into the `sdsl/data/mysql` directory.
7. On Linux: with sudo rights copy this directory to /var/lib/mysql
   On Windows: (assuming installed XAMPP) copy to xampp/mysql/data/ [5]
8. Start MySQL database
9. In Eclipse: run the program

# Troubleshooting

Any problems or alternative installations will be assisted by google.com
