<?php
// Info
$server = "localhost";
$username = "root";
$password = "root";

// Make a MySQL Connection
$link = mysqli_connect($server, $username, $password);
if (mysqli_connect_errno()) {
    die(mysqli_connect_error());
}
mysqli_select_db($link, "books") or die(mysqli_error($link));

// Get all categories
$categories = mysqli_query($link, "SELECT * FROM categories") or die(mysqli_error($link));

// Create books array
$books = array( 'root' => 'Books', 'items' => array());

// Iterate through categories
while($categoryItem = mysqli_fetch_array($categories)) {
    // Create category array
    $category = array('title' => $categoryItem['Name'], 'items' => array());

    // Select all books from a specific category
    $query = "SELECT * FROM book_details WHERE Category='" . $categoryItem['Name'] . "'";
    $booksForCategory = mysqli_query($link, $query) or die(mysqli_error($link));

    // Iterate through books
    while($bookItem = mysqli_fetch_array( $booksForCategory )){
        // Create a book array with the book details and add it to the category array
        $category['items'][] = array(
            'author' => $bookItem['Author'],
            'title' => $bookItem['Title'],
            'language' => $bookItem['Language'],
            'date' => $bookItem['Date'],
            'pages' => $bookItem['Pages'],
            'cover' => $bookItem['Cover'],
            'leaf' => true);
    }
    // Add category to books
    $books['items'][] = $category;
}

// duh!
echo json_encode($books, JSON_PRETTY_PRINT);
?>
