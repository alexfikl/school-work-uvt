DROP DATABASE IF EXISTS books;
CREATE DATABASE IF NOT EXISTS books;
USE books;

CREATE TABLE categories (Name VARCHAR(64) NOT NULL);

CREATE TABLE book_details (
    Author VARCHAR(128) NOT NULL,
    Title VARCHAR(256) NOT NULL,
    Date INT,
    Language VARCHAR(64),
    Pages INT,
    Category VARCHAR(64) NOT NULL,
    Cover VARCHAR(256)
);
