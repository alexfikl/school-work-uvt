USE books;

INSERT INTO categories VALUES('Fiction');
INSERT INTO categories VALUES('Religion');
INSERT INTO categories VALUES('Science');

INSERT INTO book_details VALUES(
    "David Baldacci",
    "Hell's Corner",
    2011,
    "English",
    656,
    "Religion",
    "david_baldacci_-_hell_s_corner.jpg"
);

INSERT INTO book_details VALUES(
    "Alvin Plantinga",
    "Warranted Christian Belief",
    2011,
    "English",
    656,
    "Religion",
    "alvin_plantinga_-_warranted_christian_belief.jpg"
);

INSERT INTO book_details VALUES(
    "Richard Dawkins",
    "The God Delusion",
    2011,
    "English",
    656,
    "Religion",
    "richard_dawkins_-_the_god_delusion.jpg"
);

INSERT INTO book_details VALUES(
    "Jordan Howard Sobel",
    "Logic and Theism",
    2011,
    "English",
    656,
    "Religion",
    "jordan_howard_sobel_-_logic_and_theism.jpg"
);

INSERT INTO book_details VALUES(
    "James Patterson",
    "Cross Fire",
    2011,
    "English",
    656,
    "Fiction",
    "james_patterson_-_cross_fire.jpg"
);

INSERT INTO book_details VALUES(
    "Stieg Larsson",
    "The Girl Who Kicked the Hornet's Nest",
    2011,
    "English",
    656,
    "Fiction",
    "stieg_larsson_-_the_girl_who_kicked_the_hornet_s_nest.jpg"
);

INSERT INTO book_details VALUES(
    "Stieg Larsson",
    "The Girl With the Dragon Tattoo",
    2011,
    "English",
    656,
    "Fiction",
    "stieg_larsson_-_the_girl_with_the_dragon_tattoo.jpg"
);

INSERT INTO book_details VALUES(
    "John Grisham",
    "The Confession: A Novel",
    2011,
    "English",
    656,
    "Fiction",
    "john_grisham_-_the_confession__a_novel.jpg"
);

INSERT INTO book_details VALUES(
    "Stephen Hawking",
    "The Grand Design",
    2011,
    "English",
    656,
    "Science",
    "stephen_hawking_-_the_grand_design.jpg"
);

INSERT INTO book_details VALUES(
    "Mark Summerfield",
    "Rapid GUI Programming with Python and Qt",
    2011,
    "English",
    656,
    "Science",
    "mark_summerfiled_-_rapid_gui_programming_with_python_and_qt.jpg"
);
