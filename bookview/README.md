# Dependencies

Needs Sencha Touch to run the website. Unfortunately these days Sencha Touch
is mostly a commercial product, so you may not be able to get your hands on
it as easily. Another problem is that this project is from 2010, so new
versions won't work (and I won't spend time to port it).

If you do manage to get your hands on an old version of Sench Touch,
extract into the `js` directory and rename it to `sencha` or change the
paths in `index.html`.

**NOTE**: You may need to add `Access-Control-Allow-Origin "*"` to your
Apache configuration.

**NOTE**: At the time of writing, I had to add the `--disable-web-security`
flag to Google Chrome for local testing.

# Database

The database use in the app has the following schema. There's a `scheme.sql`
file in the `data` directory for easy importing.

----------------------
|   book_details     |
----------------------
|Author VARCHAR(128) |
|Title VARCHAR(128)  |
|Date INT            |
|Language VARCHAR(64)|
|Pages INT           |
|Category VARCHAR(64)|
|Cover VARCHAR(256)  |
----------------------

----------------------
|    categories      |
----------------------
|Name VARCHAR(64) PK |
----------------------

