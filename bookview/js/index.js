Ext.regModel('Book', {
    fields: [
        {name: 'title',     type: 'string'},
        {name: 'author',    type: 'string'},
        {name: 'language',  type: 'string'},
        {name: 'date',      type: 'string'},
        {name: 'cover',     type: 'string'},
        {name: 'pages',     type: 'int'}
    ]
});

var ItemTemplate =
    '<tpl if="leaf != true">' +
        '{title}' +
    '</tpl>' +
    '<tpl if="leaf == true">' +
        '<p style="background: url(\'cover/small/{cover}\') no-repeat;" id="book-item-tpl">' +
            '<b>{title}</b> by <b>{author}</b>' +
        '</p>' +
     '</tpl>';

var BookDetails = new Ext.XTemplate('<img src="data/cover/large/{cover}" style="float:left;"/>',
                                    '<table style="float:left;font-size: 25px;margin-left:20px;">',
                                    '<tr><td style="padding: 0px 10px 5px 0px;"><b>Title:</b></td> <td>{title}</td></tr>',
                                    '<tr><td style="padding: 0px 10px 5px 0px;"><b>Author:</b></td> <td>{author}</td></tr>',
                                    '<tr><td style="padding: 0px 10px 5px 0px;"><b>Language:</b></td> <td>{language}</td></tr>',
                                    '<tr><td style="padding: 0px 10px 5px 0px;"><b>Date:</b></td> <td>{date}</td></tr>',
                                    '<tr><td style="padding: 0px 10px 5px 0px;"><b>Pages:</b></td> <td>{pages}</td></tr>',
                                    '</table>');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                              FUNCTIONS
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

Ext.setup({
    icon: 'images/icon.png',
    tabletStartupScreen: 'tablet_startup.png',
    phoneStartupScreen: 'phone_startup.png',
    glossOnIcon: false,
    onReady: function(){

        var currentItem = {};

        var bookExcerpt = Ext.extend(Ext.Component, {
            html: '<p>Loading...</p>',
            styleHtmlContent: true,
            scroll: 'vertical'
        });

        var bookInfo = Ext.extend(Ext.Component, {
            html: '<p>Loading...</p>',
            scroll: 'vertical'
        });

        var bookDetails = Ext.extend(Ext.Container, {
            style: 'margin: 10px 0px 0px 10px',
            layout:'card',
            cardSwitchAnimation: 'slide',
            activeItem: 0,
            scroll: 'vertical'
        });

        var store = new Ext.data.TreeStore({
            model: 'Book',
            //model: 'File',
            proxy: {
                type: 'ajax',
                url: 'db.php',
                reader: {
                    type: 'tree',
                    root: 'items'
                }
            }
        });

        var excerptBtn = new Ext.Button({
            text: 'Book Excerpt',
            handler: function(btn, ev){
                var cards = currentItem.details.getLayout();
                var item = cards.getLayoutItems()[1];
                //item.html = 'Kiss my metal ass';

                if(this.getText() == 'Book Excerpt'){
                    this.setText('Book Details');

                    Ext.Ajax.request({
                        url: 'data/description/' + currentItem.excerpt,
                        success: function(response) {
                            item.update('<h3>Small Excerpt from "' + currentItem.title + '".</h3><p>' + response.responseText + '</p>');
                            cards.setActiveItem(1);
                        },
                        failure: function() {
                            item.update('Loading of excerpt from "' + currentItem.title + '" failed.');
                            cards.setActiveItem(1);
                        }
                    });
                }
                else {
                    this.setText('Book Excerpt');
                    cards.setActiveItem(0);
                }
            }
        });

        var nestedList = new Ext.NestedList({
            fullscreen: true,
            title: 'Books',
            displayField: 'title',
            // for leaves show pic + title + author
            getItemTextTpl: function() {
                return ItemTemplate;
            },
            // provide more info about books
            getDetailCard: function(){
                return new bookDetails({
                    items: [new bookInfo(), new bookExcerpt()]
                });
            },
            onBackTap: function() {
                var currList      = this.getActiveItem(),
                currIdx       = this.items.indexOf(currList);
                excerptBtn.setVisible(false);

                if (currIdx != 0) {
                    var prevDepth     = currIdx - 1,
                        prevList      = this.items.getAt(prevDepth),
                        recordNode    = prevList.recordNode,
                        record        = recordNode.getRecord(),
                        parentNode    = recordNode ? recordNode.parentNode : null,
                        backBtn       = this.backButton,
                        backToggleMth = (prevDepth !== 0) ? 'show' : 'hide',
                        backBtnText;

                    this.on('cardswitch', function(newCard, oldCard) {
                        var selModel = prevList.getSelectionModel();
                        this.remove(currList);
                        if (this.clearSelectionDelay) {
                            Ext.defer(selModel.deselectAll, this.clearSelectionDelay, selModel);
                        }
                    }, this, {single: true});

                    this.setActiveItem(prevList, {
                        type: this.cardSwitchAnimation,
                        reverse: true,
                        scope: this
                    });
                    this.syncToolbar(prevList);
                }
            },
            toolbar: {
                items: [{xtype: 'spacer'}, excerptBtn]
            },
            store: store
        });

        excerptBtn.setVisible(false);

        nestedList.on('leafitemtap', function(subList, subIdx, el, e, details) {
            //set data for first card -> info
            var ds = subList.getStore(), r  = ds.getAt(subIdx).data, excerpt;
            details.getLayout().getActiveItem().html = BookDetails.apply(r);

            currentItem = {excerpt: r.cover.replace('.jpg', '.txt'), title: r.title, details: details};

            excerptBtn.setVisible(true);
        });

    }
});
