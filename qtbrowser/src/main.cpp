#include <QtGui/QApplication>
#include "browserwrapper.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    BrowserWrapper *w = new BrowserWrapper();
    w->openWindow();

    return a.exec();
}
