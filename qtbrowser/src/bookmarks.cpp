/****************************************************************************
  **
  ** Copyright (C) 2005-2008 Trolltech ASA. All rights reserved.
  **
  ** This file is part of the documentation of the Qt Toolkit.
  **
  ** This file may be used under the terms of the GNU General Public
 ** License versions 2.0 or 3.0 as published by the Free Software
 ** Foundation and appearing in the files LICENSE.GPL2 and LICENSE.GPL3
 ** included in the packaging of this file.  Alternatively you may (at
 ** your option) use any later version of the GNU General Public
 ** License if such license has been publicly approved by Trolltech ASA
 ** (or its successors, if any) and the KDE Free Qt Foundation. In
 ** addition, as a special exception, Trolltech gives you certain
 ** additional rights. These rights are described in the Trolltech GPL
 ** Exception version 1.2, which can be found at
 ** http://www.trolltech.com/products/qt/gplexception/ and in the file
 ** GPL_EXCEPTION.txt in this package.
 **
 ** Please review the following information to ensure GNU General
 ** Public Licensing requirements will be met:
 ** http://trolltech.com/products/qt/licenses/licensing/opensource/. If
 ** you are unsure which license is appropriate for your use, please
 ** review the following information:
 ** http://trolltech.com/products/qt/licenses/licensing/licensingoverview
 ** or contact the sales department at sales@trolltech.com.
 **
 ** In addition, as a special exception, Trolltech, as the sole
 ** copyright holder for Qt Designer, grants users of the Qt/Eclipse
 ** Integration plug-in the right for the Qt/Eclipse Integration to
 ** link to functionality provided by Qt Designer and its related
 ** libraries.
 **
 ** This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
 ** INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE. Trolltech reserves all rights not expressly
 ** granted herein.
  **
  ** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
  ** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  **
  ****************************************************************************/

#include <QtGui>

#include "bookmarks.h"
#include "xbel.h"

Bookmarks::Bookmarks(QWidget *parent): QDialog(parent)
{
    QStringList labels;
    labels << tr("Title") << tr("Location");

    treeWidget = new QTreeWidget;
    treeWidget->header()->setResizeMode(QHeaderView::ResizeToContents);
    treeWidget->setHeaderLabels(labels);
    treeWidget->setSortingEnabled(true);

    import("./defaultbookmarks.xbel");

    m_import = new QPushButton(tr("&Import..."), this);
    m_import->setShortcut(tr("Ctrl+I"));
    connect(m_import, SIGNAL(clicked()), this, SLOT(import()));

    m_export = new QPushButton(tr("&Export..."), this);
    m_export->setShortcut(tr("Ctrl+E"));
    connect(m_export, SIGNAL(clicked()), this, SLOT(exports()));

    m_close = new QPushButton(tr("&Close"), this);
    m_close->setShortcut(tr("Ctrl+C"));
    connect(m_close, SIGNAL(clicked()), this, SLOT(close()));

    QVBoxLayout *buttonlayout = new QVBoxLayout;
    buttonlayout->addWidget(m_import);
    buttonlayout->addWidget(m_export);
    buttonlayout->addStretch();
    buttonlayout->addWidget(m_close);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(treeWidget);
    layout->addLayout(buttonlayout);

    setLayout(layout);
    setFixedSize(800, 500);;

    setWindowTitle(tr("Bookmarks Editor"));
}

void Bookmarks::import(QString fileName)
{
    if (fileName.isEmpty()){
        fileName = QFileDialog::getOpenFileName(this, tr("Open Bookmark File"),
                                                QDir::currentPath(), tr("XBEL Files (*.xbel *.xml)"));
    }

    treeWidget->clear();

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("QXmlStream Bookmarks"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    XbelReader reader(treeWidget);
    if (!reader.read(&file)) {
        QMessageBox::warning(this, tr("QXmlStream Bookmarks"),
                             tr("Parse error in file %1 at line %2, column %3:\n%4")
                             .arg(fileName)
                             .arg(reader.lineNumber())
                             .arg(reader.columnNumber())
                             .arg(reader.errorString()));
    }
}

void Bookmarks::exports()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Bookmark File"),
                                                    QDir::currentPath(), tr("XBEL Files (*.xbel *.xml)"));
    if (fileName.isEmpty())
        return;

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Bookmarks Editor"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    XbelWriter writer(treeWidget);
    if (!writer.writeFile(&file)){
        QMessageBox::warning(this, tr("QXmlStream Bookmarks"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

}

void Bookmarks::addBookmark()
{
    return;
}

#include "moc_bookmarks.cpp"
