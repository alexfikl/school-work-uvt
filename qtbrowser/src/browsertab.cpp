#include "browsertab.h"

BrowserTab::BrowserTab(QWidget* parent): QTabWidget(parent)
{
    m_tabs = new QTabBar(this);
    m_tabs->setTabsClosable(true);
    setTabBar(m_tabs);
    connect (m_tabs, SIGNAL (tabCloseRequested (int)), this, SLOT (closeTab (int)));
}

BrowserTab::~BrowserTab()
{
    tabBar()->deleteLater();
}

void BrowserTab::closeTab(int index)
{
    m_tabs->removeTab(index);
}

void BrowserTab::hideTabs()
{
    tabBar()->hide();
}
void BrowserTab::showTabs()
{
    tabBar()->show();
}

void BrowserTab::contextMenuRequested(const QPoint & position)
{
    Q_UNUSED(position);
    //TODO Add this context menu
}

#include "moc_browsertab.cpp"
