#ifndef BROWSERWEBPAGE_H
#define BROWSERWEBPAGE_H

#include <QWebPage>
#include <QNetworkRequest>

class BrowserWebPage : public QWebPage
{
    Q_OBJECT
public:
    BrowserWebPage(QObject* parent);

    void setKeys(Qt::MouseButtons buttons, Qt::KeyboardModifiers modifiers);

signals:
    void newViewRequest(bool newWindow, QNetworkRequest request);
    void showPageSource();

protected:
    bool acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type);

private:
    Qt::MouseButtons m_mouseButtons;
    Qt::KeyboardModifiers m_kbdMods;
};

#endif // BROWSERWEBPAGE_H
