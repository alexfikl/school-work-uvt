#ifndef BROWSER_H
#define BROWSER_H

#include "browsertab.h"
#include "browserwebview.h"

#include <QMainWindow>
#include <QNetworkRequest>
#include <QProgressBar>
#include <QComboBox>
#include <QAction>
#include <QWidgetAction>
#include <QToolButton>
#include <QPointer>

static const signed int CLOSECURRENT = -5;
static const int TruncateLengthShort = 30;
static const int TruncateLengthLong = 65;


class Browser : public QMainWindow
{
    Q_OBJECT

public:
    Browser(QWidget *parent = 0);

    static void setHome(QUrl home)  { s_home = home; }
    static const QUrl getHome()     { return s_home; }

public slots:
    void newView(bool newWin = false, QNetworkRequest request = QNetworkRequest(QUrl("about:blank")));

signals:
    void newWindow(QNetworkRequest);
    void windowClosing();
    void newWindow();

private slots:
    void addressEntered();
    void toggleFullscreen();
    void showPageSource();
    void showWebInspector();
    void showBookmarkEditor();
    void showAddBookmark();
    void showAbout();

    void goHome();
    void setCurrentUrl(QUrl url, void*);
    void setCurrentTitle(QString title, void*);
    void setIcon(QIcon icon, void*);
    void setStatusBarText(QString text, void*);
    void loadProgress(int progress, void*);

    void tabPageChanged(int index);
    void closeTab(signed int index = CLOSECURRENT);
    void clearAddressBar();

    void pageReload()   { m_activeView->reload(); }
    void pageBack()     { m_activeView->back(); }
    void pageForward()  { m_activeView->forward(); }
    void pageStop()     { m_activeView->stop(); }

private:
    int addressToIndex(void* address);
    void createActions();
    void createToolbar();

    BrowserTab *m_tabs;
    QPointer<BrowserWebView> m_activeView;

    QProgressBar *m_progress;
    QComboBox *m_addressBar;
    QToolButton *m_menubutton;
    QToolBar *m_toolbar;

    QAction *m_addBookmark;
    QAction *m_exit;
    QAction *m_bookmarks;
    QAction *m_reloadStop;
    QAction *m_pageSource;
    QAction *m_fullscreen;
    QAction *m_back;
    QAction *m_forward;
    QAction *m_home;
    QWidgetAction *m_addressWidget;
    QAction *m_newWindow;
    QAction *m_newTab;
    QAction *m_closeTab;

    static QUrl s_home;
    static bool s_allowNoTabs;
    static QIcon DefaultIcon;
};

#endif // BROWSER_H
