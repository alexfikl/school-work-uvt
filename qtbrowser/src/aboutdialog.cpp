#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#include <QPushButton>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

#include "moc_aboutdialog.cpp"
