#ifndef BROWSERWRAPPER_H
#define BROWSERWRAPPER_H

#include <QObject>
#include <QUrl>
#include <QNetworkRequest>

class BrowserWrapper : public QObject {
    Q_OBJECT
public:
    BrowserWrapper();
    void openWindow();

private slots:
    void newBrowserWindow(QNetworkRequest request = QNetworkRequest(QUrl("about:blank")));
};

#endif // BROWSERWRAPPER_H
