#include "browserwebview.h"

#include <QWebHitTestResult>
#include <QMenu>


BrowserWebView::BrowserWebView(QWidget* parent) : QWebView(parent), m_progress(0), m_urlWanted("")
{
    m_page = new BrowserWebPage(this);
    setPage(m_page);

    connect(this, SIGNAL(urlChanged(QUrl)), this, SLOT(urlCatcher(QUrl)));
    connect(this, SIGNAL(titleChanged(QString)), this, SLOT(titleCatcher(QString)));
    connect(this, SIGNAL(statusBarMessage(QString)), this, SLOT(statusBarCatcher(QString)));
    connect(page(), SIGNAL(linkHovered(const QString&, const QString&, const QString&)),
            this, SLOT(linkHoverCatcher(const QString&, const QString&, const QString&)));
    connect(this, SIGNAL(iconChanged()), this, SLOT(iconChangeCatcher()));
    connect(this, SIGNAL(loadProgress(int)), this, SLOT(loadProgressCatcher(int)));
    connect(this, SIGNAL(urlChanged(QUrl)), this, SLOT(setAddressEditString(QUrl)));
}

BrowserWebView::~BrowserWebView()
{
    m_page->disconnect();
}

void BrowserWebView::urlCatcher(const QUrl& url)
{
    emit urlChanged(url, (void*)this);
}

void BrowserWebView::titleCatcher(const QString& title)
{
    if(!title.isEmpty())
        emit titleChanged(title, (void*)this);
    else
        emit titleChanged(url().toString(), (void*)this);
}

void BrowserWebView::statusBarCatcher(const QString& text)
{
    m_statusText = text;
    emit statusBarMessage(text, (void*)this);
}

void BrowserWebView::linkHoverCatcher(const QString& link, const QString& title, const QString& textContent)
{
    Q_UNUSED(title);
    Q_UNUSED(textContent);
    emit linkHovered(link, (void*)this);
}

void BrowserWebView::iconChangeCatcher()
{
    emit iconChanged(icon(), (void*)this);
}



QString* BrowserWebView::getAddressEditString()
{
    return &m_addressEditString;
}


void BrowserWebView::setAddressEditString(QString addressStrn)
{
    m_addressEditString.clear();
    m_addressEditString.append(addressStrn);
}

void BrowserWebView::loadProgressCatcher(int progress)
{
    m_progress = progress;
    emit loadProgress(progress, (void*)this);
}

void BrowserWebView::setAddressEditString(QUrl url)
{
    m_addressEditString = url.toString();
}

int BrowserWebView::getProgress()
{
    return m_progress;
}

QString BrowserWebView::getStatusText()
{
    return m_statusText;
}

void BrowserWebView::mousePressEvent(QMouseEvent *event)
{
    m_page->setKeys(event->buttons(), event->modifiers());
    QWebView::mousePressEvent(event);
}

void BrowserWebView::contextMenuEvent(QContextMenuEvent * event)
{
    //TODO Simplify this, reduce the duplicate code.
    QWebHitTestResult test = m_page->mainFrame()->hitTestContent(event->pos());
    QMenu menu(this);
    if(!test.imageUrl().isEmpty())
    {
        m_urlWanted = test.imageUrl();
        if(!test.linkUrl().isEmpty())
        {
            m_urlWanted = test.linkUrl();
            menu.addAction("Open in New Window", this, SLOT(newWinRequestCatcher()));
            menu.addAction("Open in New Tab", this, SLOT(newTabRequestCatcher()));
            menu.addSeparator();
            menu.addAction(pageAction(QWebPage::CopyLinkToClipboard));
            menu.addSeparator();
        }
        menu.addAction(pageAction(QWebPage::OpenImageInNewWindow));
        menu.addAction(pageAction(QWebPage::CopyImageToClipboard));
        menu.addAction(pageAction(QWebPage::DownloadImageToDisk));
        menu.addSeparator();
        menu.addAction(pageAction(QWebPage::Back));
        menu.addAction(pageAction(QWebPage::Forward));
        menu.addAction(pageAction(QWebPage::Reload));
        menu.exec(mapToGlobal(event->pos()));
    }
    else if(!test.linkUrl().isEmpty())
    {
        m_urlWanted = test.linkUrl();
        menu.addAction("Open in New Window", this, SLOT(newWinRequestCatcher()));
        menu.addAction("Open in New Tab", this, SLOT(newTabRequestCatcher()));
        menu.addSeparator();
        menu.addAction(pageAction(QWebPage::CopyLinkToClipboard));
        menu.addAction(pageAction(QWebPage::InspectElement));
        menu.addSeparator();
        menu.addAction(pageAction(QWebPage::Back));
        menu.addAction(pageAction(QWebPage::Forward));
        menu.addAction(pageAction(QWebPage::Reload));
        menu.exec(mapToGlobal(event->pos()));
    }
    else // Not a link or image
    {
        m_urlWanted = "about:youShouldntSeeThisPage";
        menu.addAction(pageAction(QWebPage::Back));
        menu.addAction(pageAction(QWebPage::Forward));
        menu.addAction(pageAction(QWebPage::Reload));
        menu.addSeparator();
        menu.addAction("Show Page Source", this, SLOT(showSourceCatcher()));
        menu.addAction(pageAction(QWebPage::InspectElement));
        menu.exec(mapToGlobal(event->pos()));
    }

}

void BrowserWebView::showSourceCatcher()
{
    emit showPageSource();
}

void BrowserWebView::newWinRequestCatcher()
{
    emit newViewRequest(true, QNetworkRequest(m_urlWanted));
}

void BrowserWebView::newTabRequestCatcher()
{
    emit newViewRequest(false, QNetworkRequest(m_urlWanted));
}

#include "moc_browserwebview.cpp"
