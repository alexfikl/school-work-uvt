#ifndef BROWSERWEBVIEW_H
#define BROWSERWEBVIEW_H

#include "browserwebpage.h"

#include <QWebView>
#include <QMouseEvent>

class BrowserWebView : public QWebView
{
    Q_OBJECT
public:
    BrowserWebView(QWidget* parent);
    ~BrowserWebView();

public slots:
    int getProgress();
    QString getStatusText();

    void setAddressEditString(QString addressStrn);
    void setAddressEditString(QUrl url);
    QString* getAddressEditString();

protected:
    void mousePressEvent(QMouseEvent *event);
    void contextMenuEvent(QContextMenuEvent *event);

signals:
    void urlChanged(const QUrl&, void*);
    void titleChanged(const QString&, void*);
    void statusBarMessage( const QString&, void*);
    void linkHovered(const QString&, void*);
    void iconChanged(QIcon, void*);
    void loadProgress(int, void*);
    void newViewRequest(bool newWindow, QNetworkRequest request);
    void showPageSource();

private:
    QString m_addressEditString;
    int m_progress;
    QString m_statusText;
    BrowserWebPage *m_page;
    QUrl m_urlWanted;

private slots:
    void urlCatcher(const QUrl&);
    void titleCatcher(const QString&);
    void statusBarCatcher(const QString&);
    void linkHoverCatcher(const QString&, const QString&, const QString&);
    void iconChangeCatcher();
    void loadProgressCatcher(int progress);
    void newTabRequestCatcher();
    void newWinRequestCatcher();
    void showSourceCatcher();

};
#endif // BROWSERWEBVIEW_H
