#include "browserwrapper.h"
#include "browser.h"

#include <QUrl>
#include <QDebug>

BrowserWrapper::BrowserWrapper() :
        QObject()
{
}

void BrowserWrapper::openWindow()
{
    newBrowserWindow(QNetworkRequest(Browser::getHome()));
}

void BrowserWrapper::newBrowserWindow(QNetworkRequest request)
{
    Browser* window = new Browser();

    connect(window, SIGNAL(newWindow()), this, SLOT(newBrowserWindow()));
    connect(window, SIGNAL(newWindow(QNetworkRequest)),this, SLOT(newBrowserWindow(QNetworkRequest)));

    window->newView(false, request);
    window->show();
}

#include "moc_browserwrapper.cpp"
