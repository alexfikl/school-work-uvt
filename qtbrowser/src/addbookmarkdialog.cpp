#include "addbookmarkdialog.h"

#include <QtGui>
#include <QDialog>
#include <QTreeWidgetItem>
#include <QLineEdit>
#include <QComboBox>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>

AddBookmarkDialog::AddBookmarkDialog(const QString url, const QString title, QWidget *parent):
        QDialog(parent),
        m_url(url),
        m_title(title)
{

    setWindowTitle("Add Bookmark");
    QLabel *label = new QLabel("Type a name for the bookmark and where to keep it", this);

    m_titleEdit = new QLineEdit(this);
    m_titleEdit->setText(title);

    m_location = new QComboBox(this);


    QPushButton *okBnt = new QPushButton("&OK",this);
    okBnt->setDefault(true);
    okBnt->setIcon(style()->standardIcon(QStyle::SP_DialogOkButton));
    connect(okBnt, SIGNAL(clicked()), this, SLOT(accept()));

    QPushButton *cancelBnt = new QPushButton("&Cancel", this);
    cancelBnt->setIcon(style()->standardIcon(QStyle::SP_DialogCancelButton));
    connect(cancelBnt, SIGNAL(clicked()), this, SLOT(reject()));

    QHBoxLayout *buttons = new QHBoxLayout;
    buttons->addStretch();
    buttons->addWidget(okBnt);
    buttons->addWidget(cancelBnt);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addWidget(m_titleEdit);
    layout->addWidget(m_location);
    layout->addSpacing(10);
    layout->addLayout(buttons);

    setLayout(layout);
    setFixedSize(sizeHint().width(), sizeHint().height());
}

#include "moc_addbookmarkdialog.cpp"
