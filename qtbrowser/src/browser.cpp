#include "browser.h"
#include "aboutdialog.h"
#include "bookmarks.h"
#include "addbookmarkdialog.h"

#include <QtGui>
#include <QtWebKit>
#include <QLocale>
#include <QString>
#include <QUrl>
#include <QStatusBar>
#include <QMenu>
#include <QToolButton>
#include <QDebug>

QUrl Browser::s_home("http://www.google.com/");
bool Browser::s_allowNoTabs(false);

Browser::Browser(QWidget *parent): QMainWindow(parent)
{
    setWindowIcon(QIcon("./icons/akonadi.png"));
    m_activeView = new BrowserWebView(this);
    m_tabs = new BrowserTab(this);
    m_progress = new QProgressBar(this);

    createActions();
    createToolbar();

    m_tabs->hideTabs();
    setCentralWidget(m_tabs);

    m_progress->setMaximumSize(150, 20);
    statusBar()->addPermanentWidget(m_progress, -5);

    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    connect(m_tabs, SIGNAL(currentChanged(int)), this, SLOT(tabPageChanged(int)));
}

void Browser::createActions()
{
    m_exit = new QAction("E&xit", this);
    m_exit->setShortcut(QKeySequence::Quit);
    m_exit->setStatusTip("Close current window");
    connect(m_exit, SIGNAL(triggered()), this, SLOT(close()));

    m_newTab = new QAction("New &Tab", this);
    m_newTab->setShortcut(QString("Ctrl+T"));
    m_newTab->setStatusTip("Open New Tab");
    connect(m_newTab, SIGNAL(triggered()), this, SLOT(newView()));

    m_closeTab = new QAction("Close Tab", this);
    m_closeTab->setShortcut(QString("Ctrl+W"));;
    m_closeTab->setStatusTip("Close Tab");
    connect(m_closeTab, SIGNAL(triggered()), this, SLOT(closeTab()));

    m_newWindow = new QAction("&New Window", this);
    m_newWindow->setShortcut(QString("Ctrl+N"));
    m_newWindow->setStatusTip("Open New Window");
    connect(m_newWindow, SIGNAL(triggered()), this, SIGNAL(newWindow()));

    m_reloadStop = new QAction("Reload", this);

    m_back = new QAction("Back", this);
    m_back->setIcon(style()->standardIcon(QStyle::SP_ArrowBack));
    connect(m_back, SIGNAL(triggered()), this, SLOT(pageBack()));

    m_forward = new QAction("Forward", this);
    m_forward->setIcon(style()->standardIcon(QStyle::SP_ArrowForward));
    connect(m_forward, SIGNAL(triggered()), this, SLOT(pageForward()));

    m_home = new QAction("Home", this);
    m_home->setIcon(style()->standardIcon(QStyle::SP_DirHomeIcon));
    connect(m_home, SIGNAL(triggered()), this, SLOT(goHome()));

    m_fullscreen = new QAction("&Fullscreen", this);
    m_fullscreen->setIcon(style()->standardIcon(QStyle::SP_TitleBarMaxButton));
    m_fullscreen->setShortcut(QString("F11"));
    connect(m_fullscreen, SIGNAL(triggered()), this, SLOT(toggleFullscreen()));

    QAction *about = new QAction("&About", this);
    connect(about, SIGNAL(triggered()), this, SLOT(showAbout()));

    m_pageSource = new QAction("&Show Page Source", this);
    m_pageSource->setShortcut(QString("Ctrl+U"));
    connect(m_pageSource, SIGNAL(triggered()), this, SLOT(showPageSource()));

    QAction *inspectbtn = new QAction("Web &Inspector", this);
    inspectbtn->setShortcut(QString("Shift+Ctrl+I"));
    connect(inspectbtn, SIGNAL(triggered()), this, SLOT(showWebInspector()));

    m_bookmarks = new QAction("&Bookmark Editor", this);
    m_bookmarks->setShortcut(QString("Ctrl+B"));
    connect(m_bookmarks, SIGNAL(triggered()), this, SLOT(showBookmarkEditor()));

    m_addBookmark = new QAction("Add Bookmark", this);
    m_addBookmark->setShortcut(QString("Ctrl+D"));
    m_addBookmark->setIcon(style()->standardIcon(QStyle::SP_DialogSaveButton));
    connect(m_addBookmark, SIGNAL(triggered()), this, SLOT(showAddBookmark()));

    QMenu *PopUpMenu = new QMenu();
    PopUpMenu->addAction(m_newTab);
    PopUpMenu->addAction(m_newWindow);
    PopUpMenu->addAction(m_closeTab);
    PopUpMenu->addAction(m_pageSource);
    PopUpMenu->addAction(inspectbtn);
    PopUpMenu->addSeparator();
    PopUpMenu->addAction(m_addBookmark);
    PopUpMenu->addAction(m_bookmarks);
    PopUpMenu->addAction(m_fullscreen);
    PopUpMenu->addAction(about);
    PopUpMenu->addSeparator();
    PopUpMenu->addAction(m_exit);

    m_menubutton = new QToolButton();
    m_menubutton->setIcon(style()->standardIcon(QStyle::SP_FileDialogDetailedView));
    m_menubutton->setShortcut(Qt::Key_Control + Qt::Key_M);
    m_menubutton->setMenu(PopUpMenu);
    connect(m_menubutton, SIGNAL(clicked()), m_menubutton, SLOT(showMenu()));

    m_addressWidget = new QWidgetAction(this);
    m_addressBar = new QComboBox(this);
    m_addressWidget->setDefaultWidget(m_addressBar);
    m_addressBar->setEditable(true);
    m_addressBar->setSizePolicy(QSizePolicy::Expanding, m_addressBar->sizePolicy().verticalPolicy());
    connect(m_addressBar->lineEdit(), SIGNAL(returnPressed()), SLOT(addressEntered()));
}

void Browser::createToolbar()
{
    m_toolbar = addToolBar("Navigation");
    m_toolbar->setMovable(false);
    m_toolbar->addAction(m_back);
    m_toolbar->addAction(m_forward);
    m_toolbar->addAction(m_reloadStop);
    m_toolbar->addAction(m_home);
    m_toolbar->addAction(m_addressWidget);
    m_toolbar->addAction(m_addBookmark);
    m_toolbar->addWidget(m_menubutton);
}

void Browser::showAddBookmark()
{
    AddBookmarkDialog *add = new AddBookmarkDialog(m_activeView->url().toString(), m_activeView->title(), this);
    add->show();
}

void Browser::showBookmarkEditor()
{
    Bookmarks *bookmarkEd = new Bookmarks(this);
    bookmarkEd->show();
}

void Browser::showWebInspector()
{
    QWebInspector *inspector = new QWebInspector();
    inspector->setPage(m_activeView->page());
    inspector->setWindowIcon(QIcon("./icons/akonadi.png"));
    inspector->setMinimumSize(800, 500);
    inspector->show();
}

void Browser::showPageSource()
{
    QString markup = m_activeView->page()->mainFrame()->toHtml();
    QString title = "view-source:" + m_activeView->url().toString();
    markup = "<html>\n<body>\n<p>" + markup.replace("<", "&lt;").replace(">", "&gt;") + "\n</p>\n</body>\n</html>";

    BrowserWebView *tmp = new BrowserWebView(this);
    int bar = m_tabs->addTab(tmp, title);

    if(m_tabs->count() > 1)
        m_tabs->showTabs();
    if(bar == m_tabs->currentIndex())
        m_activeView = qobject_cast<BrowserWebView*>(m_tabs->widget(bar));

    tmp->setHtml(markup);
    m_tabs->setCurrentIndex(m_tabs->count() - 1);

    m_addressBar->setEditText(title);
    setWindowTitle(title);

    connect(tmp, SIGNAL(urlChanged(QUrl, void*)), this, SLOT(setCurrentUrl(QUrl, void*)));
    connect(tmp, SIGNAL(titleChanged( const QString&, void* )), this, SLOT(setCurrentTitle(QString, void*)));
    connect(tmp, SIGNAL(statusBarMessage( const QString&, void*)), this, SLOT(setStatusBarText(QString, void*)));
    connect(tmp, SIGNAL(linkHovered( const QString&, void*)), this, SLOT(setStatusBarText(QString, void*)));
    connect(tmp, SIGNAL(iconChanged( QIcon, void*)), this, SLOT(setIcon(QIcon, void*)));
    connect(tmp, SIGNAL(loadProgress(int, void*)), this, SLOT(loadProgress(int, void*)));
    connect(tmp->page(), SIGNAL(newViewRequest(bool, QNetworkRequest)), this, SLOT(newView(bool, QNetworkRequest)));
    connect(tmp, SIGNAL(newViewRequest(bool, QNetworkRequest)), this, SLOT(newView(bool, QNetworkRequest)));

}

void Browser::showAbout()
{
    AboutDialog *about = new AboutDialog(this);
    about->show();
}

void Browser::toggleFullscreen()
{
    bool isFullScreen = this->windowState() & Qt::WindowFullScreen;
    if (isFullScreen)
        this->showNormal();
    else
        this->showFullScreen();
}

void Browser::addressEntered()
{
    QUrl tmpString = QUrl::fromUserInput(m_addressBar->currentText());
    m_activeView->load(tmpString);
}

void Browser::goHome()
{
    m_activeView->load(s_home);
}

void Browser::setCurrentUrl(QUrl url,void* address)
{
    QString foo(url.toString());
    if (address == m_tabs->currentWidget()) {
        m_addressBar->setEditText(foo);
    }
    m_tabs->setTabToolTip(addressToIndex(address), foo);
}

void Browser::setCurrentTitle(QString title, void* address)
{
    if(title.isEmpty())
    {
        setWindowTitle(tr("%1 - FlamingSquirrel").arg("(Untitled)"));
        m_tabs->setTabText(addressToIndex(address), "(Untitled)"); //FIXME change this back to title
        return;
    }
    QString tabTitle(title);
    if(tabTitle.length() > TruncateLengthShort)
    {
        tabTitle.truncate(TruncateLengthShort -3 );
        tabTitle.append("...");
    }
    m_tabs->setTabText(addressToIndex(address), tabTitle);

    if (address == m_tabs->currentWidget())
    {
        QString windowTitle(title);
        if(windowTitle.length() > TruncateLengthLong)
        {
            windowTitle.truncate(TruncateLengthLong -3 );
            windowTitle.append("...");
        }
        setWindowTitle(tr("%1 - Web Browser").arg(windowTitle));
        setStatusBarText(title, address);
    }
}

void Browser::setIcon(QIcon icon, void* address)
{
    m_tabs->setTabIcon(addressToIndex(address), icon);
}

void Browser::setStatusBarText(QString text, void* address)
{
    if (address == m_tabs->currentWidget()) {
        statusBar()->showMessage(text);
    }
}

void Browser::clearAddressBar()
{
    m_addressBar->clear();
    m_addressBar->setFocus(Qt::OtherFocusReason);
}

void Browser::tabPageChanged(int index)
{
    Q_UNUSED(index);

    if(m_tabs->count() == 0)
    {   //Special action needed if no tabs are open
        m_progress->setValue(100);
        m_progress->hide();
        statusBar()->showMessage("No tabs open.");
        setWindowTitle("Web Browser");
        m_addressBar->clear();
        return;
    }

    QPointer<BrowserWebView> oldView;
    oldView = m_activeView; // activeView hasn't been updated to the new tab yet.
    if(oldView)
    { //The old tab still exists. Any data to save to the object should be done now.
        oldView->setAddressEditString(m_addressBar->currentText());
    }

    m_activeView = qobject_cast<BrowserWebView*>(m_tabs->currentWidget()); // The activeView is now accurate
    if(!m_activeView)
    {
        qDebug() << "no webview found for some reason";
        abort(); // Add these checks in other places that m_activeView is used?
    }
    m_progress->setValue(m_activeView->getProgress());
    if(m_activeView->getProgress() >= 100)
        m_progress->hide();
    statusBar()->showMessage(m_activeView->getStatusText());
    m_addressBar->setEditText( *(m_activeView->getAddressEditString()) );
    QString viewTitle;
    if(m_activeView->title().isEmpty())
        viewTitle = m_activeView->url().toString();
    else
        viewTitle = m_activeView->title();

    if(viewTitle.length() > TruncateLengthLong)
    {
        viewTitle.truncate(TruncateLengthLong -3 );
        viewTitle.append("...");
    }
    if(!viewTitle.isEmpty())
        setWindowTitle(tr("%1 - Web Browser").arg( viewTitle ));
    else
        setWindowTitle(tr("Web Browser"));
    m_activeView->setFocus(Qt::OtherFocusReason);
}

void Browser::newView(bool newWin, QNetworkRequest request)
{
    if(newWin == true)
    {
        emit newWindow(request);
        return;
    }

    BrowserWebView *tmp = new BrowserWebView(this);
    int bar = m_tabs->addTab(tmp, "(Untitled)");

    if(m_tabs->count() > 1)
        m_tabs->showTabs();
    if(bar == m_tabs->currentIndex())
        m_activeView = qobject_cast<BrowserWebView*>(m_tabs->widget(bar));

    connect(tmp, SIGNAL(urlChanged(QUrl, void*)), this, SLOT(setCurrentUrl(QUrl, void*)));
    connect(tmp, SIGNAL(titleChanged( const QString&, void* )), this, SLOT(setCurrentTitle(QString, void*)));
    connect(tmp, SIGNAL(statusBarMessage( const QString&, void*)), this, SLOT(setStatusBarText(QString, void*)));
    connect(tmp, SIGNAL(linkHovered( const QString&, void*)), this, SLOT(setStatusBarText(QString, void*)));
    connect(tmp, SIGNAL(iconChanged( QIcon, void*)), this, SLOT(setIcon(QIcon, void*)));
    connect(tmp, SIGNAL(loadProgress(int, void*)), this, SLOT(loadProgress(int, void*)));
    connect(tmp->page(), SIGNAL(newViewRequest(bool, QNetworkRequest)), this, SLOT(newView(bool, QNetworkRequest)));
    connect(tmp, SIGNAL(newViewRequest(bool, QNetworkRequest)), this, SLOT(newView(bool, QNetworkRequest)));
    connect(tmp->page(), SIGNAL(showPageSource()), this, SLOT(showPageSource()));
    connect(tmp, SIGNAL(showPageSource()), this, SLOT(showPageSource()));

    tmp->load(request);
}

void Browser::closeTab(signed int index)
{
    if(index == CLOSECURRENT)
        index = m_tabs->currentIndex(); // We wanna close the current tab.

    if(m_tabs->count() <= 2 && !s_allowNoTabs)
    {
        if(m_tabs->count() <= 1)
            newView(false, QNetworkRequest(QUrl("about:blank")));
        m_tabs->hideTabs();
    }
    else if(m_tabs->count() <= 1 && s_allowNoTabs) //We don't need tabs with zero or 1 tabs
        m_tabs->hideTabs();
    BrowserWebView *moo = qobject_cast<BrowserWebView*>(m_tabs->widget(index));
    moo->disconnect();
    moo->stop();
    moo->page()->disconnect();
    moo->deleteLater();
}

void Browser::loadProgress(int progress, void* address)
{
    if (address == m_tabs->currentWidget()) { // Ignore messages from other tabs
        if (progress != 100) {
            m_progress->setValue(progress);
            m_progress->show();
            if(m_reloadStop->text() == "Reload"){
                m_reloadStop->disconnect();
                m_reloadStop->setText("Stop");
                m_reloadStop->setStatusTip("Stops loading the current tab.");
                m_reloadStop->setShortcut(QString("Escape"));
                m_reloadStop->setIcon(style()->standardIcon(QStyle::SP_BrowserStop));
                connect(m_reloadStop, SIGNAL(triggered()), this, SLOT(pageStop()));
            }

        } else{
            if(m_reloadStop->text() == "Stop"){
                m_reloadStop->disconnect();
                m_reloadStop->setText("Reload");
                m_reloadStop->setStatusTip("Reloads the current tab.");
                m_reloadStop->setShortcut(QString("F5"));
                m_reloadStop->setIcon(style()->standardIcon(QStyle::SP_BrowserReload));
                connect(m_reloadStop, SIGNAL(triggered()), this, SLOT(pageReload()));
            }
        m_progress->hide();
    }
    }
}

int Browser::addressToIndex(void* address)
{
    int count = m_tabs->count();
    for(int i = 0; i < count; i++)
    {
        if(address == m_tabs->widget(i))
        {
            return i;
        }
    }
    return count-1;
}

#include "moc_browser.cpp"
