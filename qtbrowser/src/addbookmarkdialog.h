#ifndef ADDBOOKMARKDIALOG_H
#define ADDBOOKMARKDIALOG_H

#include <QDialog>

class QTreeWidgetItem;
class QComboBox;
class QLineEdit;

class AddBookmarkDialog : public QDialog
{
    Q_OBJECT

signals:
    void addBookmark(int index, QTreeWidgetItem *item);

public:
    AddBookmarkDialog(const QString url, const QString title, QWidget *parent = 0);

private:
    QString m_url;
    QString m_title;
    QLineEdit *m_titleEdit;
    QComboBox *m_location;
};

#endif // ADDBOOKMARKDIALOG_H
