include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

set(project_SRCS
    main.cpp
    browserwrapper.cpp
    browser.cpp
    browserwebview.cpp
    browserwebpage.cpp
    browsertab.cpp
    aboutdialog.cpp
    xbel.cpp
    bookmarks.cpp
    addbookmarkdialog.cpp
)

qt4_wrap_ui(project_UIS aboutdialog.ui)

add_executable(flamingsquirrel ${project_SRCS} ${project_UIS})
target_link_libraries(flamingsquirrel Qt4::QtGui Qt4::QtNetwork Qt4::QtWebKit)
