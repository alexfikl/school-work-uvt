#ifndef BROWSERTAB_H
#define BROWSERTAB_H

#include <QTabWidget>
#include <QTabBar>

class BrowserTab : public QTabWidget
{
    Q_OBJECT
public:
    BrowserTab(QWidget* parent);

    ~BrowserTab();

public slots:
    void hideTabs();
    void showTabs();
    void closeTab(int);
    void contextMenuRequested(const QPoint &position);

signals:

private:
    QTabBar *m_tabs;

};
#endif // BROWSERTAB_H
