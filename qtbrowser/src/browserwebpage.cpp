#include "browserwebpage.h"

#include <QWebFrame>
#include <QNetworkReply>

BrowserWebPage::BrowserWebPage(QObject* parent) :
        QWebPage(parent), m_mouseButtons(Qt::NoButton), m_kbdMods(Qt::NoModifier)
{
}


bool BrowserWebPage::acceptNavigationRequest(QWebFrame *frame, const QNetworkRequest &request, NavigationType type)
{
    if (type == QWebPage::NavigationTypeLinkClicked)
    {
        if(m_mouseButtons == Qt::MidButton) {
            emit newViewRequest(false, request);
            return false;
        }
        else if(m_mouseButtons == Qt::LeftButton && m_kbdMods & Qt::ControlModifier) {
            emit newViewRequest(true, request);
            return false;
        }
    }

    return QWebPage::acceptNavigationRequest(frame, request, type);
}

void BrowserWebPage::setKeys(Qt::MouseButtons buttons, Qt::KeyboardModifiers modifiers)
{
    m_mouseButtons = buttons;
    m_kbdMods = modifiers;
}

#include "moc_browserwebpage.cpp"
