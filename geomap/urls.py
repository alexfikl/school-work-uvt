from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
from geomap.geomaps import views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^geomap/', include('geomap.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
    (r'^$', direct_to_template, {'template': 'geomap.html'}),
    (r'^ip/(?P<location>.*)$', views.getGeolocation),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': './media','show_indexes': True})
)
