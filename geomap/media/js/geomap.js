var map;
var marker;
var infowindow;

// onload
$(function () {

    // get IP information from the server and changes marker
    function addToMap(uri){
        $.getJSON('/ip/' + uri, function(location){
            // no matching location was found
            if(location.ip == ''){
                infowindow.close();
                marker.setMap(null);
                $('#ip_text').html('<center><p>A matching location could not be found.</p></center>');
                return false;
            }
            else marker.setMap(map);
            // set ip_text text
            setIPText(uri);

            if(location.isp){
                // utrace
                var content = '<span style="color: #000;"><b>IP Address: </b>' + location.ip + '<br /><b>ISP: </b>' + location.isp + '<br /><b>Organization: </b>' + location.org + '<br /><b>Region: </b>' + location.region + ' (' + location.countrycode + ')</span>';

                content += '<br /><center><span style="font-size:9px;">utrace</span></center>';
            }
            else {
                // geoip
                var content = '<span style="color: #000;"><b>IP Address: </b>';
                if(!checkURI(location.ip))
                    content = '<span style="color: #000;"><b>Domain Name: </b>';

                content += location.ip + '<br /><b>Country: </b>' + location.country_name + '<br /><b>Region: </b>' + location.city + ' (' + location.region + ')</span>';
                content += '<br /><br /><center><span style="font-size:9px;">GeoIP</span></center>';
            }

            var latlng = new google.maps.LatLng(location.latitude, location.longitude);

            // reposition elements
            map.setCenter(latlng);
            infowindow.setContent(content);
            marker.setPosition(latlng);

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });

            infowindow.open(map, marker);
        });
    }

    // checks is a uri is an IP or a domain name
    function checkURI(uri){
        var pattern = /^(\d{0,3}).(\d{0,3}).(\d{0,3}).(\d{0,3})$/;
        return pattern.test(uri);
    }

    // set ip_text
    function setIPText(uri){
        if(checkURI(uri))
            $('#ip_text').html('<center><p>The IP Address "<span style="color: #FFF">'+ uri + '</span>" is located at:</p></center>');
        else
            $('#ip_text').html('<center><p>The Domain Name "<span style="color: #FFF">'+ uri + '</span>" is located at:</p></center>');
    }

    // click event for search button
    $('#srchButton').click(function (){
        var uri = $('#srchField').val();
        addToMap(uri);
    });

    // enter key pressed for search field
    $('#srchField').keypress(function(e){
        if(e.which == 13){
            $('#srchButton').click();
            return false;
        }
    });

    // create map
    if(!map){
        var latlng = new google.maps.LatLng(45.732277,21.278071);

        var myOptions = {
            zoom: 14,
            center: latlng,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        infowindow = new google.maps.InfoWindow();
        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: 'Location'
        });
    }

    // initially add current position
    $.getJSON("http://jsonip.appspot.com?callback=?",function(ipData){
        addToMap(ipData.ip);
    });

});
