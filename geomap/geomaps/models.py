from django.db import models

# Create your models here.

class Resource(models.Model):
    ip = models.IPAddressField(unique=True)
    domainName = models.CharField(max_length=128)
    host = models.CharField(max_length=256)
    isp = models.CharField(max_length=256)
    org = models.CharField(max_length=256)
    region = models.CharField(max_length=128)
    countrycode = models.CharField(max_length=3)
    countryname = models.CharField(max_length=128)
    latitude = models.FloatField()
    longitude = models.FloatField()
    date = models.DateField(auto_now=True)

    def __unicode__(self):
        return u'%s at (%f, %f)' % (self.ip, self.latitude, self.longitude)