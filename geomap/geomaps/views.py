# Create your views here.
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils import simplejson
from geomap.geomaps import utils

def getGeolocation(request, location):
    if request.is_ajax():
        try:
            locationData = utils.getLocationData(location)
            locationData = simplejson.dumps(locationData)
            return HttpResponse(locationData, 'application/javascript')
        except Exception,e:
            print e
    else:
        return HttpResponseBadRequest()