import urllib, re, datetime
from xml.dom.minidom import parseString

from django.contrib.gis.utils import GeoIP
from geomap import settings
from geomap.geomaps.models import Resource
from django.core import serializers

def getIPFromDomain(domain):
    """
        Get data from dnswatch. didnt find an API so just parsing the return page.
        Searching for the specified string.
        Split the string to get IP.
    """
    f = urllib.urlopen('http://www.dnswatch.info/dns/dnslookup?la=en&host=%s&type=A&submit=Resolve' % (domain))
    data = f.read()
    f.close()
    data = re.search('A record found: (.*)', data)

    if data:
        return data.group(0).split(' ')[3]
    else:
        return ''


def getValue(dom, tagName):
    """
        I don't know if this is how it's done, but its the quickest way i found of getting the information from the XML file.
    """
    try:
        node = dom.getElementsByTagName(tagName)[0].toxml()
        node = node.replace('<' + tagName + '>','').replace('</' + tagName + '>','')
    except:
        node = ''

    return node

def convert(data):
    """
        Convert GeoIP data to match utrace and model.
    """
    tmp = {
        'ip': data['ip'],
        'region': data['city'],
        'countrycode': data['contry_code'],
        'countryname': data['country_name'],
        'latitude': data['latitude'],
        'longitude': data['longitude']
    }

    return tmp

def getRemoteData(location, ip=''):
    """
        Gets data from utrace.
        If maximum requests have been exceeded we get data from the local GeoLiteCity database which always works.
    """
    f = urllib.urlopen('http://xml.utrace.de/?query=' + location)
    xml = f.read()
    f.close()

    if xml.find('maximum requests') != -1:
        g = GeoIP(settings.GEOIP_PATH)
        data = g.city(location)

        if data:
            if ip:
                data['ip'] = ip
                data['domainName'] = location
            else:
                data['ip'] = location
        else:
            # empty ip so that javascript knows it didnt find anything
            data = {'ip': ''}

        return convert(data)

    dom = parseString(xml)

    data = {
        'domainName': location,
        'ip': getValue(dom, 'ip'),
        'host' : getValue(dom, 'host'),
        'isp': getValue(dom, 'isp'),
        'org': getValue(dom, 'org'),
        'region': getValue(dom, 'region'),
        'countrycode': getValue(dom, 'countrycode'),
        'latitude': getValue(dom, 'latitude'),
        'longitude': getValue(dom, 'longitude')
    }

    return data

def getLocationData(location):
    """
        Get geolocation. First try local cache. If not available check remote sources and stuff.
    """
    locationIP = ''

    # if it's not an IP address get the the required IP address
    if re.match('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', location) == None:
        locationIP = getIPFromDomain(location)

    try:
        if locationIP:
            inst = Resource.objects.get(ip=locationIP)
        else:
            inst = Resource.objects.get(ip=location)

        print 'using local db'
        # update field if it has been created more than a month ago
        if (inst.date - datetime.date.today()) > datetime.timedelta(30):
            print 'updating'
            newData = getRemoteData(location, locationIP)
            inst.ip = newData.get('ip', '')
            inst.domainName = newData.get('domainName', '')
            inst.host = newData.get('host', '')
            inst.isp = newData.get('isp', '')
            inst.org = newData.get('org', '')
            inst.region = newData.get('region', '')
            inst.countrycode = newData.get('countrycode', '')
            inst.countryname = newData.get('countryname', '')
            inst.latitude = newData.get('latitude', 0.0)
            inst.longitute = newData.get('longitude', 0.0)
            # date is updated automatically
            inst.save()

        data = inst.__dict__

        # need to be deleted to make object serializable
        del data['date']
        del data['_state']
    except Resource.DoesNotExist:
        print 'get data from webapp'
        data = getRemoteData(location, locationIP)
        if data['ip']:
            inst = Resource(**data)
            inst.save()
    except Exception,e:
        print e

    print data
    return data