# Dependencies

Requires Django.

Requires the GeoIP database. The Lite version can be downloaded for free from
[here](http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz).
Place it in the 'media' directory.

