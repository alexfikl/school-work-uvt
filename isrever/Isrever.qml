import QtQuick 2.3
import 'content'
import "content/algorithm.js" as Logic

Rectangle {
    id: game

    property bool running: false
    property bool showMoves: false


    width: boardImage.width; height: boardImage.height + 40

    Image {
        id: boardImage
        source: "content/images/board.png"
    }

    Grid {
        id: board
        width: boardImage.width; height: boardImage.height
        anchors {centerIn: boardImage}
        columns: 8

        Repeater {
            model: 64

            Piece {
                width: board.width/8
                height: board.height/8

                onClicked: Logic.possibleMove(index)
            }
        }
    }

    Message {
        id: messageDisplay
        anchors.centerIn: board
        opacity: 0

        Timer {
            interval: 3000
            running: messageDisplay.opacity
            onTriggered: {
                messageDisplay.opacity = 0;
                Logic.restartGame();
            }
        }
    }

    Message {
        id: nopass
        anchors.centerIn: board
        text: "You have valid moves"
        pixelSize: 40
        opacity: 0

        Timer {
            interval: 1000
            running: nopass.opacity
            onTriggered: {
                nopass.opacity = 0;
            }
        }
    }

    Message {
        id: welcome
        anchors.centerIn: board
        text: "Press 'New Game'!"
    }

    Rectangle {
        id: toolBar
        width: boardImage.width; height: 40
        anchors.bottom: parent.bottom
        color:"#e3e3e3"

        Row {
            spacing: 4
            anchors.verticalCenter: parent.verticalCenter

            Button {
                anchors.verticalCenter: parent.verticalCenter
                text: "New Game"
                onClicked: Logic.restartGame()
            }

            Text {
                 text: "Player Color: "
                 anchors.verticalCenter: parent.verticalCenter
                 font.bold: true
            }

            ToggleButton {
                id: color
                frontText: 'Blue'
                backText: 'Brown'
            }
        }

        Row {
            spacing: 4
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right

            Button {
                text: "Pass"
                onClicked: Logic.pass();
            }

            Button {
                pressed: showMoves == true
                text: "Valid Moves"
                onClicked: {
                    showMoves = (showMoves ? false : true);
                    Logic.highlightValidMoves(showMoves);
                }
            }
        }
    }

}
