import QtQuick 2.3

Item {
    signal clicked

    property bool selected: false
    property bool valid: false

    states: [
        State { name: 'X'; PropertyChanges { target: image; source: "images/black.png" } },
        State { name: 'O'; PropertyChanges { target: image; source: "images/white.png" } }
    ]

    Rectangle {
        id:imagecontainer
        width:parent.width; height: parent.height
        color: valid == true ? Qt.rgba(0,0,0,0.3) : Qt.rgba(0, 0, 0, 0)
        border {
            width: selected ? 3 : 0
            color: 'blue'
        }

        Image {
            id: image
            anchors.centerIn: parent
        }
    }

    MouseArea {
        id: pieceArea
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}
