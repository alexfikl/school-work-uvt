import QtQuick 2.3

Rectangle {
    id: container

    property string text
    property bool pressed: false;


    signal clicked

    width: buttonLabel.width + 20; height: buttonLabel.height + 6
    border { width: 1; color: "#2259BA" }
    radius: 8
    color: "lightgray"
    smooth: true

    gradient: Gradient {
        GradientStop {
            position: 0.0
            color: mouseArea.pressed || pressed ? "darkgray" : "white"
        }
        GradientStop {
            position: 1.0
            color: container.color
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: container.clicked()
    }

    Text {
        id: buttonLabel
        anchors.centerIn: container
        color: '#2259BA'
        text: container.text
        font.pixelSize: 14
    }
}
