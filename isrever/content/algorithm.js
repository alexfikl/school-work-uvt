var directions = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]];
var selectedPiece = -1;
var playerTile;
var computerTile;

// clone method for array objects
Object.prototype.clone = function()
{
    var newObj = (this instanceof Array) ? [] : {};
    for(var i in this)
    {
        if (i == 'clone') continue;

        if (this[i] && typeof this[i] == "object")
        {
          newObj[i] = this[i].clone();
        }
        else
        {
          newObj[i] = this[i];
        }
    }

    return newObj;
}

// get position in array from row and column
function index(row, column)
{
    return (row * 8 + column) % 64;
}

function getBoardMatrix()
{
    var boardMatrix = new Array(8);

    for(var i = 0; i < 8; i++)
    {
        boardMatrix[i] = new Array(8);
        for(var j = 0; j < 8; j++)
            boardMatrix[i][j] = board.children[index(i, j)].state;
    }

    return boardMatrix;
}

function refresh()
{
    if(selectedPiece != -1) board.children[selectedPiece].selected = false;
    for(var i = 0; i < 64; ++i)
        board.children[i].valid = false;
}

function restartGame()
{
    game.running = true;
    welcome.opacity = 0;

    if(color.currentText == 'Blue')
    {
        computerTile = 'O';
        playerTile = 'X';
    }
    else
    {
        computerTile = 'X';
        playerTile = 'O';
    }

    for(var i = 0; i < 64; ++i)
    {
        board.children[i].valid = false;
        board.children[i].selected = false;
        board.children[i].state = '';
    }

    board.children[index(3,3)].state = 'X';
    board.children[index(3,4)].state = 'O';
    board.children[index(4,3)].state = 'O';
    board.children[index(4,4)].state = 'X';
}

function possibleMove(pos)
{
    if (board.children[pos].state == playerTile)
    {
        select(pos);
    }
    else if(board.children[pos].valid == true || isValidMove(getBoardMatrix(), playerTile, Math.floor(pos / 8 ) % 8, pos % 8))
    {
        board.children[selectedPiece].selected = false;
        makeMove(getBoardMatrix(), playerTile, Math.floor(pos / 8) % 8, pos % 8, true);

        refresh();
        if(game.showMoves == true) {
            highlightValidMoves(true);
        }

        gameFinished(); // check winner after player move

        computerTurn();

        gameFinished(); //check winner after computer move
    }
}

function getMessage(tile)
{
    if(playerTile == 'X')
    {
        if(playerTile == tile)
            return 'Computer wins';
        return 'You win!';
    }
    else
    {
        if(playerTile == tile)
            return 'You win!';
        return 'Computer wins';
    }
}

function checkIfGameEnded()
{
    var scores = getScoreForBoard(getBoardMatrix());

    // if board is full
    if((scores.X + scores.O) == 64)
    {
        if(scores.X == scores.O)
            return 'Draw'
        if(scores.X > scores.O)
            return getMessage('O');
        return getMessage('X');
    }

    if(scores.O == 0)
        return getMessage('O');

    if(scores.X == 0)
        return getMessage('X');

    return false;
}

function pass()
{
    if(game.running == false)
        return false;

    // don't allow pass if player has valid moves
    if(getValidMoves(getBoardMatrix(), playerTile).length != 0)
    {
        nopass.opacity = 100;
        return false;
    }

    computerTurn();
    gameFinished();
}

function gameFinished()
{
    var message = checkIfGameEnded();

    if(message == false)
        return;

    messageDisplay.text = message;
    messageDisplay.opacity = 100;
    game.running = false;
}

function select(pos)
{
    // unselect previous
    if(selectedPiece != -1) board.children[selectedPiece].selected = false;

    // select curent
    board.children[pos].selected = true;
    selectedPiece = pos;
}

function highlightValidMoves(showMoves)
{
    if(game.running == false)
        return;

    if(showMoves == false)
    {
        refresh();
    }
    else
    {
       var boardm = getBoardMatrix();
       var moves = getValidMoves(boardm, playerTile);

       for(var i = 0; i < moves.length; ++i)
           board.children[index(moves[i][0], moves[i][1])].valid = true;
    }
}

function computerTurn()
{
    var computerMove = getComputerMove(getBoardMatrix());
    if(computerMove) makeMove(getBoardMatrix(), computerTile, computerMove[0], computerMove[1], true);
}

function isValidMove(tboard, tile, xstart, ystart)
{
    // returns False if the player's move to pos (xstart, ystart) is invalid
    // if it is valid it returns the list of spaces that would become the player's if they made that move
    var tilesToFlip = [];
    var otherTile = (tile == playerTile ? computerTile : playerTile);

    if(!isOnBoard(xstart, ystart) || tboard[xstart][ystart] != '')
        return false;

    tboard[xstart][ystart] = tile;
    for(var i = 0; i < 8; ++i)
    {
        var xDirection = directions[i][0];
        var yDirection = directions[i][1];
        var x = xstart;
        var y = ystart;

        x += xDirection;
        y += yDirection;

        if(isOnBoard(x, y) && tboard[x][y] == otherTile)
        {
            x += xDirection;
            y += yDirection
            if(!isOnBoard(x, y))
                continue;

            while(tboard[x][y] == otherTile)
            {
                x += xDirection;
                y += yDirection;

                if(!isOnBoard(x, y))
                    break;
            }

            if(!isOnBoard(x, y))
                continue;

            if(tboard[x][y] == tile)
                while(1)
                {
                    x -= xDirection;
                    y -= yDirection;
                    if(x == xstart && y == ystart)
                        break;
                    tilesToFlip.push([x, y])
                }
        }
    }

    tboard[xstart][ystart] = '';
    if(tilesToFlip.length == 0)
        return false;

    return tilesToFlip;
}

function isOnBoard(x, y)
{
    return (x >= 0 && x <= 7 && y >= 0 && y <= 7);
}

function getValidMoves(tboard, tile)
{
    var validMoves = [];

    for(var i = 0; i < 8; ++i)
        for(var j = 0; j < 8; ++j)
            if(isValidMove(tboard, tile, i, j) != false)
                validMoves.push([i, j]);

    return validMoves;
}

function getScoreForBoard(tboard)
{
    var oscore = 0;
    var xscore = 0;

    for(var i = 0; i < 8; ++i)
        for(var j = 0; j < 8; ++j)
        {
            if(tboard[i][j] == computerTile)
                ++oscore;
            if(tboard[i][j] == playerTile)
                ++xscore;
        }

    return {'X': xscore, 'O': oscore};
}

function makeMove(tboard, tile, xstart, ystart, realBoard)
{
    // place tile at (xstart, ystart) and flip all necessary opponent pieces
    var tilesToFlip = isValidMove(tboard, tile, xstart, ystart);

    if(tilesToFlip == false)
        return false;

    // make move on matrix for computer player
    if(realBoard == false)
    {
        tboard[xstart][ystart] = tile;
        for(var i = 0; i < tilesToFlip.length; ++i)
            tboard[tilesToFlip[i][0]][tilesToFlip[i][1]] = tile;
    }
    else // make visible move on actual board
    {
        board.children[index(xstart, ystart)].state = tile;
        for(var i = 0; i < tilesToFlip.length; ++i)
            board.children[index(tilesToFlip[i][0], tilesToFlip[i][1])].state = tile;
    }

    return true;
}

function isOnCorner(x, y)
{
    return ((x == 0 && y == 0) || (x == 7 && y == 7) || (x == 0 && y == 7) || (x == 7 && y ==0));
}

function shuffle(array)
{
    var i = array.length;
    if (i == 0)
        return false;

    while (--i) {
        var j = Math.floor(Math.random() * (i + 1));
        var tempi = array[i];
        var tempj = array[j];
        array[i] = tempj;
        array[j] = tempi;
    }
}

function getComputerMove(tboard)
{
    var bestScore = -1, score;
    var bestMove = [];
    var possibleMoves = getValidMoves(tboard, computerTile);

    // if no moves computer passes
    if(possibleMoves.length == 0)
        return false;

    shuffle(possibleMoves);

    for(var i = 0; i < possibleMoves.length; ++i)
        if(isOnCorner(possibleMoves[i][0], possibleMoves[i][1]))
            return possibleMoves[i];

    for(i = 0; i < possibleMoves.length; ++i)
    {
        var tempBoard = tboard.clone();
        makeMove(tempBoard, computerTile, possibleMoves[i][0], possibleMoves[i][1], false);

        score = getScoreForBoard(tempBoard)[computerTile];
        if(score > bestScore)
        {
            bestMove = possibleMoves[i];
            bestScore = score;
        }
    }

    return bestMove;
}