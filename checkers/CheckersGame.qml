import QtQuick 1.1
import "content"
import "content/checkers.js" as Logic

Rectangle {
    id: game

    property bool running: true
    property int difficulty: 10     // chance it will actually think
    property bool showMoves: false

    width: boardImage.width; height: boardImage.height + 40

    Image {
        id: boardImage
        source: "content/images/board.png"
    }

    Grid {
            id: board
            width: boardImage.width - 22; height: boardImage.height - 22
            anchors.centerIn: boardImage
            columns: 8

            Repeater {
                model: 64

                Piece {
                    width: board.width / 8
                    height: board.height / 8

                    onClicked: Logic.possibleMove(index)
                    }
                }
    }

    Message {
        id: messageDisplay
        anchors.centerIn: board
        opacity: 0

        Timer {
            interval: 3000
            running: messageDisplay.opacity
            onTriggered: {
                messageDisplay.opacity = 0;
                Logic.restartGame();
            }
        }
    }

    Message {
        id: welcome
        anchors.centerIn: board
        text: "Press 'New Game'!"
    }

        Rectangle {
                id: toolBar
                width: boardImage.width; height: 40
                anchors.bottom: parent.bottom
                color:"#e3e3e3"

                Row {
                    spacing: 4
                    anchors.verticalCenter: parent.verticalCenter

                    Button {
                        anchors.verticalCenter: parent.verticalCenter
                        text: "New Game"
                        onClicked: Logic.restartGame()
                    }

                    Text {
                        anchors.verticalCenter: parent.verticalCenter
                        text: 'Player Color:'
                        font.bold: true
                    }

                    ToggleButton {
                        id: color
                        frontText: 'Blue'
                        backText: 'Brown'
                    }
                }

                Row {
                    spacing: 4
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right

                    Button {
                        text: "Show Moves"
                        pressed: game.showMoves
                        onClicked: game.showMoves = game.showMoves == true ? false : true
                    }
                }
            }

}
