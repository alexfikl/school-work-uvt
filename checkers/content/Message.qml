import QtQuick 1.1

Rectangle{
    id: container
    property string text;
    property int pixelSize: 50;

    width: label.width + 40; height: label.height + 20
    border { width: 1; color: "#2259BA" }
    radius: 8
    color: Qt.rgba(0, 0, 0, 0.3)
    smooth: true

    Text {
        id: label
        text: container.text
        anchors.centerIn: container
        color: '#fdfdfd'
        style: Text.Raised; styleColor: "#969EAB"
        font.pixelSize: container.pixelSize; font.bold: true
    }

    Behavior on opacity {
        NumberAnimation { properties: "opacity"; duration: 200 }
    }
}
