import QtQuick 1.1

Item {
    id: container

    signal clicked

    property bool selected: false
    property bool available: false


    states: [
        State { name: "x"; PropertyChanges { target: image; source: "images/black.png" } },
        State { name: "o"; PropertyChanges { target: image; source: "images/white.png" } },
        State { name: "X"; PropertyChanges { target: image; source: "images/black_king.png" } },
        State { name: "O"; PropertyChanges { target: image; source: "images/white_king.png" } }
    ]

    Rectangle {
        id:imagecontainer
        width:parent.width; height: parent.height
        color: container.available == true ? Qt.rgba(0, 0, 0, 0.3) : Qt.rgba(0, 0, 0, 0)
        border {
            width: container.selected == true ? 3 : 0
            color: 'blue'
        }

        Image {
            id: image
            anchors.centerIn: parent
        }
    }

    MouseArea {
        id: pieceArea
        anchors.fill: parent
        onClicked: container.clicked()
    }
}
