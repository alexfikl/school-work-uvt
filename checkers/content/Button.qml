import QtQuick 1.1

Rectangle {
    id: container

    property string text
    property bool pressed: false;


    signal clicked

    width: buttonLabel.width + 20; height: buttonLabel.height + 6
    border { width: 1; color: "#2259BA" }
    radius: 8
    color: "lightgray"
    smooth: true

    gradient: Gradient {
        GradientStop {
            position: 0.0
            color: mouseArea.pressed == true || container.pressed == true ? "darkgray" : "white"
        }
        GradientStop {
            position: 1.0
            color: container.color
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: container.clicked()
    }

    Text {
        id: buttonLabel
        anchors.centerIn: container
        color: container.pressed == true ? '#2259BA' : '#101761'
        text: container.text
        font.pixelSize: 14
    }
}
