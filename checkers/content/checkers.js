/****************************************************
 *                  Variables                       *
 ****************************************************/

var scoreboard = [[0, 4, 0, 4, 0, 4, 0, 4],
                  [4, 0, 3, 0, 3, 0, 3, 0],
                  [0, 3, 0, 2, 0, 2, 0, 4],
                  [4, 0, 2, 0, 1, 0, 3, 0],
                  [0, 3, 0, 1, 0, 2, 0, 4],
                  [4, 0, 2, 0, 2, 0, 3, 0],
                  [0, 3, 0, 3, 0, 3, 0, 4],
                  [4, 0, 4, 0, 4, 0, 4, 0]];

var selectedPiece = -1;
var computerPiece;
var playerPiece;

/****************************************************
 *              Helper Functions                    *
 ****************************************************/

// add clone method to objects
Object.prototype.clone = function()
{
    var newObj = (this instanceof Array) ? [] : {};
    for (var i in this)
    {
        if (i == 'clone') continue;

        if (this[i] && typeof this[i] == "object")
        {
          newObj[i] = this[i].clone();
        }
        else
        {
          newObj[i] = this[i];
        }
    }

    return newObj;
}

// get position in array from row and column
function index(row, column)
{
    return (row * 8 + column) % 64;
}

// get row from position in array
function row(pos)
{
    return Math.floor(pos / 8) % 8;
}

// get column from position in array
function col(pos)
{
    return pos % 8;
}

function onBoard(x, y)
{
    return x >= 0 && x < 8 && y >= 0 && y < 8;
}

function isEmpty(state, x, y)
{
    return onBoard(x, y) && state == '';
}

function getBoardMatrix()
{
    var boardMatrix = new Array(8);

    for(var i = 0; i < 8; i++)
    {
        boardMatrix[i] = new Array(8);
        for(var j = 0; j < 8; j++)
            boardMatrix[i][j] = board.children[index(i, j)].state;
    }

    return boardMatrix;
}

// refresh board from possible moves
function refresh()
{
    if(selectedPiece != -1) board.children[selectedPiece].selected = false;
    for(var i = 0; i < 64; ++i)
        board.children[i].available = false;
}

// select a piece highlighting all its possible moves
function select(position)
{
    refresh();
    board.children[position].selected = true;

    selectedPiece = position;
    if(game.showMoves == true) highlightPossibleMoves(position);
}

// highlight list of positions
function highlightPossibleMoves(position)
{
    var moves = getMovesFor(getBoardMatrix(), position);

    for(var i = 0; i < moves.length; ++i)
        board.children[moves[i]].available = true;
}

// if game is finished display message
function gameFinished()
{
    var board = getBoardMatrix();
    var message = '';

    if(winner(getBoardMatrix(), playerPiece) == true)
        message = 'You win!';
    else if(winner(getBoardMatrix(), computerPiece) == true)
        message = 'Computer Wins';
    else
        return false;

    messageDisplay.text = message;
    messageDisplay.opacity = 1
    game.running = false
}

/****************************************************
 *              Game Helper Functions               *
 ****************************************************/

// checks if a player won. looking if any of his adversary's pieces are still on the board
function winner(tboard, player)
{
    console.log(playerPiece, computerPiece, player);
    var adversary = (player.toUpperCase() == 'X' ? 'O' : 'X')

    for(var i = 0; i < 8; ++i)
        for(var j = 0; j < 8; ++j)
            if(tboard[i][j].toUpperCase() == adversary)
                return false;

    return true;
}

function makeMove(tboard, from, to, realBoard)
{
    console.log(from, to);
    // make move on real board
    if(realBoard == true)
    {
        // jump?
        if(Math.abs(to - from) > 9)
        {
            var jumped = (to + from) / 2;
            board.children[jumped].state = ''; // remove jumped piece
            board.children[to].state = board.children[from].state; // move own piece
            board.children[from].state = ''; // remove previous position
            return true;
        }

        board.children[to].state = board.children[from].state;
        board.children[from].state = '';
    }
    else
    {
        var x1 = row(from), y1 = col(from);
        var x2 = row(to), y2 = col(to);

        if(Math.abs(x1 - x2) == 2)
        {
            var xJ = (x1 + x2) / 2, yJ = (y1 + y2) / 2;
            tboard[xJ][yJ] = '';
            tboard[x2][y2] = tboard[x1][y1];
            tboard[x1][y1] = '';
        }
        else
        {
            tboard[x2][y2] = tboard[x1][y1];
            tboard[x1][y1] = '';
        }

        return tboard;
    }

    return false;
}

function jumpPossible(tboard, x1, y1, x2, y2)
{
    var adversary = (computerPiece == 'x' ? 'O' : 'X');
    var xTo = (2 * x2 - x1), yTo = (2 * y2 - y1);

    // if position empty && position on board && jumping over adversary
    return isEmpty(tboard[xTo][yTo], xTo, yTo) && onBoard(x2, y2) && (tboard[x2][y2].toUpperCase() == adversary);
}

/* return jump moves for computer player
 * @param int x original row
 * @param int y original column
 * @param bool king order of piece to be moved (king or not)
 */
function getJumpMoves(tboard, x, y, king)
{
    var jumps = [];
    var pos = index(x, y);

    if(jumpPossible(tboard, x, y, x + 1, y + 1))
        jumps.push([pos, index(x + 2, y + 2)]);
    if(jumpPossible(tboard, x, y, x + 1, y - 1))
        jumps.push([pos, index(x + 2, y -2)]);

    if(king == true)
    {
        if(jumpPossible(tboard, x, y, x - 1, y + 1))
            jumps.push([pos, index(x - 2, y + 2)]);
        if(jumpPossible(tboard, x, y, x - 1, y - 1))
            jumps.push([pos, index(x - 2, y - 2)]);
    }

    return jumps;
}

// returns an array with all the possible moves by 'player' on a given board
// used for computer player
function getAllPossibleMoves(tboard)
{
    var allMoves = [];

    for(var i = 0; i < 8; ++i)
        for(var j = 0; j < 8; ++j)
            if(tboard[i][j].toUpperCase() == computerPiece.toUpperCase())
            {
                // may return multiple jumps. need to store as multiple moves.
                var jumps = getJumpMoves(tboard, i, j, tboard[i][j] == computerPiece.toUpperCase());
                for(var k = 0; k < jumps.length; ++k)
                    allMoves.push(jumps[k]);
            }

    // ignore other moves if there are jump moves. because i'm a mean AI
    if(allMoves.length == 0)
    {
        for(i = 0; i < 8; ++i)
            for(j = 0; j < 8; ++j)
            {
                var position = index(i, j);
                try {
                    if(isEmpty(tboard[i + 1][j - 1], i + 1, j - 1))
                        allMoves.push([position, index(i + 1, j - 1)]);
                    if(isEmpty(tboard[i + 1][j - 1], i + 1, j + 1))
                        allMoves.push([position, index(i + 1, j + 1)]);
                }
                catch(err) {
                    console.log(err);
                }
                // if king
                if(tboard[i][j] == computerPiece.toUpperCase())
                {
                    if(isEmpty(tboard[i - 1][j - 1], i - 1, j - 1))
                        allMoves.push([position, index(i - 1, j - 1)]);
                    if(isEmpty(tboard[i - 1][j + 1], i - 1, j + 1))
                        allMoves.push([position, index(i - 1, j + 1)]);
                }
            }
    }

    console.log(allMoves);
    return allMoves;
}

// returns all possible moves for a particular piece
// used only for human player
function getMovesFor(tboard, position)
{
    var moves = [];
    var x = row(position), y = col(position);

    if(jumpPossible(tboard, x, y, x - 1, y + 1))
        moves.push(index(x - 2, y + 2));
    if(jumpPossible(tboard, x, y, x - 1, y - 1))
        moves.push(index(x - 2, y - 2));

    if(isEmpty(tboard[x - 1][y - 1], x - 1, y - 1))
        moves.push(index(x - 1, y - 1));
    if(isEmpty(tboard[x - 1][y + 1], x - 1, y + 1))
        moves.push(index(x - 1, y + 1));

    if(tboard[x][y] != playerPiece)
    {
        if(jumpPossible(tboard, x, y, x + 1, y + 1))
            moves.push(index(x + 2, y + 2));
        if(jumpPossible(tboard, x, y, x + 1, y - 1))
            moves.push(index(x + 2, y - 2));

        if(isEmpty(tboard[x + 1][y - 1], x + 1, y - 1))
            moves.push(index(x + 1, y - 1));
        if(isEmpty(tboard[x + 1][y + 1], x + 1, y + 1))
            moves.push(index(x + 1, y + 1));
    }

    console.log(moves);
    return moves;
}

// computer turn: call minimax function and make the resulting move
function computerTurn()
{
    var computerMove = getComputerMove()
    makeMove(computerMove[0], computerMove[1], true);
}

// calculate the value of the board for a specific player considering the scoreboard
// return (ownScore - AdversaryScore)
function getBoardScore(tboard, player){
    var score = 0;
    var adversary = (player == 'x' ? 'o' : 'x');

    for(var i = 0; i < 8; ++i)
    {
        for(var j = 0; j < 8; ++j)
        {
            if(tboard[i][j] == player)
                score += scoreboard[i];
            if (tboard[i][j] == player.toUpperCase())
                // if piece is king double the score
                score += 2 * scoreboard[i];

            if(tboard[i][j] == adversary)
                score -= scoreboard[i];
            if (tboard[i][j] == adversary.toUpperCase())
                // if piece is king double the score
                score -= 2 * scoreboard[i];
        }
    }

    return score;
}

/****************************************************
 *              QML Callable Functions              *
 ****************************************************/

// restart game
function restartGame()
{
    game.running = true;
    welcome.opacity = 0;

    if(color.currentText == 'Blue')
    {
        playerPiece = 'x';
        computerPiece = 'o';
    }
    else
    {
        playerPiece = 'o';
        computerPiece = 'x';
    }

    for (var i = 0; i < 64; ++i)
    {
        var pos = row(i) + col(i);
        board.children[i].selected = false;
        board.children[i].available = false;

        if( row(i) < 3 && pos % 2 == 1) // upper part of the board
        {
            board.children[i].state = computerPiece;
        }
        else if(row(i) > 4 && pos % 2 == 1) // lower part
        {
            board.children[i].state = playerPiece;
        }
        else
        {
            board.children[i].state = ''; //empty spaces
        }
    }
}

// if clicked on own piece then select all possible moves
// else refresh, move piece to selected position and give control to the computer
function possibleMove(position)
{
    if(board.children[position].state.toUpperCase() == playerPiece.toUpperCase())
    {
        select(position);
    }
    else if(board.children[position].available == true)
    {
        refresh();
        makeMove([], selectedPiece, position, true);

        gameFinished();

        computerTurn();

        gameFinished();
    }
}

/****************************************************
 *                   MINIMAX                        *
 ****************************************************/

//start minimax algorithm
function getComputerMove(depth)
{
    var tboard = getBoardMatrix();
    var bestScore = -1000;
    var bestMove = [];
    var possibleMoves = getAllPossibleMoves(tboard, computerPiece);

    for(var i = 0; i < possibleMoves.length; ++i)
    {
        var score = minimax(tboard, depth, -Infinity, Infinity, computerPiece);
        if(score > bestScore)
        {
            bestScore = score;
            bestMove = possibleMoves[i];
        }
    }

    return bestMove;
}

function minimax(tboard, depth, alpha, beta, player)
{
    if(winner(tboard, player) || depth == 0)
    {
        return getBoardScore(tboard, player);
    }

    var score;
    var possibleMoves = getAllPossibleMoves(tboard, player);

    if(player == computerPiece)
        score = -1000;
    else
        score = 1000

    for(var i = 0; i < possibleMoves.length; ++i)
    {
        var tempBoard = tboard.clone();
        makeMove(tempBoard, possibleMoves[i], false);

        if(player == computerPiece)
        {
            score = Math.max(score, minimax(tempBoard, depth - 1, alpha, beta, playerPiece));
            alpha = score

            if(beta > alpha)
                return score;
        }
        else
        {
            score = Math.min(score, minimax(tempBoard, depth - 1, alpha, beta, computerPiece));
            beta = score

            if(alpha > beta)
                return score;
        }
    }

    return score;
}