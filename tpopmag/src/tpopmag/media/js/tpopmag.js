$(function () {

    $(':input').change(function() {
        console.log('selected file');
        $('#file-upload').submit();
    });

    var t = setTimeout('$("#uplmsg").remove()', 2000)

    $('#us').click(function(){
        $.post('/i18n/setlang/', {language: 'en-us', next: '/'}, function(data){
            location.reload();
        });

        return 0;
    });

    $('#ro').click(function(){
        $.post('/i18n/setlang/', {language: 'ro', next: '/'}, function(data){
            location.reload();
        });

        return 0;
    });

});