from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template
import opmanagement.views as views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^tpopmag/', include('tpopmag.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
     (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     (r'^admin/', include(admin.site.urls)),
     #(r'^admin_tools/', include('admin_tools.urls')),

    # templates
     (r'^upload/$', views.upload_file),
     (r'^$', direct_to_template, {'template': 'base.html'}),
     (r'^students/$', views.students),
     (r'^optionals/$', direct_to_template, {'template': 'optionals.html'}),
     (r'^i18n/', include('django.conf.urls.i18n')),
     (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': './media','show_indexes': True})
)
