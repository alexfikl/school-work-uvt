from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect

def require_login(func):
    '''A decorator that checks for authentication.
    '''
    def decorated(*args,**kwargs):
        request = args[0]
        if 'username' in request.session:
            return func(*args,**kwargs)
        else:
            return HttpResponseRedirect('/')
    return decorated