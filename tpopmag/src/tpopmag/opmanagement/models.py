from django.db import models
from django.contrib import admin

# Create your models here.

class University(models.Model):
    sUniversityName = models.CharField(max_length=256,unique=True)
    sUniversityLocation = models.CharField(max_length=256)

    def __str__(self):
        return str(self.sUniversityName) + ', ' + str(self.sUniversityLocation)

    #def __unicode__(self):
    #    return self.sUniversityName + ', ' + self.sUniversityLocation

    def clean(self):
        from django.core.exceptions import ValidationError
        if len(self.sUniversityName) < 3:
            raise ValidationError('The University name is too short to be considered valid.')
        if len(self.sUniversityLocation) < 10:
            raise ValidationError('The University location is too short to be considered valid.')

class Faculty(models.Model):
    sFacultyName = models.CharField(max_length=256)
    sFacultyLocation = models.CharField(max_length=256)
    sUniversity = models.ForeignKey(University)

    def __str__(self):
        return str(self.sFacultyName) + ', ' + str(self.sFacultyLocation)

    #def __unicode__(self):
    #    return self.sFacultyName + ' ,' + self.sFacultyLocation

    def clean(self):
        from django.core.exceptions import ValidationError
        if len(self.sFacultyName) < 4:
            raise ValidationError('The Faculty name is too short to be considered valid.')
        if len(self.sFacultyLocation) < 10:
            raise ValidationError('The Faculty location is too short to be considered valid.')


class FacultyAdmin(admin.ModelAdmin):
    pass

class Secretary(models.Model):
    sSecretaryName = models.CharField(max_length=256)
    sSecretaryOffice = models.CharField(max_length=256)

    def __str__(self):
        return str(self.sSecretaryName) + ', cam. ' + str(self.sSecretaryOffice)
    
    #def __unicode__(self):
    #    return self.sSecretaryName + ', cam. ' + self.sSecretaryOffice

    def clean(self):
        from django.core.exceptions import ValidationError

        if len(self.sSecretaryName.split(" ")) < 2:
            raise ValidationError('Secretary has to have at least a first name and a last name.')
        if len(self.sSecretaryName) < 4:
            raise ValidationError('Secretary name too short to be considered valid.')

        if len(self.sSecretaryOffice) < 3:
            raise ValidationError('The Secretary office location is too short to be considered valid.')


class Section(models.Model):
    sSectionName = models.CharField(max_length=256)
    sFaculty = models.ForeignKey(Faculty)
    sSectionSecretary = models.ForeignKey(Secretary)

    def __str__(self):
        return str(self.sSectionName) + ', ' + str(self.sFaculty)

    #def __unicode__(self):
    #    return self.sSectionName + ', ' + self.sFaculty

    def clean(self):
        from django.core.exceptions import ValidationError
        if len(self.sSectionName) < 4:
            raise ValidationError('The Section name is too short to be considered valid.')


class Students(models.Model):
    sStudentCNP = models.CharField(max_length=14)
    sStudentName = models.CharField(max_length=256, primary_key=True)
    nStudentID = models.IntegerField()
    sStudentEmail = models.CharField(max_length=256)
    sSection = models.ForeignKey(Section)

    class Meta:
        ordering = ['nStudentID']

    def __str__(self):
        return str(self.nStudentID) + ', ' + str(self.sStudentName) + ', ' + '%s' % str(self.sStudentCNP).format('d').zfill(13)

    #def __unicode__(self):
    #    return self.sStudentName + ', ' + self.nStudentID + ', ' + self.sStudentCNP

    def clean(self):
        from django.core.exceptions import ValidationError
        import re

        if len(self.sStudentName.split(" ")) < 2:
            raise ValidationError('Student has to have at least a first name and a last name.')
        if len(self.sStudentName) < 4:
            raise ValidationError('Student name too short to be considered valid.')

        result = re.match("[12]\d{2}(1[0-9]|0[1-9])(0[1-9]|[12][0-9]|3[01])\d{6}", self.sStudentCNP)
        if result == None:
            raise ValidationError('Student CNP is not valid.')

        result = re.match('[a-zA-Z0-9+_\-\.]+@[0-9a-zA-Z][.-0-9a-zA-Z]*.[a-zA-Z]+', self.sStudentEmail)
        if result == None:
            raise ValidationError('Student email is not valid.')


class Optionals(models.Model):
    sOptionalName = models.CharField(max_length=256)
    sOptionalAssignedTeacher = models.ForeignKey('opmanagement.Teachers')

    def __str__(self):
        return str(self.sOptionalName) + ', ' + str(models.ForeignKey('opmanagement.Teachers'))

    #def __unicode__(self):
    #    return self.sOptionalName

    def clean(self):
        from django.core.exceptions import ValidationError
        if len(self.sOptionalName) < 4:
            raise ValidationError('The Optional name is too short to be considered valid.')


class Teachers(models.Model):
    sTeacherName = models.CharField(max_length=256)
    sTeacherOffice = models.CharField(max_length=256)

    def __str__(self):
        return str(self.sTeacherName) + ', ' + str(self.sTeacherOffice)

    #def __unicode__(self):
    #    return self.sTeacherName + ', ' + self.sTeacherOffice

    def clean(self):
        from django.core.exceptions import ValidationError

        if len(self.sTeacherName.split(" ")) < 2:
            raise ValidationError('Teacher has to have at least a first name and a last name.')
        if len(self.sTeacherName) < 4:
            raise ValidationError('Teacher name too short to be considered valid.')
        if len(self.sTeacherOffice) < 10:
            raise ValidationError('The Teacher office location is too short to be considered valid.')


admin.site.register(Teachers)
admin.site.register(Optionals)
admin.site.register(Students)
admin.site.register(Section)
admin.site.register(Secretary)
admin.site.register(Faculty)
admin.site.register(University)
