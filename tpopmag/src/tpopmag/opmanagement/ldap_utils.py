import ldap

_default_search_dn = None
_default_bind_dn = None
_default_uri = None

class InvalidCredentials(Exception):
    pass


def set_default_uri(dn):
    global _default_uri
    _default_uri = dn

def set_default_search_dn(dn):
    global _default_search_dn
    _default_search_dn = dn

def set_default_bind_dn(dn):
    global _default_bind_dn
    _default_bind_dn = dn

def open_connection(uri = None, dn = None, passwd = None):
    if not uri: uri = _default_uri
    if not dn: dn = _default_bind_dn
    conn = ldap.initialize(uri)
    try:
        if dn or passwd:
            conn.simple_bind_s(dn, passwd)
        else:
            conn.simple_bind_s()
        return conn
    except ldap.INVALID_CREDENTIALS:
        raise InvalidCredentials()

def filter(filter, dn = None, connection = None):
    if not dn: dn = _default_search_dn
    unbind_on_finish = False
    if connection is None:
        connection = open_connection()
        unbind_on_finish = True
    try:
        ldap_result_id = connection.search(dn,
                                    ldap.SCOPE_SUBTREE,
                                    filter)
        result_type, result_data = connection.result(ldap_result_id, 0)
        return result_data
    finally:
        if unbind_on_finish:
            connection.unbind()