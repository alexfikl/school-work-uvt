import ldap_utils as lu
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from tpopmag.opmanagement.models import *
import os
import time
import commands
from datetime import datetime
from tpopmag.settings import MEDIA_ROOT
#FIXME: Just for testing the following
from tpopmag.opmanagement import xls_tools
from tpopmag.opmanagement.xls_tools import import_from_xls

_filter = 'uid=%s'
dn = 'uid=%s,ou=Users,dc=info,dc=uvt,dc=ro'


#Set up ldap_utils
#FIXME: Set URI !
lu.set_default_uri('ldap://URI')
lu.set_default_search_dn('ou=Users,dc=info,dc=uvt,dc=ro')
#lu.set_default_bind_dn('uid=root,dc=info,dc=uvt,dc=ro')


class InvalidCredentials(Exception):
    'Invalid Credentials.'
class MalformedUsername(Exception):
    'Malformed username.'


def get_url(url):
    '''function to get the url of a view'''
    #TODO: get prefix from config file
    prefix = 'tpopmag.opmanagement.views.'
    return reverse(prefix+url)


def check_credentials(username, password):
    ''' Validate the username '''

    # check to see if user is in the db
    try:
        event = user.objects.get(
                                 username = username,
                                )
    except ObjectDoesNotExist:
        return False

    #check to see if user is in ldap
    try:
        conn = lu.open_connection(
                                  dn = dn % username,
                                  passwd = password
                                 )
        conn.unbind()
        return True
    except lu.InvalidCredentials:
        return False


def handle_uploaded_file(f):
    ''' Handles file uploading and removes it if not Excel mimetype '''
    
    file_name = MEDIA_ROOT + 'uploaded/' + f.name[:-4] + time.strftime("-%H:%M:%S") + '.xls'
    try:
        destination = open(file_name, 'wb+')
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()
        do_stuff_to_it(file_name)
        return 1
    except:
        return 0

def do_stuff_to_it(file_name):
    if 'CDF V2 Document' not in commands.getoutput('file ' + file_name):
        try:
            os.unlink(file_name)
            return 0
        except OSError:
            #FIXME: Probably should do something about it
            pass
        

    else:
        #FIXME: Ugly hook, modify template
        import_from_xls(file_name)