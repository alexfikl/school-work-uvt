from tpopmag.opmanagement.models import *
import xlutils
from xlrd import open_workbook

#FIXME: File format is expected to be static. Like at our University.

def import_from_xls(file_name):
    ''' Creates django models and saves them to the backend DB. '''
    print "\n\n [+] import_from_xls\n\n"

    book = open_workbook(filename = file_name, pickleable = True, use_mmap = True, formatting_info = False, on_demand = True)
    for sheet_index in range(book.nsheets):
        sheet = book.sheet_by_index(sheet_index)
        for row_index in range(1,sheet.nrows):
            student_data = [cell.value for cell in sheet.row_slice(row_index)]
            #FIXME: some checking needs to be done to make sure the data is valid. probably in the models, not here

            #FIXME: Section also a Model, needs instantiation
            secretary = Secretary()
            secretary.sSecretaryName = student_data[8]
            secretary.sSecretaryOffice = student_data[9]
            secretary.save()

            university = University()
            university.sUniversityLocation = student_data[4] 
            university.sUniversityName = student_data[3]
            university.save()
            
            faculty = Faculty()
            faculty.sFacultyName = student_data[5]
            faculty.sFacultyLocation = student_data[4]
            faculty.sUniversity = university 
            faculty.save()
            
            try:
                section = Section.objects.get(sSectionName=str(student_data[6]),sFaculty=faculty,sSectionSecretary=secretary)
            except:
                section = Section()
                section.sFaculty = faculty
                section.sSectionSecretary = secretary
                section.sSectionName = student_data[6]
                section.save()
            
            s = Students()
            s.sSection = section
            s.sStudentCNP = student_data[1]
            s.sStudentEmail= student_data[10]
            s.sStudentName = student_data[0]
            s.nStudentID = student_data[2]
            #Performancewise would be ok to do a transaction, but the load is pretty small...
            s.save()
            
            del secretary
            del section
            del university
            del faculty
            del s


# end of import_from_xls
