from django import forms
from tpopmag.opmanagement.models import *

class baseUser(forms.Form):
    username = forms.CharField(max_length = 25, label = 'Username')
    
class basePass(forms.Form):
    password = forms.CharField(max_length = 25, label = 'Password', widget=forms.PasswordInput)
    
class loginForm(baseUser, basePass):
    pass