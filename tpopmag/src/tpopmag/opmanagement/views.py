from django.http import HttpResponse, HttpResponseRedirect
from django import forms
from django.utils import simplejson
from django.template import Context, loader
from tpopmag.opmanagement.util import require_login
from django.shortcuts import render_to_response
from tpopmag.settings import MEDIA_ROOT
from tpopmag.opmanagement.backend import *
#from tpopmag.opmanagement import lforms
import time

success = ''

class UploadFileForm(forms.Form):
    file  = forms.FileField()

def upload_file(request):
    global success
    if request.method == "POST":
        
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            success = handle_uploaded_file(request.FILES['file'])
            if success:
                success = '%s was succesfully uploaded and imported.' % (request.FILES['file'].name)
            else:
                success = 'There was an error uploading the file.'
    return HttpResponseRedirect('/students/')

def students(request):
    form = UploadFileForm()
    student_list = Students.objects.order_by('nStudentID').all()
    return render_to_response('students.html', {'form': form, 'success': success, 'student_list': student_list})



# Login / session stuff

#def login(request):
#    '''Log in user. Saves 'username' and 'password' in session dictionary.'''
#    if request.method == 'POST':
#        form = loginForm(request.POST)
#    else:
#        form = loginForm()
#    errors = []
#
#    if form.is_valid():
#        username = form.cleaned_data['username']
#        password = form.cleaned_data['password']
#
#        # check if the user exists in database and ldap
#        if backend.check_credentials(username, password):
#            request.session['username'] = username
#            request.session['password'] = password
#            return HttpResponseRedirect(get_url('index'))
#        else:
#            errors.append('username or password invalid')
#
#    template = loader.get_template('base.html')
#    context = Context({
#                       #something:something
#                       })
#    return HttpResponse(template.render(context))

#def logout(request):
#    '''Log out. Removes all values from session dictionary.'''
#    for key in request.session.keys():
#        del request.session[key]
#    return HttpResponseRedirect(get_url('login'))


#@require_login
#def profile(request):
#    '''User information page'''
#    template = loader.get_template('base.html')
#    context = Context({'title':'Account profile.'})
#    return HttpResponse(template.render(context))

