��          |      �          >   !     `  '   e     �  ,   �  	   �  @   �          %     B  ~   K  k  �  K   6     �  (   �     �  B   �  	   �  E        M  #   i     �  �   �                 	                 
                         Administration for students, sections, years, optional courses Home Import students from Excel spreadsheets Made by Model faculty structure (section, year, etc) Optionals Possibility to redistribute students based on different criteria Project can be found on Student Optionals Management Students The project consists of offering a management system for student options for optional curses. The functional requirements are: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-01-04 21:13+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Optiuni de administrare pentru studenti, sectiuni, ani si cursuri optionale Acasa Importarea studentilor din fisiere Excel Facut de Baza de date modelata dupa structura facultatii (sectie, an, etc.) Optionale Posibilitatea de a redistribui studentii pe baza diferitelor criterii Proiectul poate fi gasit pe Sistem de gestionare a Optionalelor Studenti Proiectul ofera un sistem de gestionare a cursurilor optionale disponibile pentru studenti. Cerintele functionale ale proiectului sunt: 