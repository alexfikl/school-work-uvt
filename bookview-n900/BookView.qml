/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.1
import 'content/server.js' as Server
import 'content'

// This example illustrates expanding a list item to show a more detailed view.

Rectangle {
    id: bookcategory
    width: 800; height: 480
    color: "#343434"

    Image { source: "content/images/stripes.png"; fillMode: Image.Tile; anchors.fill: parent; opacity: 0.3 }

    Rectangle {
        id: toolbar
        z: 1
        width: parent.width; height: 40


        BorderImage {
            source: "content/images/titlebar.png"
            width: parent.width
            height: parent.height + 17
            y: -9
            border.left: 10
            border.right: 10
        }

        Row {
            anchors.verticalCenter: parent.verticalCenter
            spacing: 15

            Text {
                anchors {verticalCenter: parent.verticalCenter; leftMargin: 5}
                text: "  Search:"
                color: 'white'
                font.pixelSize: 14
                font.bold: true
            }

            SearchLine {
                id: editor
                width: 200
                height: 27
            }
        }

        Button {
            width: 100
            height: 31
            anchors { right: parent.right; verticalCenter: parent.verticalCenter}
            onClicked: { list1.reload(); list2.reload(); list3.reload(); }
        }
    }

    VisualItemModel {
        id: bookcategorymodel

        BookList {
            id: list1
            model: 'fiction'
            anchors.fill: bookcategory
        }

        BookList {
            id: list2
            model: 'religion'
            anchors.fill: bookcategory
        }

        BookList {
            id: list3
            model: 'science'
            anchors.fill: bookcategory
        }
    }

    ListView {
            id: bookview
            width: parent.width
            y: parent.y + 40
            model: bookcategorymodel
            orientation: ListView.Horizontal
            snapMode: ListView.SnapOneItem; flickDeceleration: 1000
        }

    Rectangle {
        width: parent.width; height: 40
        anchors { bottom: parent.bottom }

        BorderImage {
            source: "content/images/titlebar.png"
            width: parent.width
            height: parent.height + 17
            y: -9
            border.left: 10
            border.right: 10
        }

        Row {
            anchors.centerIn: parent
            spacing: 22

            Rectangle{
                width: 200
                height: 15
                color: Qt.rgba(0,0,0,0)

                Text {
                    anchors.centerIn: parent
                    text: "Fiction"
                    font.bold: true
                    font.pixelSize: 14
                    color: bookview.currentIndex == 0 ? "yellow" : "white"
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: {bookview.currentIndex = 0}
                }
            }

            Rectangle{
                width: 200
                height: 15
                color: Qt.rgba(0,0,0,0)

                Text {
                    anchors.centerIn: parent
                    text: "Religion"
                    font.bold: true
                    font.pixelSize: 14
                    color: bookview.currentIndex == 1 ? "yellow" : "white"
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: {bookview.currentIndex = 1}
                }
            }

            Rectangle{
                width: 200
                height: 15
                color: Qt.rgba(0,0,0,0)

                Text {
                    anchors.centerIn: parent
                    text: "Science"
                    font.bold: true
                    font.pixelSize: 14
                    color: bookview.currentIndex == 2 ? "yellow" : "white"
                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: {bookview.currentIndex = 2}
                }
            }
        }
    }
}
