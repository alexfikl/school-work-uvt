import QtQuick 1.1

Item {
    id: lineEdit
    height: parent.height - 5

    BorderImage {
        source: "images/lineedit.png"
        anchors.fill: parent
        border.right: 12
        border.left: 12
    }

    TextInput {
        id: editor
        anchors {
            left: parent.left; right: parent.right; leftMargin: 10; rightMargin: 10
            verticalCenter: parent.verticalCenter
        }
        cursorVisible: true
        color: "#151515"; selectionColor: "Green"
        font.pixelSize: 14
    }
}
