import QtQuick 1.1
import 'server.js' as Server

Item {
    id: booklist
    width: 800
    height: 400

    signal reload

    property variant model: ''

    ListModel { id: bookmodel }

    Component.onCompleted: {
        Server.fetchModelData(model);
    }

    onReload: Server.reload()

    Loading {
        id: loading
        width:32; height: 32
        anchors.centerIn: parent
    }

    Component {
        id: bookDelegate

        Item {
            id: book

            Component.onCompleted: {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4) {
                        book.excerpt = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", excerptFile, true);
                xmlhttp.send(null);
            }

            // Create a property to contain the visibility of the details.
            // We can bind multiple element's opacity to this one property,
            // rather than having a "PropertyChanges" line for each element we
            // want to fade.
            property real detailsOpacity : 0
            property string excerpt: ''

            width: 800
            height: 100

            // A simple rounded rectangle for the background
            Rectangle {
                id: background
                x: 2; y: 2; width: parent.width - x*2; height: parent.height - y*2
                color: Qt.rgba(0,0,0,0.5)
            }

            // This mouse region covers the entire delegate.
            // When clicked it changes mode to 'Details'.  If we are already
            // in Details mode, then no change will happen.
            MouseArea {
                anchors.fill: parent
                onClicked: book.state = 'Details';
            }

            // Lay out the page: picture, title and ingredients at the top, and method at the
            // bottom.  Note that elements that should not be visible in the list
            // mode have their opacity set to book.detailsOpacity.
            Row {
                id: topLayout
                x: 10; y: 10; height: bookImage.height; width: parent.width
                spacing: 10

                Image {
                    id: bookImage
                    width: 53; height: 80
                    fillMode: Image.PreserveAspectFit
                    source: picture
                }

                Column {
                    width: background.width - bookImage.width - 20; height: bookImage.height
                    spacing: 5

                    Text {
                        text: title + '\n' + author
                        font.pointSize: 16
                        font.bold: true
                        color: "white"
                    }

                    Text {
                        text: "Details"
                        font.pointSize: 14; font.bold: true
                        opacity: book.detailsOpacity
                        color: "white"
                    }

                    Text {
                        text: "<br /><b>Date:</b> " + date + "<br /><b>Pages:</b> " + pages + "<br /><b>Language:</b> English"
                        wrapMode: Text.WordWrap
                        font.pointSize: 12
                        width: parent.width
                        opacity: book.detailsOpacity
                        color: "white"
                    }
                }
            }

            Item {
                id: details
                x: 10; width: parent.width - 20
                anchors { top: topLayout.bottom; topMargin: 10; bottom: parent.bottom; bottomMargin: 10 }
                opacity: book.detailsOpacity

                Text {
                    id: excerptTitle
                    anchors.top: parent.top
                    text: "Excerpt\n"
                    font.pointSize: 12; font.bold: true
                    color: "white"
                }

                Flickable {
                    id: flick
                    width: parent.width
                    anchors { top: excerptTitle.bottom; bottom: parent.bottom }
                    contentHeight: excerptText.height
                    clip: true

                    Text {
                        id: excerptText
                        text: book.excerpt
                        wrapMode: Text.WordWrap
                        font.pointSize: 11
                        width: details.width
                        color: "white"
                    }
                }

                Image {
                    anchors { right: flick.right; top: flick.top }
                    source: "images/moreUp.png"
                    opacity: flick.atYBeginning ? 0 : 1
                }

                Image {
                    anchors { right: flick.right; bottom: flick.bottom }
                    source: "images/moreDown.png"
                    opacity: flick.atYEnd ? 0 : 1
                }
            }

            // A button to close the detailed view, i.e. set the state back to default ('').
            Image {
                source: 'images/close.png'
                y: 10
                anchors { right: background.right; rightMargin: 10 }
                opacity: book.detailsOpacity

                MouseArea{
                    anchors.fill: parent
                    onClicked: book.state = '';
                }
            }

            states: State {
                name: "Details"

                PropertyChanges { target: background; color: Qt.rgba(0,0,0,0.7) }
                PropertyChanges { target: bookImage; width: 110; height: 210 } // Make picture bigger
                PropertyChanges { target: book; detailsOpacity: 1; x: 0 } // Make details visible
                PropertyChanges { target: book; height: bookListView.height } // Fill the entire list area with the detailed view

                // Move the list so that this item is at the top.
                PropertyChanges { target: bookListView; explicit: true; contentY: book.y }

                // Disallow flicking while we're in detailed bookview
                PropertyChanges { target: bookListView; interactive: false }
            }

            transitions: Transition {
                // Make the state changes smooth
                ParallelAnimation {
                    ColorAnimation { property: "color"; duration: 500 }
                    NumberAnimation { duration: 300; properties: "detailsOpacity,x,contentY,height,width" }
                }
            }
        }
    }

    ListView {
        id: bookListView
        height: parent.height - 80
        width: parent.width
        model: bookmodel
        delegate: bookDelegate
    }
}
