function fetchModelData(model){
    console.log('fetch');
    var xhr = new XMLHttpRequest;
    xhr.open("GET", "http://localhost:8000/db.php");
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if(xhr.responseText == '') return 0;
            console.log(xhr.responseText);
            var a = JSON.parse(xhr.responseText);
            var i = 0;
            if(model == 'fiction') i = 0;
            if(model == 'religion') i = 1;
            if(model == 'science') i = 2;
            a = a.items;
            fetchCategory(a[i].items);
            loading.visible = false;
            console.log('iei..loaded books');
        }
    }
    xhr.send();
}

function fetchCategory(books){
    for(var i = 0; i < books.length; i++) {
        var o = books[i];
        console.log(o.cover)
        bookmodel.append({
            title: o.title,
            author: o.author,
            language: o.language,
            date: o.date,
            pages: o.pages,
            picture: 'data/cover/' + o.cover,
            excerptFile: 'data/description/' + o.cover.replace('.jpg', '.txt')
        });
    }
    console.log(bookmodel.count)
}

function reload(){
    bookmodel.clear();
    loading.visible = true;
    fetchModelData(booklist.model);
}
